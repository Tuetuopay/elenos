<?php

include_once 'views/membre_view.php';
include_once 'bbcode_parser.php';

function membreVewProfile ($bdd, $username)	{
	beginPage ();

	echo buildMembreListButton ();

	if (!doUserExists ($bdd, $username) && 1==2)	{
		beginStandaloneSection ('Erreur', false);
		echo "<p>Personne ne s'appelle ".$username.". Dommage !</p>";
		endStandaloneSection ();
		endPage ();
		return;
	}

	membreBuildProfile ($bdd, $username);

	if (getCurrentUserTokens () > 0 && $username != getCurrentUserName ())	{
		beginStandaloneSection ("Envoi de tokens", false);
		echo buildSendTokenForm ($username, getCurrentUserTokens ());
		endStandaloneSection ();
	}

	if (isUserLoggedIn () && doCurrentUserHavePermission ('general.editUser') && doUserExists ($bdd, $username))	{
		beginStandaloneSection ('Administration', false);

		$tokens = PDOQuery ($bdd, 'SELECT `tokens` AS `var` FROM `elenos_users` WHERE `username` = :var', 'var', $username);
		$rank = PDOQuery ($bdd, 'SELECT `groupID` AS `var` FROM `elenos_users` WHERE `username` = :var', 'var', $username);

		$ranks = array();
		$req = $bdd->query ('SELECT * FROM `elenos_user_group` ORDER BY `id`');
		while ($data = $req->fetch ()) {
			$ranks[$data['id']] = $data['groupName'];
		}

		echo buildAdminForm ($username, $tokens, $ranks, $rank);

		endStandaloneSection ();
	}

	endPage ();
}
function membreBuildProfile ($bdd, $username)	{
	/* Getting connexion dates */
	$lobbyPort = PDOQuery ($bdd, 'SELECT `port` AS `var` FROM `elenos_servers` WHERE `id` = :var', 'var', getLobbyServer ($bdd));
	$api = getJSONAPI ($lobbyPort);
	$infosSRV = $api->call('getPlayer', array($username));
	$infosSRV = $infosSRV[0]['success'];

	if (is_null ($infosSRV['gameMode']))
			$firstConnDate = $lastConnDate = false;
	else	{
		$firstConnDate = date ("d/m/Y à H:i:s", $infosSRV['firstPlayed']);
		$lastConnDate = $infosSRV['lastPlayed'];

		if ($lastConnDate == "0")	$lastConnDate = $firstConnDate;
		else						$lastConnDate = date("d/m/Y à H:i:s", $lastConnDate);
	}
	/* OP ? */
	if ($infosSRV['op'])
		$op = bbcodeParse (' [color=red][OP][/color]', true, false);
	else
		$op = "";

	/* Ingame rank */
	$rankIG = $api->call ('permissions.getGroups', array ($username));
	$rankIG = $rankIG['0'];
	$rankIG = @$rankIG['success']['0'];
	if (empty ($rankIG))
		$rankIG = "Indéfini";

	/* Getting the server */
	$server = getUserServer ($bdd, $username);
	if ($server == 0)
		$server = bbcodeParse ('[color=red][ Hors-ligne ][/color]', true, false);
	else
		$server = bbcodeParse ('[color=green][ '.getServerName ($bdd, $server).' ][/color]', true, false);

	/* Getting the avatar */
	$avatar = getUserAvatarURL ($bdd, $username);

	/* Getting the rank */
	$req = $bdd->prepare ('SELECT `group`.`groupName` AS `name`, `group`.`general.groupColor` AS `color` 
						   FROM `elenos_user_group` AS `group`, `elenos_users` AS `user`
						   WHERE `user`.`username` = :username AND `group`.`id` = `user`.`groupID`');
	$req->execute (array ('username' => $username));
	$data = $req->fetch ();
	if (is_empty ($data))	{
		$data['color'] = 'white';
		$data['name'] = 'Non-inscrit';
	}
	$rank = bbcodeParse ('[color='.$data['color'].']'.$data['name'].'[/color]', true, false);

	/* Finally, getting register date */
	$registerDate = PDOQuery ($bdd, 'SELECT `registerDate` AS `var` FROM `elenos_users` 
									 WHERE `username` = :var', 'var', $username);
	if (is_empty ($registerDate))
		$registerDate = false;
	else
		$registerDate = str_replace (' ', ' à ', $registerDate);

	beginStandaloneSection ($username.' '.$server, false);

	echo buildUserProfile ($username, $avatar, $rank, $rankIG, $op, $firstConnDate, $lastConnDate, $registerDate);

	endStandaloneSection ();
}
function membreList ($bdd, $letter)	{
	beginPage ();

	if ($letter == 'zero')	$letter = "0";
	if ($letter != "meh" && $letter != "all" && $letter != "rand")	{
		substr($letter, 0, 0);
		$letter = strtoupper ($letter);
	}

	$charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	echo buildLetterList ($charset, $letter);

	/* Joueurs demandés */
	$players = array ();
	if ((string)$letter == "meh")	{
		$req = $bdd->query ('SELECT * FROM `elenos_users` ORDER BY `username` LIMIT 200');
		$title = "Joueurs inscrits";
	}
	else if ((string)$letter == "rand")	{
		$req = $bdd->query ('SELECT * FROM `elenos_users` ORDER BY RAND() LIMIT 200');
		$title = "Joueurs aléatoires";
	}
	else if (@strpos($charset, $letter) !== false)	{
		$req = $bdd->prepare ('SELECT * FROM `elenos_users` WHERE LOWER(`username`) LIKE LOWER(concat(:letter, \'%\'))');
		$req->execute (array ('letter' => $letter));
		$title = "Joueurs en ".$letter;
	}
	else	{
		$req = $bdd->query ("SELECT * FROM `elenos_users` WHERE LOWER(`username`) REGEXP '^[^".$charset."]'");
		$title = "Les autres joueurs";
	}

	while ($data = $req->fetch ())
		$players[$data['username']] = array ('avatar' => getUserAvatarURL ($bdd, $data['username']));

	beginStandaloneSection ($title." (".count($players).")", true, "", 564);
	beginMemberList ();
	$i = 0;
	foreach ($players as $player => $infos)	{
		if ($i %2 == 0)
			echo beginMemberLine ();
		if (isset($infos['server']))	echo buildMemberEntry ($player, $infos['avatar'], $infos['server']);
		else							echo buildMemberEntry ($player, $infos['avatar'], false);
		if ($i % 2 == 1)
			echo endMemberLine ();
		$i++;
	}
	endMemberList ();
	endStandaloneSection ();

	/* Joueurs connectés */
	$players = getOnlinePlayers ($bdd);
	foreach ($players as $name => $server)
		$players[$name] = array ('avatar' => getUserAvatarURL ($bdd, $name),
								 'server' => bbcodeParse ('[color=green][ '.$server.' ][/color]', false, false));
	$title = "Joueurs connectés";

	beginStandaloneSection ($title." (".count($players).")", true, "", 336);
	beginMemberList ();
	foreach ($players as $player => $infos)	{
		beginMemberLine ();
		if (isset($infos['server']))	echo buildMemberEntry ($player, $infos['avatar'], $infos['server']);
		else							echo buildMemberEntry ($player, $infos['avatar'], false);
		endMemberLine ();
	}
	endMemberList ();
	endStandaloneSection ();

	endPage ();
}

function membreSetRank ($bdd, $username, $rank)	{
	if (!isUserLoggedIn ())
		return 'Vous devez être connecté pour espérer modifier un rang ...';
	if (!doCurrentUserHavePermission ('general.editUser'))
		return "Vous n'avez pas le droit de modifier le rang de ".$username.".";
	if (!doUserExists ($bdd, $username))
		return $username." n'est pas inscrit sur le site.";
	if (PDOQuery ($bdd, 'SELECT COUNT(*) AS `var` FROM `elenos_user_group` WHERE `id` = :var', 'var', $rank) == 0)
		return "Ce grade n'existe pas.";

	$req = $bdd->prepare ('UPDATE `elenos_users` SET `groupID` = :id WHERE `username` = :username');
	$req->execute (array ('id' => $rank, 'username' => $username));
	$rc = $req->rowCount ();
	$req->closeCursor ();

	if ($rc != 0)
		return 'ok';
	else
		return 'Erreur interne, réessayez plus tard.';
}
function membreSetTokens ($bdd, $username, $tokens)	{
	if (!isUserLoggedIn ())
		return 'Vous devez être connecté pour espérer modifier un rang ...';
	if (!doCurrentUserHavePermission ('general.editUser'))
		return "Vous n'avez pas le droit de modifier les tokens de ".$username.".";
	if (!doUserExists ($bdd, $username))
		return $username." n'est pas inscrit sur le site.";

	$req = $bdd->prepare ('UPDATE `elenos_users` SET `tokens` = :tokens WHERE `username` = :username');
	$req->execute (array ('tokens' => $tokens, 'username' => $username));
	$rc = $req->rowCount ();
	$req->closeCursor ();

	if ($rc != 0)
		return 'ok';
	else
		return 'Erreur interne, réessayez plus tard.';
}
function membreSendToken ($bdd, $username, $amount)	{
	$amount = (int)$amount;
	if (!isUserLoggedIn ())
		return "Vous devez être connecté pour envoyer des tokens.";
	if ($amount > (int)getCurrentUserTokens ())
		return "Vous n'avez pas assez de tokens.";
	if ($amount < 0)
		return "Vous nous prenez pas pour un jambon par hasard ? Faut pas voler ...";
	if (!doUserExists ($bdd, $username))
		return $username." n'est pas inscrit sur le site.";

	/* Removing tokens from user's account */
	$req = $bdd->prepare ('UPDATE `elenos_users` SET `tokens`=`tokens`-:amount WHERE `username` = :username');
	$req->execute (array ('amount' => $amount, 'username' => getCurrentUserName ()));

	/* Adding tokens to friend's account */
	$req = $bdd->prepare ('UPDATE `elenos_users` SET `tokens`=`tokens`+:amount WHERE `username` = :username');
	$req->execute (array ('amount' => $amount, 'username' => $username));

	/* Recording transaction */
	$req = $bdd->prepare ('INSERT INTO `elenos_token_transact`(`user`, `token`, `status`, `itemID`, `completionDate`, `method`, `fromUser`)
						   VALUES (:toUser, :amount, "completed", 1, NOW(), "user", :fromUser)');
	$req->execute (array ('toUser' => $username, 'amount' => $amount, 'fromUser' => getCurrentUserName ()));

	return 'ok';
}

?>