<?php

function getTokenPrice ()	{
	return 10;
}

include_once 'views/vote_view.php';
include_once 'views/membre_view.php';

function voteMainPage ($bdd)	{
	/* ========== VOTE MODULE ========== */
	beginPage ();
	beginStandaloneSection ("Voter pour Elenos", true, "vote");

	$req = $bdd->query ('SELECT * FROM `elenos_vote` ORDER BY `id`');

	echo bbcodeParse ("Vous pouvez voter pour Elenos sur différents sites et ainsi recevoir des points.\nC'est cool !", true);

	while ($site = $req->fetch ())
		echo buildVoteButton ('vote.php?vote='.$site['id'], $site['image'], $site['points']);

	endStandaloneSection ();

	/* ========= USER'S SCORE ========== */
	beginStandaloneSection ("Votre progression", true);
	echo bbcodeParse ("[center]Atteignez 10 points vote pour obtenir\nun jeton vote ![/center]", true);

	$points = 0;

	if (isUserLoggedIn ())	{
		$points = PDOQuery ($bdd, 'SELECT `votePoints` AS `var` FROM `elenos_users` WHERE `username` = :var', 'var', getCurrentUserName ());
		echo bbcodeParse ("[center]Vous avez ".($points % 10)." / 10 points vote[/center]", true);
	}
	else	echo bbcodeParse ("[center]Connectez-vous pour\ngagner des points ![/center]", true);

	echo buildProgressBar (10, $points % 10);

	if ($points)
		echo bbcodeParse ("[center]<img src=\"images/coins.png\" class=\"icon icon_left\" alt=\"MONEY\" /> ".(int)($points / 10)." jetons vote.[/center]");

	//if (isUserLoggedIn ())	{
		echo '<p class="text_center"><a href="vote.php#popup_vote_shop" class="push_button_normal">Voir les récompenses</a></p>';
	//}
	$req = $bdd->query ('SELECT * FROM `elenos_vote_shop` ORDER BY `price`');
	$items = array();
	while ($item = $req->fetch ())
		$items[$item['name']] = array ('price' => (int)($item['price'] / 10), 'server' => getServerName ($bdd, $item['server']), 'id' => $item['id']);

	echo buildVoteShop ($items, (int)($points / 10));

	endStandaloneSection ();

	/* ========== BEST VOTERS ========== */
	beginStandaloneSection ("Les 12 meilleurs votants de ce mois", false);

	$req = $bdd->query ('SELECT * FROM `elenos_users` WHERE `voteCount` != 0 ORDER BY `voteCount` DESC LIMIT 12');

	beginMemberList ();
	$i = 0;
	while ($player = $req->fetch ())	{
		if ($i %3 == 0)
			echo beginMemberLine ();

		echo buildVoteEntry ($player['username'], getUserAvatarURL ($bdd, $player['username']), $player['voteCount']." vote".(($player['voteCount'] > 1) ? "s" : ""), $i+1);

		if ($i % 3 == 2)
			echo endMemberLine ();
		$i++;
	}
	endMemberList ();
	if (!$i)	{
		echo "<p>Personne n'a encore voté ce mois-ci ! Votez et tentez de gagner un compte Minecraft Premium !</p>";
	}
	endStandaloneSection ();

	/* ========== ADMIN STUFF : EDITING PRIZES ========== */
	if (doCurrentUserHavePermission("shop.editItems"))	{
		beginStandaloneSection ("Éditer les récompenses", false);
		beginPrizeList ();

		$servers = array();
		$req = $bdd->query ('SELECT * FROM `elenos_servers`');
		while ($server = $req->fetch ())
			$servers[$server['id']] = $server['name'];

		$req = $bdd->query ('SELECT * FROM `elenos_vote_shop` ORDER BY `price` DESC');
		while ($item = $req->fetch ())
			echo prizeEntry ($item['id'], $item['name'], (int)($item['price'] / 10), $item['server'], $item['command'], $servers);
		echo addItemEntry ($servers);

		endPrizeList ();
		endStandaloneSection ();
	}

	endPage ();
}

function voteOnSite ($bdd, $id)	{
	if (!isUserLoggedIn ())
		return "Vous devez être connecté pour voter.";

	if (PDOQuery ($bdd, 'SELECT COUNT(*) AS `var` FROM `elenos_vote` WHERE `id` = :var', 'var', $id) == 0)
		return "Ce site n'existe pas.";

	$times = json_decode (PDOQuery ($bdd, 'SELECT `voteCooldown` AS `var` FROM `elenos_users` WHERE `username` = :var', 'var',
		 getCurrentUserName ()), true);
	if (!isset ($times[$id]))
		$times[$id] = 0;

	$cooldown = PDOQuery ($bdd, 'SELECT `cooldown` AS `var` FROM `elenos_vote` WHERE `id` = :var', 'var', $id);
	$now = time();
	if ($now - $times[$id] < $cooldown)	{
		$still = explode (':', gmdate('H:i:s', $cooldown - ($now - $times[$id])));
		return 'Vous devez encore attendre '.$still[0].' heures, '.$still[1].' minutes et '.$still[2].' secondes avant de revoter
		 sur ce site.';
	}

	$times[$id] = $now;

	$req = $bdd->prepare ('UPDATE `elenos_users` AS `users`, `elenos_vote` AS `vote`
						   SET `users`.`votePoints` = `users`.`votePoints` + `vote`.`points`, 
							   `users`.`voteCooldown` = :cool, 
							   `users`.`voteCount` = `users`.`voteCount` + 1
						   WHERE `users`.`username` = :user AND `vote`.`id` = :id');
	$req->execute (array('user' => getCurrentUserName (), 'id' => $id, 'cool' => json_encode($times)));

	return 'ok';
}
function voteGetSiteURL ($bdd, $id)	{
	return PDOQuery ($bdd, 'SELECT `url` AS `var` FROM `elenos_vote` WHERE `id` = :var', 'var', $id);
}

function voteBuyItem ($bdd, $itemID)	{
	if (!isUserLoggedIn ())
		return "Vous devez être connecté pour obtenir une récompense !";

	$points = (int)PDOQuery ($bdd, 'SELECT `votePoints` AS `var` FROM `elenos_users` WHERE `username` = :var', 'var', getCurrentUserName ());

	if ($itemID == "tokens")	{
		$tokenPrice = getTokenPrice ();
		if (!isset ($_POST['tokens']))
			return "Erf ... Il manque quelque-chose ...";
		$tokens = $_POST['tokens'];
		if (empty ($tokens))
			return "C'est vide par ici ...";
		if ($points < $tokens * 10 * $tokenPrice)
			return "Vous n'avez pas assez de points pour obtenir ".$tokens." tokens.";

		$req = $bdd->prepare ('UPDATE `elenos_users` SET `votePoints` = `votePoints` - :price, `tokens` = `tokens` + :tokens WHERE `username` = :username');
		$req->execute (array('price' => $tokens * 10 * $tokenPrice, 'username' => getCurrentUserName (), 'tokens' => $tokens));
		return 'ok';
	}

	$req = $bdd->prepare ('SELECT * FROM `elenos_vote_shop` WHERE `id` = :id');
	$req->execute (array('id' => $itemID));
	$item = $req->fetch ();

	if (empty($item))
		return "La récompense N°".$itemID." n'existe pas.";

	if ($points < $item['price'])
		return "Vous n'avez pas assez de points pour cette récompense.";
	if (!isCurrentUserOnline ($bdd, $item['server']))
		return "Veuillez vous connecter au serveur “".getServerName ($bdd, $item['server'])."” pour acheter cet objet.";

	$req = $bdd->prepare ('UPDATE `elenos_users` SET `votePoints` = `votePoints` - :price WHERE `username` = :username');
	$req->execute (array('price' => $item['price'], 'username' => getCurrentUserName ()));

	$port = (int)PDOQuery ($bdd, 'SELECT `port` AS `var` FROM `elenos_servers` WHERE `id` = :var', 'var', $item['server']);
	$api = getJSONAPI ($port);

	$command = $item['command'];
	$command = str_replace('{player}', getCurrentUserName (), $command);
	$command = str_replace('{rec}', $item['name'], $command);

	if (strstr($command, '[{NEW}]')) {
		$commands = explode('[{NEW}]', $command);
		foreach ($commands as $cmd)
			$api->call("runConsoleCommand", array($cmd));
	} else {
		$api->call("runConsoleCommand", array($command));
	}

	return 'ok';
}
function voteEditItem ($bdd, $id, $name, $price, $server, $command)	{
	if (!isUserLoggedIn ())
		return "Vous devez être connecté pour espérer modifier les prix.";
	if (!doCurrentUserHavePermission("shop.editItems"))
		return "Vous n'avez pas la permission de modifier les prix.";
	if (PDOQuery ($bdd, 'SELECT COUNT(*) AS `var` FROM `elenos_vote_shop` WHERE `id` = :var', 'var', $id) == 0)
		return "Ce prix n'existe pas.";

	$req = $bdd->prepare ('UPDATE `elenos_vote_shop` SET `name`=:name, `price`=:price, `server`=:server, `command`=:command WHERE `id` = :id');
	$req->execute (array ('name' => $name, 'price' => $price * 10, 'server' => $server, 'command' => $command, 'id' => $id));
	$rc = $req->rowCount ();
	$req->closeCursor ();

	if ($rc != 0)
		return 'ok';
	else
		return "Erreur interne. Merci de réessayer plus tard.";
}
function voteDeleteItem ($bdd, $id)	{
	if (!isUserLoggedIn ())
		return "Vous devez être connecté pour espérer supprimer un prix.";
	if (!doCurrentUserHavePermission("shop.editItems"))
		return "Vous n'avez pas la permission de supprimer un prix.";
	if (PDOQuery ($bdd, 'SELECT COUNT(*) AS `var` FROM `elenos_vote_shop` WHERE `id` = :var', 'var', $id) == 0)
		return "Ce prix n'existe pas.";

	PDOQuery ($bdd, 'DELETE FROM `elenos_vote_shop` WHERE `id` = :id', 'id', $id);

	return 'ok';
}
function voteAddItem ($bdd, $name, $price, $server, $command)	{
	if (!isUserLoggedIn ())
		return "Vous devez être connecté pour espérer ajouter un prix.";
	if (!doCurrentUserHavePermission("shop.editItems"))
		return "Vous n'avez pas la permission d'ajouter un prix.";

	$req = $bdd->prepare ('INSERT INTO `elenos_vote_shop`(`price`, `name`, `command`, `server`) VALUES (:price, :name, :command, :server)');
	$req->execute (array('price' => $price * 10, 'name' => $name, 'command' => $command, 'server' => $server));

	return 'ok';
}


?>