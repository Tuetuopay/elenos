<?php

include_once ('views/support_view.php');
include_once ('bbcode_parser.php');

function supportMainPage ($bdd, $pending = false)	{
	beginPage ();

	if (!isUserLoggedIn ())	{
		beginStandaloneSection ("Vous devez être connecté pour accéder au support.", false);
		endStandaloneSection ();
		endPage ();
		return;
	}

	if (doCurrentUserHavePermission ('support.validateTicket'))
		echo buildViewPendingButton ();

	echo buildNewTicketForm ();

	beginTicketList ();

	if (doCurrentUserHavePermission ('support.validateTicket') && $pending)
		$req = $bdd->query ('SELECT * FROM `elenos_support_tickets` WHERE `status` = "pending" ORDER BY `id` DESC');
	else if (doCurrentUserHavePermission ('support.validateTicket'))
		$req = $bdd->query ('SELECT * FROM `elenos_support_tickets` ORDER BY `id` DESC');
	else	{
		$req = $bdd->prepare ('SELECT * FROM `elenos_support_tickets` WHERE UPPER(`author`) = UPPER(:author) ORDER BY `id` DESC LIMIT 50');
		$req->execute (array('author' => getCurrentUserName ()));
	}

	while ($data = $req->fetch ())	{
		$answerCount = PDOQuery ($bdd, 'SELECT COUNT(*) AS `var` FROM `elenos_support_tickets_replies` WHERE `ticketID` = :var', 'var', $data['id']);
		echo buildSingleTicket ($data['id'], bbcodeParse ($data['title'], true), bbcodeParse($data['content'], true), $data['status'], $data['author'],
			$data['date'], $answerCount);
	}

	endTicketList ();

	endPage ();
}
function supportViewTicket ($bdd, $id)	{
	beginPage ();

	if (isUserLoggedIn () && doCurrentUserHavePermission ('support.validateTicket'))
		echo buildValidateButtons ($id);

	if (!supportTicketExist ($bdd, $id))	{
		echo buildGoBackButton ();
		beginStandaloneSection ("Ce ticket n'existe pas.", false);
		endStandaloneSection ();
		endPage ();
		return;
	}

	$req = $bdd->prepare ('SELECT * FROM `elenos_support_tickets` WHERE `id` = :id');
	$req->execute(array('id' => $id));
	$ticket = $req->fetch ();

	beginTicketDetail($ticket['id'], bbcodeParse ($ticket['title'], true), bbcodeParse($ticket['content'], true), $ticket['status'], $ticket['author'],
		getUserAvatarURL($bdd, $ticket['author']), $ticket['date']);

	$req = $bdd->prepare ('SELECT * FROM `elenos_support_tickets_replies` WHERE `ticketID` = :id ORDER BY `id`');
	$req->execute (array('id' => $ticket['id']));
	while ($answer = $req->fetch ())	{
		echo buildTicketReply ($answer['id'], bbcodeParse($answer['content'], true), $answer['status'],
			$answer['author'], getUserAvatarURL($bdd, $answer['author']), $answer['date']);
	}

	endTicketList ();

	echo buildNewReplyForm ($id);

	endPage ();
}

function supportSaveTicket ($bdd, $subject, $content)	{
	if (!isUserLoggedIn ())
		return 'Vous devez être connecté pour ajouter un ticket.';
	if ($subject == '' || $content == '')
		return 'Veuillez remplir tous les champs.';

	$req = $bdd->prepare ('INSERT INTO `elenos_support_tickets`(`title`, `content`, `status`, `author`, `date`)
						   VALUES (:title, :content, "pending", :author, NOW())');
	$req->execute (array('title' => $subject, 'content' => $content, 'author' => getCurrentUserName ()));
	$rc = $req->rowCount ();
	$req->closeCursor ();

	$_SESSION['ticketID'] = PDOQuery ($bdd, 
		'SELECT `id` AS `var` FROM `elenos_support_tickets` WHERE UPPER(`author`) = UPPER(:var) ORDER BY `id` DESC LIMIT 1', 'var', getCurrentUserName ());
	
	if ($rc != 0)
		return 'ok';
	else
		return 'Erreur interne, réessayez plus tard.';
}
function supportSaveAnswer ($bdd, $ticketID, $content)	{
	if (!isUserLoggedIn ())
		return 'Vous devez être connecté pour répondre à ce ticket.';
	if ($content == '')
		return 'Une réponse vide ? Nan mais sérieux T_T';
	if (!supportTicketExist ($bdd, $ticketID))
		return "Ce ticket n'existe pas.";

	$req = $bdd->prepare ('INSERT INTO `elenos_support_tickets_replies`(`ticketID`, `content`, `author`, `date`)
						   VALUES (:id, :content, :author, NOW())');
	$req->execute (array('id' => $ticketID, 'content' => $content, 'author' => getCurrentUserName ()));
	$rc = $req->rowCount ();
	$req->closeCursor ();

	if ($rc != 0)
		return 'ok';
	else
		return 'Erreur interne, réessayez plus tard.';
}

function supportValidateTicket ($bdd, $ticketID)	{
	if (!isUserLoggedIn () || !doCurrentUserHavePermission ('support.validateTicket'))
		return 'Vous n\'avez pas le droit de valider un ticket.';
	if (!supportTicketExist ($bdd, $ticketID))
		return "Ce ticket n'existe pas.";

	$req = $bdd->prepare ('UPDATE `elenos_support_tickets` SET `status` = "completed" WHERE `id` = :id');
	$req->execute (array('id' => $ticketID));
	$rc = $req->rowCount ();
	$req->closeCursor ();

	if ($rc != 0)
		return 'ok';
	else
		return 'Erreur interne, réessayez plus tard.';
}
function supportPendTicket ($bdd, $ticketID)	{
	if (!isUserLoggedIn () || !doCurrentUserHavePermission ('support.validateTicket'))
		return 'Vous n\'avez pas le droit d\'invalider un ticket.';
	if (!supportTicketExist ($bdd, $ticketID))
		return "Ce ticket n'existe pas.";

	$req = $bdd->prepare ('UPDATE `elenos_support_tickets` SET `status` = "pending" WHERE `id` = :id');
	$req->execute (array('id' => $ticketID));
	$rc = $req->rowCount ();
	$req->closeCursor ();

	if ($rc != 0)
		return 'ok';
	else
		return 'Erreur interne, réessayez plus tard.';
}
function supportDeleteTicket ($bdd, $ticketID)	{
	if (!isUserLoggedIn () || !doCurrentUserHavePermission ('support.validateTicket'))
		return 'Vous n\'avez pas le droit dde supprimer un ticket.';
	if (!supportTicketExist ($bdd, $ticketID))
		return "Ce ticket n'existe pas.";

	$req = $bdd->prepare ('DELETE FROM `elenos_support_tickets` WHERE `id` = :id');
	$req->execute (array('id' => $ticketID));
	$rc = $req->rowCount ();
	$req->closeCursor ();

	if ($rc != 0)
		return 'ok';
	else
		return 'Erreur interne, réessayez plus tard.';
}

function supportTicketExist ($bdd, $id)	{
	$cnt = PDOQuery ($bdd, 'SELECT COUNT(*) AS `var` FROM `elenos_support_tickets` WHERE `id` = :var', 'var', $id);
	if ($cnt == 0)	return false;
	else			return true;
}


?>