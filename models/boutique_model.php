<?php

include_once ('common.php');
include_once ('views/boutique_view.php');
include_once ('bbcode_parser.php');
include_once ('JSONAPI.php');

function shopMainPage ($bdd, $activeServer)	{
	beginPage ();

	if (isUserLoggedIn ())
		echo buildUserTokenCount (getCurrentUserTokens ());
	else
		echo buildUserNotLoggedIn ();

	$req = $bdd->prepare ('SELECT * FROM `elenos_shop_sections` WHERE `server` = :srv ORDER BY `order`');
	$req->execute (array ('srv' => $activeServer));
	while ($data = $req->fetch ())	{
		if (doCurrentUserHavePermission ('shop.editItems') && 1==2)
			echo buildAddSection ($activeServer);
		shopSection ($bdd, $activeServer, $data['id'], $data['name']);
	}
	$req->closeCursor ();

	if (doCurrentUserHavePermission ('shop.editItems'))
		echo buildAddSection ($activeServer);

	endPage ();
}
function shopSection ($bdd, $server, $id, $name)	{
	beginSection ($server, $name, $id);

	if (doCurrentUserHavePermission ('shop.editItems'))
		echo shopSectionMoveButtons ($bdd, $server, $id);

	$req = $bdd->prepare ('SELECT * FROM `elenos_shop_items` WHERE `section` = :id ORDER BY `id`');
	$req->execute (array ('id' => $id));

	while ($data = $req->fetch ())	{
		echo buildProduct ($data['id'], $data['label'], $data['icon'], bbcodeParse($data['description']), $data['price'], $data['description'], 
			$data['command']);
	}
	$req->closeCursor ();

	if (doCurrentUserHavePermission ('shop.editItems'))
		echo buildAddItemBox ($id);

	endSection ();
}

function shopChooseServer ($bdd)	{
	beginPage ();

	if (isUserLoggedIn ())
		echo buildUserTokenCount (getCurrentUserTokens ());
	else
		echo buildUserNotLoggedIn ();

	echo '<p class="text_center"><img src="images/fleche.png" alt="flèche" /></p>';

	$servers = array();
	$req = $bdd->query ('SELECT * FROM `elenos_servers`');
	while ($srv = $req->fetch ())
		$servers[$srv['id']] = $srv['name'];

	echo buildPreServerList ($servers);

	endPage ();
}

function shopServerList ($bdd, $activeServer)	{
	$req = $bdd->query ('SELECT * FROM `elenos_servers` WHERE `shopDisplay` = 1 ORDER BY `id`');

	if (isUserLoggedIn ())
		$connectedServer = getCurrentUserServer ($bdd);
	else
		$connectedServer = 0;

	$ret = '<div class="aux_navbar"><ul>';
	while ($data = $req->fetch ())	{
		$ret .= '<li><a href="boutique.php?srv='.$data['id'].'"';
		if ((int)$data['id'] == (int)$activeServer)
			$ret .= ' class="menu_item_active"';
		$ret .= '>'.$data['name'].'</a>';
		if ((int)$data['id'] == (int)$connectedServer)
			$ret .= '<div class="shop_marker"></div>';
		$ret .= '</li>';
	}
	$ret .= '</ul></div>';
	echo $ret;
}
function shopSectionMoveButtons ($bdd, $server, $section)	{
	$ret = '<div class="shopSectionMoveButtons">';
	if (shopGetSectionBefore ($bdd, $server, $section) != 0)
		$ret .= '<a href="boutique.php?srv='.$server.'&amp;moveUp='.$section.'" class="shopSectionMoveUp push_button_normal">&#8593;</a>';
	if (shopGetSectionAfter ($bdd, $server, $section) != 0)
		$ret .= '<a href="boutique.php?srv='.$server.'&amp;moveDown='.$section.'" class="shopSectionMoveDown push_button_normal">&#8595;</a>';
	$ret .= '</div>';
	return $ret;
}

function shopBuyItem ($bdd, $itemID)	{
	$price = shopGetItemPrice ($bdd, $itemID);

	if (!isUserLoggedIn ())
		return "Vous devez être connecté pour effectuer des achats.";

	/* Getting the correct server port for this item */
	$req = $bdd->prepare ('SELECT `srv`.*
						   FROM `elenos_servers` AS `srv`, `elenos_shop_items` AS `itm`, `elenos_shop_sections` AS `sec`
						   WHERE `srv`.`id` = `sec`.`server` AND `sec`.`id` = `itm`.`section` AND `itm`.`id` = :item');
	$req->execute (array('item' => $itemID));
	$serverInfo = $req->fetch ();

	if (!isCurrentUserOnline ($bdd, $serverInfo['id']))
		return "Veuillez vous connecter au serveur “".$serverInfo['name']."” pour acheter cet objet.";
	if ($price > getCurrentUserTokens ())
		return "Vous n'avez pas assez de crédits ! Achetez en de nouveaux.";

	$rc = shopSetTokens ($bdd, getCurrentUserTokens () - $price);

	$insert = $bdd->prepare ('INSERT INTO `elenos_shop_history`(`item`, `player`, `date`) VALUES(:item, :player, NOW())');
	$insert->execute (array ('item' => $itemID, 'player' => getCurrentUserName ()));
	$insert->closeCursor ();

	if (class_exists (JSONAPI))	{
		$api = getJSONAPI ($serverInfo['port']);
		
		$itemInfo_req = $bdd->prepare ('SELECT `command` FROM `elenos_shop_items` WHERE `id` = :id');
		$itemInfo_req->execute (array('id' => $itemID));

		$command = $itemInfo_req->fetch();
		$command = $command['command'];
		$command = str_replace('{player}', getCurrentUserName (), $command);

		if (strstr($command, '[{NEW}]')) {
			$commands = explode('[{NEW}]', $command);
			foreach ($commands as $cmd) {
				$api->call("runConsoleCommand", array($cmd));
			}
		} else {
			$api->call("runConsoleCommand", array($command));
		}
	}

	if ($rc == 0)
		return 'Erreur interne. Merci de réessayer plus tard.';
	else
		return 'ok';
}
function shopGetItemPrice ($bdd, $itemID)	{
	$req = $bdd->prepare ('SELECT `price` FROM `elenos_shop_items` WHERE `id` = :id');
	$req->execute (array ('id' => $itemID));
	$ret = $req->fetch ();
	$ret = $ret['price'];
	$req->closeCursor ();
	return $ret;
}
function shopItemExists ($bdd, $itemID)	{
	$req = $bdd->prepare ('SELECT COUNT(*) AS cnt FROM `elenos_shop_items` WHERE `id` = :id');
	$req->execute (array ('id' => $itemID));
	$ret = $req->fetch ();
	$ret = $ret['cnt'];
	$req->closeCursor ();
	return ($ret == '0') ? false : true;
}
function shopSectionExists ($bdd, $sectionID)	{
	$req = $bdd->prepare ('SELECT COUNT(*) AS cnt FROM `elenos_shop_sections` WHERE `id` = :id');
	$req->execute (array ('id' => $sectionID));
	$ret = $req->fetch ();
	$ret = $ret['cnt'];
	$req->closeCursor ();
	return ($ret == '0') ? false : true;
}
function shopSetTokens ($bdd, $tokens)	{
	$req = $bdd->prepare ('UPDATE `elenos_users` SET `tokens` = :tokens WHERE UPPER(`username`) = UPPER(:username)');
	$req->execute (array ('tokens' => $tokens, 'username' => getCurrentUserName ()));
	$rc = $req->rowCount ();
	$req->closeCursor ();
	return $rc;
}

function shopAddItem ($bdd, $item_title, $item_img, $item_desc, $item_price, $sectionID, $command)	{
	if (!doCurrentUserHavePermission ('shop.editItems'))
		return "Vous n'avez pas la permission de modifier la boutique.";

	$item_price = (int)$item_price;
	if ($item_price <= 0)
		return "Le prix est trop bas ...";

	$req = $bdd->prepare ('INSERT INTO `elenos_shop_items`(`section`, `label`, `icon`, `description`, `price`, `command`)
						   VALUES (:section, :label, :icon, :description, :price, :command)');
	$req->execute (array ('section' => $sectionID,
						  'label' => $item_title,
						  'icon' => $item_img,
						  'description' => $item_desc,
						  'price' => $item_price,
						  'command' => $command));
	$rc = $req->rowCount ();
	$req->closeCursor ();

	if ($rc == 0)
		return 'Erreur interne. Merci de réessayer plus tard.';
	else
		return 'ok';
}
function shopEditItem ($bdd, $item_id, $item_title, $item_img, $item_desc, $item_price, $command)	{
	if (!doCurrentUserHavePermission ('shop.editItems'))
		return "Vous n'avez pas la permission de modifier la boutique.";

	$item_price = (int)$item_price;
	if ($item_price <= 0)
		return "Le prix est trop bas ...";

	$req = $bdd->prepare ('UPDATE `elenos_shop_items` SET `label`=:label, `icon`=:icon, `description`=:description, `price`=:price, `command`=:command
						   WHERE `id` = :id');
	$req->execute (array ('label' => $item_title,
						  'icon' => $item_img,
						  'description' => $item_desc,
						  'price' => $item_price,
						  'command' => $command,
						  'id' => $item_id));
	$rc = $req->rowCount ();
	$req->closeCursor ();

	if ($rc == 0)
		return 'Erreur interne. Merci de réessayer plus tard.';
	else
		return 'ok';
}
function shopDeleteItem ($bdd, $itemID)	{
	if (!doCurrentUserHavePermission ('shop.editItems'))
		return "Vous n'avez pas la permission de modifier la boutique.";

	$itemID = (int)$itemID;
	if (!shopItemExists ($bdd, $itemID))
		return "Cet objet n'existe pas.";

	$req = $bdd->prepare ('DELETE FROM `elenos_shop_items` WHERE `id` = :id');
	$req->execute (array ('id' => $itemID));

	$rc = $req->rowCount ();
	$req->closeCursor ();

	if ($rc == 0)
		return 'Erreur interne. Merci de réessayer plus tard.';
	else
		return 'ok';
}
function shopAddSection ($bdd, $name, $server)	{
	if (!doCurrentUserHavePermission ('shop.editItems'))
		return "Vous n'avez pas la permission de modifier la boutique.";

	$req = $bdd->prepare ('INSERT INTO `elenos_shop_sections`(`name`, `server`, `order`) VALUES (:name, :srv, 0)');
	$req->execute (array ('name' => $name, 'srv' => $server));
	$rc = $req->rowCount ();
	$req->closeCursor ();

	/* Getting last inserted ID */
	$req = $bdd->query ('SELECT `id` FROM `elenos_shop_sections` ORDER BY `id` DESC LIMIT 1');
	$lastID = $req->fetch ();
	$lastID = $lastID['id'];
	$req->closeCursor ();

	$req = $bdd->prepare ('UPDATE `elenos_shop_sections` SET `order` = `id` WHERE `id` = :id');
	$req->execute (array ('id' => $lastID));
	$req->closeCursor ();

	if ($rc == 0)
		return 'Erreur interne. Merci de réessayer plus tard.';
	else
		return 'ok';
}
function shopDeleteSection ($bdd, $sectionID)	{
	if (!doCurrentUserHavePermission ('shop.editItems'))
		return "Vous n'avez pas la permission de modifier la boutique.";

	$sectionID = (int)$sectionID;
	if (!shopSectionExists ($bdd, $sectionID))
		return "Cette section n'existe pas.";

	$req = $bdd->prepare ('DELETE FROM `elenos_shop_sections` WHERE `id` = :id');
	$req->execute (array ('id' => $sectionID));

	$rc = $req->rowCount ();
	$req->closeCursor ();

	if ($rc == 0)
		return 'Erreur interne. Merci de réessayer plus tard.';
	else
		return 'ok';
}
function shopMoveSectionUp ($bdd, $server, $section)	{
	if (!doCurrentUserHavePermission ('shop.editItems'))
		return "Vous n'avez pas la permission de modifier la boutique.";
	if (!shopSectionExists ($bdd, $section))
		return 'La section n\'existe pas.';

	if (($to = shopGetSectionBefore ($bdd, $server, $section)) == 0)
		return 'La section ne peut pas être déplacée.';

	return shopSwapSections ($bdd, $section, $to);
}
function shopMoveSectionDown ($bdd, $server, $section)	{
	if (!doCurrentUserHavePermission ('shop.editItems'))
		return "Vous n'avez pas la permission de modifier la boutique.";
	if (!shopSectionExists ($bdd, $section))
		return 'La section n\'existe pas.';

	if (($to = shopGetSectionAfter ($bdd, $server, $section)) == 0)
		return 'La section ne peut pas être déplacée.';

	return shopSwapSections ($bdd, $section, $to);
}
function shopGetSectionBefore ($bdd, $server, $section)	{
	/* Getting the section's order */
	$req = $bdd->prepare ('SELECT `order` FROM `elenos_shop_sections` WHERE `id` = :id');
	$req->execute (array ('id' => $section));
	$order = $req->fetch ();
	$order = $order['order'];
	$req->closeCursor ();

	$req = $bdd->prepare ('SELECT * FROM `elenos_shop_sections` WHERE `order` < :order AND `server` = :srv ORDER BY `order` DESC LIMIT 1');
	$req->execute (array ('order' => $order, 'srv' => $server));
	$rc = $req->rowCount ();
	$data = $req->fetch ();

	if ($rc <= 0)
		return 0;
	else
		return $data['id'];
}
function shopGetSectionAfter ($bdd, $server, $section)	{
	/* Getting the section's order */
	$req = $bdd->prepare ('SELECT `order` FROM `elenos_shop_sections` WHERE `id` = :id');
	$req->execute (array ('id' => $section));
	$order = $req->fetch ();
	$order = $order['order'];
	$req->closeCursor ();

	$req = $bdd->prepare ('SELECT * FROM `elenos_shop_sections` WHERE `order` > :order AND `server` = :srv ORDER BY `order` LIMIT 1');
	$req->execute (array ('order' => $order, 'srv' => $server));
	$rc = $req->rowCount ();
	$data = $req->fetch ();

	if ($rc <= 0)
		return 0;
	else
		return $data['id'];
}

function shopSwapSections ($bdd, $section1, $section2)	{
	if (!doCurrentUserHavePermission ('shop.editItems'))
		return "Vous n'avez pas la permission de modifier la boutique.";

	if (!shopSectionExists ($bdd, $section1) || !shopSectionExists ($bdd, $section2))
		return 'Les sections sélectionnées n\'existent pas.';

	$req = $bdd->prepare ('UPDATE
							`elenos_shop_sections` AS `sec1`
    						JOIN `elenos_shop_sections` AS `sec2` ON ( `sec1`.`id` = :id1 AND `sec2`.`id` = :id2 )
						   SET
    						`sec1`.`order` = `sec2`.`order`,
    						`sec2`.`order` = `sec1`.`order`
    					;');
	$req->execute (array ('id1' => $section1, 'id2' => $section2));
	$rc = $req->rowCount ();
	$req->closeCursor ();

	if ($rc <= 0)
		return 'Erreur interne.';
	else
		return 'ok';
}

function shopBuyTokens ($bdd)	{
	beginPage ();

	if (isUserLoggedIn ())
		echo buildUserTokenCount (getCurrentUserTokens ());
	else
		echo buildUserNotLoggedIn ();

	/* Getting the list of tokens */
	$itemList = array ();
	$req = $bdd->query ('SELECT `id`, `name`, `display`, `price_paypal` AS `price` FROM `elenos_shop_tokens`');
	while ($data = $req->fetch ())
		$itemList[$data['id']] = array('name' => $data['name'], 'display' => $data['display'], 'price' => $data['price']);
	$req->closeCursor ();

	echo buildTokenShop ($itemList);

	endPage ();
}
function shopGetTokenInfo ($bdd, $name)	{
	$req = $bdd->prepare ('SELECT * FROM `elenos_shop_tokens` WHERE `name` = :name');
	$req->execute (array ('name' => $name));
	$ret = $req->fetch ();
	$req->closeCursor ();
	return $ret;
}

function shopSendTokens ($bdd)	{
	beginPage ();

	if (isUserLoggedIn ())
		echo buildUserTokenCount (getCurrentUserTokens ());
	else
		echo buildUserNotLoggedIn ();

	beginSection (0, "Envoyer des tokens", -1);
	echo bbcodeParse ("[center]Vous voulez partager vos tokens avec un ami ? Vous pouvez lui faire un cadeau en les lui envoyant ![/center]", 
		false, true);
	echo '<form action="membre.php?sendTokens&amp;fromShop" method="post"><table><tr><td><label for="send_user">Votre ami : </label></td><td>&nbsp;
		<input type="text" name="send_user" id="send_user" class="textfield" placeholder="Pseudo du destinataire" style="min-width: 150px;" /></td>
		<td>&nbsp; &nbsp;</td><td><label for="send_tokens">Tokens à envoyer : </label></td><td>&nbsp; <input type="number" name="send_tokens" 
		id="send_tokens" value="1" min="1" max="'.getCurrentUserTokens ().'" class="textfield" style="max-width: 200px;" /></td></tr></table>
		<p class="text_right"><button type="submit" name="send" title="Envoyer" class="push_button_normal"><img src="images/RightArrow.png" 
		alt="right_icon" class="icon icon_left" /> Faire cadeau !</button></p></form>';
	echo '<p class="thin_info">Protip : vous ne vous souvenez plus du pseudo de votre ami ou avez un doute sur son orthographe ? Visitez la <a 
		href="membre.php">liste des membres</a>, vous pouvez lui envoyer des tokens directement depuis son profil.</p>';
	endSection ();

	endPage ();
}

?>