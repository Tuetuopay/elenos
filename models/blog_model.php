<?php

if (!isset ($postsPerPage))
	$postsPerPage = 5;

include_once ('common.php');
include_once ('views/blog_view.php');
include_once ('bbcode_parser.php');

/* Give <= 0 to display with no limit */
function blogHomePage ($bdd, $firstArticle)	{
	global $postsPerPage;

	beginPage ();

	$req = NULL;

	/* All blog entries */
	if ($firstArticle > 0)	{
		$req = $bdd->prepare ('SELECT * FROM `elenos_news` WHERE `id` <= :first ORDER BY `id` DESC LIMIT '.$postsPerPage);
		$req->bindParam (':first', $firstArticle);
		$req->execute ();
	} else	{
		$req = $bdd->prepare ('SELECT * FROM `elenos_news` ORDER BY `id` DESC LIMIT '.$postsPerPage);
		$req->execute ();
	}

	$data = array ();

	while ($data = $req->fetch ())	{
		echo buildArticle ($data['id'], $data['title'], bbcodeParse($data['contents'], true), 
						   $data['authorName'], $data['date'], blogCountArticleComments ($bdd, $data['id']));
		if ($firstArticle <= 0)	$firstArticle = $data['id'];
		$lastArticle = $data['id'];
	}
	$req->closeCursor ();

	echo buildNavigationButtons (blogSkipNArticles ($bdd, $firstArticle, $postsPerPage), blogGetArticleBeforeID ($bdd, $lastArticle));

	endPage ();
}

function blogPostDetail ($bdd, $articleID, $answerTo)	{
	beginPage ();

	$req = $bdd->prepare ('SELECT * FROM elenos_news WHERE id = :id');
	$req->execute (array ('id' => $articleID));
	$articleContent = $req->fetch ();
	$req->closeCursor ();

	$commentCount = blogCountArticleComments ($bdd, $articleID);

	echo buildArticle ($articleContent['id'], $articleContent['title'], bbcodeParse($articleContent['contents'], true),
					   $articleContent['authorName'], $articleContent['date'], $commentCount);


	beginComments ($commentCount);
	blogCommentAnswers ($bdd, $articleID, 0, $answerTo);
	if ($answerTo == 0 && (doCurrentUserHavePermission ('blog.postComment') || !isUserLoggedIn()))
		echo buildCommentReplyForm ($articleContent['id'], 0);
	endComments ();

	endPage ();
}
function blogCommentAnswers ($bdd, $postID, $commentID, $answerTo)	{
	$commentReq = $bdd->prepare ('SELECT * FROM `elenos_blog_comments` WHERE postID = :postID AND commentID = :answerID ORDER BY id DESC');
	$commentReq->execute (array ('postID' => $postID, 'answerID' => $commentID));
	while ($commentData = $commentReq->fetch ())	{
		beginComment ();
		echo buildComment ($postID, $commentData['id'], $commentData['author'], getUserAvatarURL ($bdd, $commentData['author']),
			bbcodeParse($commentData['commentContents'], true), $commentData['date']);
		if ($commentData['id'] == $answerTo && (doCurrentUserHavePermission ('blog.postComment') || !isUserLoggedIn()))
			echo buildCommentReplyForm ($postID, $answerTo);
		blogCommentAnswers ($bdd, $postID, $commentData['id'], $answerTo);
		endComment ();
	}
	$commentReq->closeCursor ();
}
function blogCountArticleComments ($bdd, $articleID)	{
	$req = $bdd->prepare ('SELECT COUNT(*) AS cnt FROM `elenos_blog_comments` WHERE `postID` = :id');
	$req->execute (array ('id' => $articleID));
	$ret = $req->fetch ();
	$ret = $ret['cnt'];
	$req->closeCursor ();
	return $ret;
}
function blogNewArticleEditor ($bdd)	{
	beginPage ();

	buildNewArticleForm ();

	endPage ();
}

function blogGetArticleBeforeID ($bdd, $id)	{
	$req = $bdd->prepare ('SELECT `id` FROM `elenos_news` WHERE `id` < :id ORDER BY `id` DESC LIMIT 1');
	$req->execute (array ('id' => $id));
	$ret = $req->fetch ();
	$req->closeCursor ();
	return $ret['id'];
}
function blogGetArticleAfterID ($bdd, $id)	{
	$req = $bdd->prepare ('SELECT `id` FROM `elenos_news` WHERE `id` > :id ORDER BY `id` LIMIT 1');
	$req->execute (array ('id' => $id));
	$ret = $req->fetch ();
	$req->closeCursor ();
	return $ret['id'];
}
function blogSkipNArticles ($bdd, $id, $n)	{
	$req = $bdd->prepare ('(SELECT `id` FROM `elenos_news` WHERE `id` > :id ORDER BY `id` LIMIT :n) ORDER BY `id` DESC LIMIT 1');
	$req->bindParam (':n', intval($n), PDO::PARAM_INT);
	$req->bindParam (':id', $id);
	$req->execute ();
	$ret = $req->fetch ();
	$req->closeCursor ();
	return $ret['id'];
}

function blogSaveComment ($bdd, $postID, $replyID, $pseudo, $email, $content)	{
	$result = '';
	$email = strtolower ($email);
	if (!isUserLoggedIn () && doUserExists($bdd, $pseudo))
		return 'Ce n\'est pas bien de voler l\'identité de '.$pseudo.' !';
	if (strlen($pseuo) > 255)
		$result .= 'Votre pseuo est trop long (255 caractères max). ';
	if ($pseudo == '')
		$result .= 'Vous devez spécifier un pseudo. ';
	if ($content == '')
		$result .= 'Écrivez un commentaire pour commenter !';

	if (!preg_match('#^[a-z0-9._-]+@[a-z0-9._-]{2,}\\.[a-z]{2,4}$#', $email))
		$result .= 'Vous devez spécifier une adresse email valide. ';

	if ($result != '')
		return $result;

	$req = $bdd->prepare ('INSERT INTO `elenos_blog_comments` (`postID`, `commentID`, `author`, `email`, `commentContents`, `date`) 
						   VALUES (:postID, :commentID, :pseudo, :email, :content, NOW())');
	$req->execute (array ('postID' => $postID,
						  'commentID' => $replyID,
						  'pseudo' => $pseudo,
						  'email' => $email,
						  'content' => $content));
	$rc = $req->rowCount ();
	$req->closeCursor ();
	
	if ($rc != 0)
		return 'ok';
	else
		return 'Erreur interne.';
}
function blogSaveArticle ($bdd, $title, $contents)	{
	$req = $bdd->prepare ('INSERT INTO `elenos_news` (`title`, `contents`, `authorName`, `date`) VALUES(:title, :contents, :authorName, NOW())');
	$req->execute (array ('title' => $title, 'contents' => $contents, 'authorName' => getCurrentUserName ()));
	$req->closeCursor ();
	$req = $bdd->query ('SELECT `id` FROM `elenos_news` ORDER BY `id` DESC LIMIT 1');
	$data = $req->fetch ();

	if ($req->rowCount () == 0)
		return 0;
	else
		return $data['id'];
}
function blogDeleteArticle ($bdd, $id)	{
	$req = $bdd->prepare ('DELETE FROM `elenos_news` WHERE `id` = :id');
	$req->execute (array ('id' => $id));
	$count = $req->rowCount ();
	$req->closeCursor ();
	if ($count != 0)
		return 'ok';
	else
		return 'Erreur interne.';
}
function blogDeleteComment ($bdd, $id)	{
	$req = $bdd->prepare ('UPDATE `elenos_blog_comments` SET `commentContents` = \'[i]Commentaire supprimé.[/i]\' WHERE `id` = :id');
	$req->execute (array ('id' => $id));
	$count = $req->rowCount ();
	$req->closeCursor ();
	if ($count != 0)
		return 'ok';
	else
		return 'Erreur interne';
}

?>