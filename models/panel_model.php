<?php

include_once ('views/panel_view.php');
include_once ('login.php');
include_once 'bbcode_parser.php';

function is404($filename){
	$handle = curl_init($filename);
	curl_setopt($handle,CURLOPT_RETURNTRANSFER,true);
	$response = curl_exec($handle);
	$httpCode = curl_getinfo($handle,CURLINFO_HTTP_CODE);
	curl_close($handle);
	if($httpCode>= 200&&$httpCode<300){return false;}
	else{return true;}
}
function prettifySize ($size)	{
	$size = (int)$size;
	if ($size < 0)	return $size.' o';
	$i = 0;
	while ($size >= 1024 && $i <= 4)	{
		$size /= 1024;
		$i++;
	}
	$ext = array ("o", "Kio", "Mio", "Gio", "Tio");
	return floor($size).' '.$ext[$i];
}

function panelMainPage ($bdd)	{
	beginPage ();

	if (!isUserLoggedIn ())	{
		beginStandaloneSection ('Vous devez être connecté pour modifier votre compte ...', false);
		endStandaloneSection ();
		endPage ();
		return;
	}

	panelHistory ($bdd, false);

	beginStandaloneSection ('Informations générales', true);
	echo buildMainSettings ();
	endStandaloneSection ();

	if (doCurrentUserHavePermission ('general.editServers'))	{
		beginStandaloneSection ('Serveurs', true);
		beginServerList ();

		$servers = array ();

		$req = $bdd->query ('SELECT * FROM `elenos_servers` ORDER BY `id`');
		while ($srv = $req->fetch ())	{
			echo serverEntry ($srv['id'], $srv['name'], $srv['internalName'], $srv['port'], $srv['shopDisplay'], $srv['menuDisplay']);
			$servers[$srv['id']] = $srv['name'];
		}
		$req->closeCursor ();
		echo addServerEntry ();

		endServerList ();

		echo setLobbyForm ($servers, getLobbyServer ($bdd));
		endStandaloneSection ();

		beginStandaloneSection ("Upload de fichiers", false);
		echo '<br />'.bbcodeParse ("[title2]Vos fichiers[/title2]", false, true).'<br />';
		beginTable (array("URL locale", "URL externe", "Taille"), true);
		$req = $bdd->prepare ('SELECT * FROM `elenos_admin_upload` WHERE `username` = :username');
		$req->execute (array('username' => getCurrentUserName ()));
		while ($file = $req->fetch ())	{
			$file['file'] = urlencode($file['file']);
			/* OKAAAAAY another f*cking problem on OVH servers :
			 * Standard PHP behavior :	urlencode("test text") = "test%20text"
			 * OVH servers behavior :	urlencode("test text") = "test+text"
			 * So when dealing with uploaded files, that's quite embarrassing : leading to 404 ...
			 */
			$file['file'] = str_replace("+", "%20", $file['file']);
			echo tableEntry (array('up/'.$file['file'], 'http://'.getServerURL ().'/up/'.$file['file'], prettifySize ($file['size'])));
		}
		endTable ();
		echo bbcodeParse ("[thin]L'URL locale est l'URL à mettre directement dans le site (boutique, blog, etc...). L'URL externe est celle à utiliser".
			" dès que l'image ne sera pas sur ce site.[/thin]");

		echo bbcodeParse ("[hsep/][title2]Envoyer un fichier[/title2]", false, true).'<br />';
		echo buildImageUpload ();
		endStandaloneSection ();

		/* Historique Admin */
		/* Boutique */
		beginStandaloneSection ("Historique de la boutique &nbsp; <a href=\"panel.php?shopCSV\" title=\"Obtenir en CSV\">
			<img src=\"images/xls.png\" alt=\"icon\" class=\"icon\"></a>", true);
		beginTable (array("Article", "Serveur", "Pseudo", "Date"), true);
		$req = $bdd->query ('SELECT `hist`.*, `itm`.*, `srv`.`name` AS `server`
FROM `elenos_shop_history` AS `hist`, `elenos_shop_items` AS `itm`, `elenos_servers` AS `srv`, `elenos_shop_sections` AS `sec` 
WHERE `itm`.`id` = `hist`.`item` AND `srv`.`id` = `sec`.`server` AND `sec`.`id` = `itm`.`section` ORDER BY `date` DESC');
		while ($data = $req->fetch ())
			echo tableEntry (array ($data['label'], $data['server'], 
				'<a href="membre.php?u='.$data['player'].'">'.$data['player'].'</a>', $data['date']));
		endTable ();
		endStandaloneSection ();

		/* Tokens */
		beginStandaloneSection ("Historique des tokens &nbsp; <a href=\"panel.php?tokensCSV\" title=\"Obtenir en CSV\">
			<img src=\"images/xls.png\" alt=\"icon\" class=\"icon\"></a>", true);
		beginTable (array ("De", "À", "Mont.", "Date", "Jeton Transaction"), true);
		$req = $bdd->query ('SELECT `hist`.*, `itm`.* FROM `elenos_token_transact` AS `hist`, `elenos_shop_tokens` AS `itm` 
							 WHERE `itm`.`id` = `hist`.`itemID` ORDER BY `completionDate` DESC');
		while ($data = $req->fetch ())	{
			if ($data['method'] == "paypal")
				echo tableEntry (array("PayPal", '<a href="membre.php?u='.$data['user'].'">'.$data['user'].'</a>', $data['tokens']." tok.", $data['completionDate'], $data['token']));
			else if ($data['method'] == "starpass")
				echo tableEntry (array("StarPass", '<a href="membre.php?u='.$data['user'].'">'.$data['user'].'</a>', "10 tok.", $data['completionDate'], $data['token']));
			else if ($data['method'] == "user")
				echo tableEntry (array('<a href="membre.php?u='.$data['fromUser'].'">'.$data['fromUser'].'</a>', '<a href="membre.php?u='.$data['user'].'">'.$data['user'].'</a>', $data['token']." tok.", $data['completionDate'], ''));
		}
		endTable ();
		endStandaloneSection ();
	}

	endPage ();
}
function panelFullHistory ($bdd)	{
	beginPage ();

	if (!isUserLoggedIn ())	{
		beginStandaloneSection ('Vous devez être connecté pour modifier votre compte ...', false);
		endStandaloneSection ();
		endPage ();
		return;
	}

	echo buildReturnButton ();

	panelHistory ($bdd, true);

	endPage ();
}

function panelHistory ($bdd, $full)	{
	beginStandaloneSection ('Historique de vos achats', true);
	if ($full)	{
		beginStandaloneSection ('Boutique', true);
		beginHistory ('');
	}
	else
		beginHistory ('Boutique');

	$sql = 'SELECT `hist`.`date`, `hist`.`id`, `items`.`label`, `items`.`price` 
			FROM `elenos_shop_history` AS `hist`, `elenos_shop_items` AS `items`
			WHERE UPPER(`player`) = UPPER(:name) AND `hist`.`item` = `items`.`id` ORDER BY `hist`.`date`';
	if (!$full)	$sql .= 'DESC LIMIT 4';
	$req = $bdd->prepare ($sql);
	$req->execute (array ('name' => getCurrentUserName ()));

	while ($data = $req->fetch ())
		echo historyItem ($data['date'], $data['label'], $data['price'].' tokens');
	$req->closeCursor ();

	endHistory ();
	if ($full)	endStandaloneSection ();
	else		echo '<div class="horizontal_separator"></div>';

	if ($full)	{
		beginStandaloneSection ('Achat de tokens', true);
		beginHistory ('');
	}
	else	beginHistory ('Achat de tokens');

	$sql = 'SELECT `tk`.`completionDate`, `tk`.`method`, `shop`.`display` FROM `elenos_token_transact` AS `tk`, `elenos_shop_tokens` AS `shop` 
			WHERE UPPER(`tk`.`user`) = UPPER(:user) AND `shop`.`id` = `tk`.`itemID` ORDER BY `tk`.`completionDate`';
	if (!$full)	$sql .= 'LIMIT 4';
	$req = $bdd->prepare ($sql);
	$req->execute (array('user' => getCurrentUserName ()));
	while ($data = $req->fetch ()){
		if ($data['method'] == 'paypal')
			echo historyItem ($data['completionDate'], $data['display'], 'PayPal');
		else if ($data['method'] == 'starpass')
			echo historyItem ($data['completionDate'], '10 tokens', '1 code StarPass');
	}
	$req->closeCursor ();

	endHistory ();
	if ($full)	endStandaloneSection ();
	else		echo buildFullHistoryButton ();

	endStandaloneSection ();
}

function panelSetAvatarURL ($bdd, $url)	{
	if (!isUserLoggedIn ())
		return 'Vous devez être connecté pour éditer votre avatar.';

	if ($url == getCurrentUserAvatarURL ())
		return 'ok';

	if (!preg_match("#(\.png|\.jpg|\.jpeg|\.gif|\.svg)$#i", $url))
		return 'Format invalide. Formats acceptés : PNG, JPG, GIF, SVG.';

	if (is404($url) && preg_match("#^http#i", $url))
		return 'Le lien semble mort.';

	$req = $bdd->prepare ('UPDATE `elenos_users` SET `avatarURL` = :url WHERE UPPER(`username`) = UPPER(:name)');
	$req->execute (array ('url' => $url, 'name' => getCurrentUserName ()));
	$rc = $req->rowCount ();
	$req->closeCursor ();

	if ($rc != 0)
		return 'ok';
	else
		return 'Erreur interne, réessayez plus tard.';
}
function panelSetAvatarFile ($bdd, $file)	{
	if (!isUserLoggedIn ())
		return 'Vous devez être connecté pour éditer votre avatar.';

	if ($file['error'] != 0)	{

		$errorText = array(UPLOAD_ERR_INI_SIZE => 'Le fichier trop gros.',
						   UPLOAD_ERR_FORM_SIZE => 'Le fichier excède la taille maximale.',
						   UPLOAD_ERR_PARTIAL => 'Le transfert a échoué. Merci de réessayer.',
						   UPLOAD_ERR_NO_FILE => 'Rentrez un fichier à uploader.',
						   UPLOAD_ERR_NO_TMP_DIR => 'Erreur interne. Code : UPLOAD_ERR_NO_TMP_DIR',
						   UPLOAD_ERR_CANT_WRITE => 'Erreur interne. Code : UPLOAD_ERR_CANT_WRITE',
						   UPLOAD_ERR_EXTENSION => 'Erreur interne. Code : UPLOAD_ERR_EXTENSION');
		return 'Erreur d\'upload : '.$errorText[$file['error']];
	}
	if ($file['size'] > 102400)
		return 'Le fichier est trop gros. Taille maximale : 100Ko.';

	if (!preg_match("#(\.png|\.jpg|\.jpeg|\.gif|\.svg)$#i", $file['name']))
		return 'Format invalide. Form ats acceptés : PNG, JPG, GIF, SVG.';

	$p = pathinfo ($file['name']);
	$p = $p['extension'];
	$dst = 'avatar/'.getCurrentUserName ().'.'.$p;
	if (!move_uploaded_file($file['tmp_name'], $dst))
		return 'Erreur interne. Code : CANT_MOVE from "'.$file['tmp_name'].'" to '.$dst;

	return panelSetAvatarURL ($bdd, $dst);
}
function panelSetPassword ($bdd, $cur, $new, $new2)	{
	if (!isUserLoggedIn ())
		return 'Vous devez être connecté pour changer votre mot de passe.';

	if (!canUserLogin ($bdd, getCurrentUserName (), $cur))
		return 'Votre mot de passe actuel est incorrect.';

	if ($new != $new2)
		return 'Les mots de passe sont différents.';

	$hashed = hashPassword (getCurrentUserName (), $new);
	$req = $bdd->prepare ('UPDATE `elenos_users` SET `hashedPassword` = :hash WHERE UPPER(`username`) = UPPER(:username)');
	$req->execute (array ('hash' => $hashed, 'username' => getCurrentUserName ()));
	$rc = $req->rowCount ();
	$req->closeCursor ();

	if ($rc != 0)
		return 'ok';
	else
		return 'Erreur interne, réessayez plus tard.';
}

function panelEditServer ($bdd, $id, $name, $iname, $port, $displayShop, $displayMenu)	{
	if (!isUserLoggedIn ())
		return 'Vous devez être connecté pour modifier les serveurs.';
	if (!doCurrentUserHavePermission ('general.editServers'))
		return 'Vous n\'avez pas le droit de modifier les serveurs.';
	if (!doServerExists ($bdd, $id))
		return 'Le serveur n°'.$id.' n\'existe pas.';

	if ($displayShop == "on")	$shopDisplay = (int)1;
	else				$shopDisplay = (int)0;
	if ($displayMenu == "on")	$menuDisplay = (int)1;
	else				$menuDisplay = (int)0;
	$port = (int)$port;

	$req = $bdd->prepare ('UPDATE `elenos_servers`
						   SET `name` = :name,
							   `internalName` = :iname,
							   `port` = :port,
							   `shopDisplay` = :shopDisplay,
							   `menuDisplay` = :menuDisplay
						   WHERE `id` = :id');
	$req->execute (array ('id' => $id,
						  'name' => $name,
						  'iname' => $iname,
						  'port' => $port,
						  'shopDisplay' => $shopDisplay,
						  'menuDisplay' => $menuDisplay));
	$rc = $req->rowCount ();
	$req->closeCursor ();

	if ($rc != 0)
		return 'ok';
	else
		return 'Erreur interne, réessayez plus tard.';
}
function panelDeleteServer ($bdd, $id)	{
	if (!isUserLoggedIn ())
		return 'Vous devez être connecté pour supprimer un serveur.';
	if (!doCurrentUserHavePermission ('general.editServers'))
		return 'Vous n\'avez pas le droit de supprimer un serveur.';
	if (!doServerExists ($bdd, $id))
		return 'Le serveur n°'.$id.' n\'existe pas.';

	$req = $bdd->prepare ('DELETE FROM `elenos_servers` WHERE `id` = :id');
	$req->execute (array ('id' => $id));
	$rc = $req->rowCount ();
	$req->closeCursor ();

	if ($rc != 0)
		return 'ok';
	else
		return 'Erreur interne, réessayez plus tard.';
}
function panelAddServer ($bdd, $name, $iname, $port, $displayShop, $displayMenu)	{
	if (!isUserLoggedIn ())
		return 'Vous devez être connecté pour ajouter un serveur.';
	if (!doCurrentUserHavePermission ('general.editServers'))
		return 'Vous n\'avez pas le droit de rajouter un serveur.';

	if ($displayShop == "on")	$shopDisplay = (int)1;
	else				$shopDisplay = (int)0;
	if ($displayMenu == "on")	$menuDisplay = (int)1;
	else				$menuDisplay = (int)0;
	$port = (int)$port;

	$req = $bdd->prepare ('INSERT INTO `elenos_servers`(`name`, `internalName`, `port`, `shopDisplay`, `menuDisplay`)
						   VALUES (:name, :iname, :port, :shopDisplay, :menuDisplay)');
	$req->execute (array ('name' => $name, 'iname' => $iname, 'port' => $port, 'shopDisplay' => $shopDisplay,
						  'menuDisplay' => $menuDisplay));
	$rc = $req->rowCount ();
	$req->closeCursor ();

	if ($rc != 0)
		return 'ok';
	else
		return 'Erreur interne, réessayez plus tard.';
}
function panelSetLobby ($bdd, $server)	{
	if (!isUserLoggedIn ())
		return 'Vous devez être connecté pour ajouter un serveur.';
	if (!doCurrentUserHavePermission ('general.editServers'))
		return 'Vous n\'avez pas le droit de définir le lobby.';

	$bdd->query ('UPDATE `elenos_servers` SET `isLobby` = 0');
	PDOQuery ($bdd, 'UPDATE `elenos_servers` SET `isLobby` = 1 WHERE `id` = :id', 'id', $server);

	return 'ok';
}

function panelSendFile ($bdd, $file)	{
	if (!isUserLoggedIn ())
		return 'Vous devez être connecté pour envoyer un fichier.';
	if (!doCurrentUserHavePermission ('general.editServers'))
		return "Vous n'avez pas le droit d'envoyer un fichier.";

	if ($file['error'] != 0)	{

		$errorText = array(UPLOAD_ERR_INI_SIZE => 'Le fichier trop gros.',
						   UPLOAD_ERR_FORM_SIZE => 'Le fichier excède la taille maximale.',
						   UPLOAD_ERR_PARTIAL => 'Le transfert a échoué. Merci de réessayer.',
						   UPLOAD_ERR_NO_FILE => 'Rentrez un fichier à uploader.',
						   UPLOAD_ERR_NO_TMP_DIR => 'Erreur interne. Code : UPLOAD_ERR_NO_TMP_DIR',
						   UPLOAD_ERR_CANT_WRITE => 'Erreur interne. Code : UPLOAD_ERR_CANT_WRITE',
						   UPLOAD_ERR_EXTENSION => 'Erreur interne. Code : UPLOAD_ERR_EXTENSION');
		return 'Erreur d\'upload : '.$errorText[$file['error']];
	}

	$dst = 'up/'.$file['name'];
	if (!move_uploaded_file($file['tmp_name'], $dst))
		return 'Erreur interne. Code : CANT_MOVE from "'.$file['tmp_name'].'" to '.$dst;

	$req = $bdd->prepare ('INSERT INTO `elenos_admin_upload`(`username`, `file`, `size`) VALUES(:username, :file, :size)');
	$req->execute (array ('username' => getCurrentUserName (), 'file' => $file['name'], 'size' => $file['size']));

	return 'ok';
}

function doServerExists ($bdd, $id)	{
	return PDOQuery ($bdd, 'SELECT COUNT(`id`) AS `var` FROM `elenos_servers` WHERE `id` = :var', 'var', $id);
}

?>