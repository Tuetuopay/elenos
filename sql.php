<?php
include_once 'bdd_connect.php';

function error ($message)	{
	return array ('success' => false, 'message' => $message);
}

$value = '';
if (isset ($_POST['file']))	{
	$file = $_POST['file'];
	$value = ' value="'.$file.'"';

	if (file_exists ($file))	{
		$contents = file_get_contents ($file);
		$replace = (isset ($_POST['comma']) && !empty ($_POST['comma'])) ? $_POST['comma'] : '#comma#';

		if (!empty ($contents))	{
			if (isset ($_POST['escape']))	{
				$final = '';
				$isInString = false;
				$strDelim = array ('"', '\'');
				$length = strlen ($contents);
				for ($i = 0; $i < $length; $i++)	{
					$char = $contents[$i];
					if (in_array ($char, $strDelim))
						$isInString = !$isInString;
					else if ($char == ';' && $isInString)
						$char = $replace;
					$final .= $char;
				}
				$contents = $final;
			}
			$queries = split (';', $contents);
			$count = 0;
			foreach ($queries as $query)	{
				if (empty ($query))	continue;
				$query = str_replace ($replace, ';', $query);
				$bdd->query ($query);
				$count++;
			}
			$info = array ('success' => true, 'message' => 'Executed '.$count.' querie(s).');
		}
		else
			$info = error ('File is empty.');
	}
	else
		$info = error ('File not found.');
}

?>

<!DOCTYPE html>
<html>
	<head>
		<title>SQL</title>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Exécuter un fichier .sql (ou autre) stocké localement.</h2>
		<form method="post" action="sql">
			<label for="file">Fichier : </label>
			<input type="text" name="file" id="file" style="width: 200px; padding: 3px;"<?php echo $value; ?>><br>
			<input type="checkbox" name="escape" id="escape" <?php if (isset ($_POST['escape'])) echo 'checked'; ?>>
			<label for="escape">Échapper les &quot;;&quot;</label>
			(<label for="comma">Remplacement du ";" : </label><input type="text" name="comma" id="comma" style="padding: 3px;">)<br>
			<input type="submit" value="Exécuter">
		</form>
		<h2>Résultat :</h2>
		<pre><code><?php var_dump ($info); ?></code></pre>
		<h2>Code échappé :</h2>
		<pre><code><?php echo $contents; ?></code></pre>
	</body>
</html>