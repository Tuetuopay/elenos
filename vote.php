<?php

include_once 'session.php';

include_once 'common.php';
include_once 'bbcode_parser.php';
include_once 'models/vote_model.php';

$page_name = 'Vote';

function redirectAndExit ()	{
	header('Location: vote.php');
	exit;
}

// Executing functions
if (isset ($_GET['vote']))	{
	$ret = voteOnSite ($bdd, $_GET['vote']);
	if ($ret != 'ok')	{
		$_SESSION['voteResult'] = $ret;
		redirectAndExit ();
	}
	else
		header('Location: '.voteGetSiteURL ($bdd, $_GET['vote']));
}
if (isset($_GET['buy']))	{
	$_SESSION['buyItemResult'] = voteBuyItem ($bdd, $_GET['buy']);
	redirectAndExit ();
}
if (isset($_GET['editItem']))	{
	$_SESSION['editItemResult'] = voteEditItem ($bdd, $_GET['editItem'], $_POST['name'], $_POST['price'], $_POST['server'], $_POST['command']);
	redirectAndExit ();
}
if (isset($_GET['addItem']))	{
	$_SESSION['addItemResult'] = voteAddItem ($bdd, $_POST['name'], $_POST['price'], $_POST['server'], $_POST['command']);
	redirectAndExit ();
}
if (isset($_GET['deleteItem']))	{
	$_SESSION['deleteItemResult'] = voteDeleteItem ($bdd, $_GET['deleteItem']);
	redirectAndExit ();
}

include_once ('header.php');

if ($_SESSION['loginError'] != '')	{
	printErrorMessage ($_SESSION['loginError']);
	$_SESSION['loginError'] = '';
}

// checkSessionExecuteMessage ('session', 'ok', "ToDisplay");
checkSessionExecuteMessage ('voteResult', 'ok', '');
checkSessionExecuteMessage ('buyItemResult', 'ok', "Vous avez été récompensé !");
checkSessionExecuteMessage ('editItemResult', 'ok', "Récompense éditée.");
checkSessionExecuteMessage ('addItemResult', 'ok', "Récompense ajoutée.");
checkSessionExecuteMessage ('deleteItemResult', 'ok', "Récompense suppriée.");

/* voteMainPage ($bdd); */

voteMainPage ($bdd);

include ('right_menu.php');
include ('footer.php');

?>