<?php

function beginComments ($count)	{
	echo '<div id="comments"><h3>'.$count.' commentaires</h3>';
}
function endComments ()	{
	echo '</div>';
}
function beginComment ()	{
	echo '<div class="comment">';
}
function endComment ()	{
	echo '</div>';
}

function buildArticle ($id, $title, $contents, $authorName, $date, $commentsCount)	{
	$ret = '<section class="blogPost"><header><a href="blog.php?view='
		.$id.'"><h2>'
		.$title.'</h2></a></header><article>'
		.$contents.'</article><div class="horizontal_separator"></div><footer><p>';
	if (doCurrentUserHavePermission ('blog.deleteArticle'))
		$ret .= '<a href="blog.php?deleteArticle='.$id.'">Supprimer</a> &middot; ';
	$ret .= 'Le '.$date.' par <a href="membre.php?u='.$authorName.'">'
		.$authorName.'</a></p><a href="blog.php?view='.$id.'#comments" class="comments_count"><p><img src="images/comment.png" alt="CommentIcon" /> '
		.$commentsCount.'</p></a></footer></section>'."\n";
	return $ret;
}
function buildComment ($postID, $commentID, $authorName, $authorAvatar, $contents, $date)	{
	$ret = '<p><a href="membre.php?u='.$authorName.'"><img src="'.$authorAvatar.'" alt="UserAvatar" /></a></p><h4><a href="membre.php?u='.$authorName.'">
		'.$authorName.'</a></h4>'.$contents.'<p class="comment_subtitle">Le '.$date.
	' &middot; <a href="blog.php?view='.$postID.'&amp;answerTo='.$commentID.'#main_reply_box">Répondre</a>';
	if (doCurrentUserHavePermission ('blog.deleteComment') || getCurrentUserName () == $authorName)
		$ret .= ' &middot; <a href="blog.php?view='.$postID.'&amp;deleteComment='.$commentID.'">Supprimer</a>';
	$ret .= '</p>';
	return $ret;
}

function buildNavigationButtons ($prevID, $nextID)	{
	$ret = '<p id="nav_buttons">';
	if (!is_null($prevID))
		$ret .= '<a href="blog.php?first='.$prevID.'" class="push_button_normal"><span class="nav_arrow">&#8592;</span> &nbsp; Page précédente</a>';
	$ret .= ' ';
	if (!is_null($nextID))
		$ret .= '<a href="blog.php?first='.$nextID.'" class="push_button_normal">Page suivante &nbsp; <span class="nav_arrow">&#8594;</span></a>';
	$ret .= '</p>';
	return $ret;
}

function buildCommentReplyForm ($articleID, $commentID)	{
	$ret = '<form method="post" action="blog.php?view='.$articleID.'" class="reply_box" id="main_reply_box">
		<input type="text" name="pseudo" id="input_pseudo'.$commentID.'" value="'.getCurrentUserName ().'" /> 
		<label for="input_pseudo'.$commentID.'">Votre pseudo</label><br />
		<input type="email" name="email" id="email'.$commentID.'" value="'.getCurrentUserEmail ().'" /> 
		<label for="email'.$commentID.'">Votre adresse email</label><br />
		<label for="comment'.$commentID.'">Votre commentaire :</label><br />
		<textarea name="comment" id="comment'.$commentID.'" rows="10" cols="80"></textarea><br />
		<input type="hidden" name="commentID" value="'.$commentID.'" />
		<input type="submit" class="submit push_button_normal" value="Commenter">
	</form>';
	return $ret;
}

function buildNewArticleForm ()	{
	?>
	<section class="standalone_section"><header><h2>Nouveau billet</h2></header><article>
		<form action="blog.php?poster" method="post">
			<p class="formfield">
				<label for="article_title">Titre de l'article</label><br />
				<input type="text" name="article_title" id="article_title" placeholder="Titre" required="required" class="textfield" value="<?php echo $_POST['article_title'] ?>" />
			</p>
			<p class="formfield">
				<label for="article_content">Contenu même de l'article</label><br />
				<textarea name="article_content" id="article_content" placeholder="" required="required" class="textfield" rows="25"><?php echo $_POST['article_content'] ?></textarea>
			</p>
			<p class="formfield">
				<input type="submit" class="push_button_normal" value="Envoyer ce billet">
			</p>
		</form>
	</article></section>
	<?php
}

?>