<?php

function buildUserProfile ($username, $avatar, $rank, $rankIG, $op, $firstConnDate, $lastConnDate, $registerDate)	{
	$ret = '<div id="avatar_box" class=""><img src="'.$avatar.'" alt="avatar_'.$username.'" class="avatar" /></div>';
	$ret .= '<table id="user_profile">';
	$ret .= '<tr><td>Rang :</td><td>'.$rank.'</td></tr>';
	$ret .= '<tr><td colspan="2"><div class="horizontal_separator"></div></td></tr>';
	$ret .= '<tr><td>Grade :</td><td>'.$rankIG.$op.'</td></tr>';
	$ret .= '<tr><td colspan="2"><div class="horizontal_separator"></div></td></tr>';
	if ($firstConnDate != false)	{
		$ret .= '<tr><td>Première connexion sur le serveur :</td><td>Le '.$firstConnDate.'</td></tr>';
		$ret .= '<tr><td colspan="2"><div class="horizontal_separator"></div></td></tr>';
		$ret .= '<tr><td>Dernière connexion sur le serveur :</td><td>Le '.$lastConnDate.'</td></tr>';
	}
	else
		$ret .= '<tr><td colspan="2">'.$username.' ne s\'est jamais connecté sur le serveur.</td></tr>';
	$ret .= '<tr><td colspan="2"><div class="horizontal_separator"></div></td></tr>';
	if ($registerDate != false)
		$ret .= '<tr><td>Inscription sur le site :</td><td>Le '.$registerDate.'</td></tr>';
	else
		$ret .= '<tr><td colspan="2">'.$username.' ne s\'est pas encore inscrit sur le site.</td></tr>';
	$ret .= '</table>';
	return $ret;
}
function buildAdminForm ($username, $tokens, $ranks, $rank)	{
	$ret = '<div class="table"><div class="tbody">';
	$ret .= '<form class="tr" action="membre.php?editRank='.$username.'" method="post"><label for="user_rank" class="td">Rang : </label>
		<span class="td"><select name="user_rank" id="user_rank">';
	foreach ($ranks as $key => $value) {
		$ret .= '<option value="'.$key.'"';
		if ($key == $rank)
			$ret .= ' selected';
		$ret .= '>'.$value.'</option>';
	}
	$ret .= '</select></span><span class="td"><button type="submit" name="edit_rank" title="Modifier" class="push_button_normal"><img 
		src="images/edit.png" alt="edit_icon" class="icon icon_left" /> Modifier</button></span></form>';
	// $ret .= '<div class="tr"><div class=td" colspan="3"><div class="horizontal_separator"></div></div></div>';
	$ret .= '<form class="tr" action="membre.php?editTokens='.$username.'" method="post"><label for="user_tokens" class="td">Tokens : &nbsp;&nbsp;
		</label><span class="td"><input type="number" name="user_tokens" id="user_tokens" value="'.$tokens.'" class="textfield" /></span><span 
		class="td"><button type="submit" name="edit_tokens" title="Modifier" class="push_button_normal"><img src="images/edit.png" alt="edit_icon" 
		class="icon icon_left" /> Modifier</button></span></form>';
	$ret .= '</div></div>';
	return $ret;
}
function buildSendTokenForm ($username, $max)	{
	$ret = '<p>'.$username.' est votre ami ? Vous pouvez lui faire un cadeau en lui envoyant des tokens !</p>
		<form action="membre.php?sendTokens='.$username.'" method="post"><label for="send_tokens">Tokens à envoyer : &nbsp;&nbsp;
		</label><input type="number" name="send_tokens" id="send_tokens" value="1" min="1" max="'.$max.'" class="textfield" style="max-width: 200px;" />&nbsp;
		<button type="submit" name="edit_rank" title="Envoyer" class="push_button_normal"><img src="images/RightArrow.png" alt="right_icon" 
		class="icon icon_left" /> Faire cadeau !</button></form>';
	return $ret;
}

function buildLetterList ($charset, $selected)	{
	$arr = str_split ($charset.'#');
	$ret = '<div id="letter_list">';
	$ret .= '<a href="membre.php">Tous</a><a href="membre.php?l=rand">RAND()</a>';
	foreach ($arr as $value) {
		$ret .= '<a href="membre.php?l=';
		if ($value == '0')
			$ret .= urlencode('zero');
		else
			$ret .= urlencode ($value);
		$ret .= '"';
		if ((string)$value == (string)$selected)
			$ret .= ' class="selected"';
		$ret .= '>'.$value.'</a>';
	}
	$ret .= '</div>';
	return $ret;
}
function beginMemberList ()	{
	echo '<div class="member_list"><table>';
}
function endMemberList ()	{
	echo '</table></div>';
}
function beginMemberLine ()	{
	echo '<tr>';
}
function endMemberLine ()	{
	echo '</tr>';
}
function buildMemberEntry ($username, $avatarURL, $server)	{
	$ret = '<td><img src="'.$avatarURL.'" alt="avatar_'.$username.'" class="avatar_small" /><div><h2><a href="membre.php?u='.$username.'">'
	.$username.'</a></h2>';
	if ($server)	$ret .= '<span class="thin_info">Connecté au '.$server.'</span>';
	$ret .= '</div></td>';
	return $ret;
}

function buildMembreListButton ()	{
	return '<a href="membre.php" class="push_button_normal">&#8592; Liste des membres</a>';
}

?>