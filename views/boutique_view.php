<?php

include_once 'paymentAPI.php';

function beginSection ($server, $title, $id)	{
	$ret = '<div><h2 class="shopSectionTitle">'.$title.'</h2>';
	if (doCurrentUserHavePermission ('shop.editItems') && $id != -1)
		$ret .= popupWindowStyle ('section_'.$id).'<p><a href="#popup_section_'.$id.'" class="shopSectionDelete push_button_normal">
		<img src="images/delete.png" alt="delete" class="icon icon_left" /> Supprimer</a>'.
		buildPopupWindow ('section_'.$id, 'Confirmer la suppression : '.$title, '<br /><p>Voulez vous vraiment supprimer la section '.$title.' ?<br />
			<img src="images/warning.png" class="icon" alt="warning_icon" /> <span class="thin_info">Cette suppression est définitive.</span><br />
			<br /></p><a href="#empty" class="push_button_normal">Annuler</a><a href="boutique.php?srv='.$server.'&amp;deleteSection='.$id.'" 
			class="push_button_normal">Valider</a>');
	$ret .= '</div><section class="shopSection"><div>';
	echo $ret;
}
function endSection ()	{
	echo '</div></section>';
}

function buildUserTokenCount ($count)	{
	$ret = '<div id="token_count"><p>&middot; Crédit : '.$count.' tokens &middot;<br />&middot; <a href="boutique.php?buyTokens=display">Acheter</a>
		&middot;';
	if ((int)$count > 0)	$ret .= ' <a href="boutique.php?sendTokens">Envoyer</a> &middot; </p></div>';
	return $ret;
}
function buildUserNotLoggedIn ()	{
	return '<div id="token_count"><p>Veuillez vous connecter / <a href="register.php">inscrire</a>.</p></div>';
}

function buildProduct ($id, $label, $icon, $description, $price, $rawDesc = "", $commmand = "")	{
	$ret = '<div class="shop_item">'.popupWindowStyle ('item_'.$id).'<p class="item_price">'
		.$price.'</p><h3>'.$label.'</h3><p><span class="drop-shadow"><img src="'.$icon.'" alt="'.$label.'_icon" /></span></p>'.
		'<p><a href="#popup_item_'.$id.'" class="push_button_normal shop_item_btn"></a></p>';

	if (doCurrentUserHavePermission ('shop.editItems'))
		$ret .= '<p class="deleteItem"><a href="boutique.php?srv='.$_GET['srv'].'&amp;deleteItem='.$id.'" class="">
			<img src="images/delete.png" alt="delete" class="icon icon_left" /> Supprimer</a><br /><a href="#popup_edit_'.$id.'">
			<img src="images/edit.png" alt="delete" class="icon icon_left" /> Éditer</a></p><div>'.popupWindowStyle ('edit_'.$id).
			buildPopupWindow ('edit_'.$id, 'Éditer : '.$label, '<br /><form action="boutique.php?srv='.$_GET['srv'].'&amp;editItem='.$id.'" 
			method="post">
			<p class="formfield">
				<input type="text" name="edit_title" id="edit_title'.$id.'" placeholder="Nom" required="required" class="textfield" value="'.$label.'">
			</p>
			<p class="formfield">
				<input type="text" name="edit_img" id="edit_img'.$id.'" placeholder="URL de l\'image" required="required" class="textfield" value="'.$icon.'">
			</p>
			<p class="formfield">
				<textarea name="edit_desc" id="edit_desc'.$id.'" placeholder="Description de l\'objet" required="required" class="textfield" rows="3">'.$rawDesc.'</textarea>
			</p>
			<p class="formfield">
				<input type="number" name="edit_price" id="edit_price'.$id.'" placeholder="20" required="required" class="textfield" value="'.$price.'" />
			</p>
			<p class="formfield">
				<input type="text" name="edit_command" id="edit_command'.$id.'" placeholder="Commande à exécuter" required="required" class="textfield" value="'.$commmand.'" />
			</p>
			<p class="thin_info">Rappel : {player} = le nom du joueur, [{NEW}] = nouvelle commande.</p><br />
			<p class="formfield">
				<button type="submit" name="submit" title="Modifier" class="push_button_normal"><img src="images/edit.png" alt="edit_icon" 
				class="icon icon_left"> Modifier</button>
			</p>
			</form>')
			.'</div>';

	$ret .= buildPopupWindow ('item_'.$id, 'Confirmer l\'achat : '.$label, '<br />
		<p><span class="drop-shadow"><img src="'.$icon.'" alt="'.$label.'_icon" /></span></p><br />'.$description.'<br /><a href="#empty" 
	   class="push_button_normal" style="color:red;"><img src="images/false.png" alt="cancel" class="icon icon_left" /> Annuler</a>
	   <a href="boutique.php?srv='.$_GET['srv'].'&amp;buy='.$id.'" class="push_button_normal" style="color:green;"><img src="images/true.png" 
	   alt="confirm" class="icon icon_left" /> Valider</a>');
	$ret .= '</div>';
	return $ret;
}
function buildAddItemBox ($sectionID)	{
	$ret = '<div class="shop_item shopAddItem">';
	$ret .= '<form action="boutique.php?srv='.$_GET['srv'].'&amp;addItem" method="post">
			<p class="formfield">
				<input type="text" name="item_title" id="item_title'.$sectionID.'" placeholder="Nom" required="required" class="textfield" value="">
			</p>
			<p class="formfield">
				<input type="text" name="item_img" id="item_img'.$sectionID.'" placeholder="URL de l\'image" required="required" class="textfield" value="">
			</p>
			<p class="formfield">
				<textarea name="item_desc" id="item_desc'.$sectionID.'" placeholder="Description de l\'objet" required="required" class="textfield" rows="3"></textarea>
			</p>
			<p class="formfield">
				<input type="number" name="item_price" id="item_price'.$sectionID.'" placeholder="20" required="required" class="textfield" value="20" />
			</p>
			<p class="formfield">
				<input type="text" name="item_command" id="item_command'.$sectionID.'" placeholder="Commande à exécuter" required="required" class="textfield" />
			</p>
			<input type="hidden" name="sectionID" value="'.$sectionID.'." />
			<p class="formfield">
				<input type="submit" class="push_button_normal" value="Ajouter">
			</p>
		</form>';
	$ret .= '</div>';
	return $ret;
}
function buildAddSection ($server)	{
	$ret = '<h2 class="shopSectionTitle">
		<form action="boutique.php?srv='.$server.'&amp;addSection" method="post">
			<input type="text" name="section_title" placeholder="Nom" required="required" value="" />
			<input type="submit" value="Ajouter" />
		</form>
	</h2>';
	return $ret;
}
function buildServerList ($servers, $active)	{
	$ret = '<div class="aux_navbar"><ul>';
	foreach ($servers as $id => $name) {
		if ((int)$id == (int)$active)
			$ret .= '<li><a href="boutique.php?srv='.$id.'" class="menu_item_active">'.$name.'</a></li>';
		else
			$ret .= '<li><a href="boutique.php?srv='.$id.'">'.$name.'</a></li>';
	}
	$ret .= '</ul></div>';
	return $ret;
}

function buildPreServerList ($servers)	{
	$ret = '<h2 class="shopSectionTitle">Choisissez votre serveur</h2>';
	return $ret;
}

function buildTokenShop ($itemList)	{
	$ret = '<h2 class="shopSectionTitle">Acheter des Tokens</h2>';
	$ret .= '<form action="https://'.getBasePaypalURL().'/cgi-bin/webscr" method="post" id="tokenCheckoutForm">
				<input type="radio" name="payment" value="paypal" id="radio_paypal" checked /> 
				<label for="radio_paypal"><img src="images/paypal_logo.png" alt="Logo PayPal" class="payment_logo"/></label>
				<input type="radio" name="payment" value="starpass" id="radio_starpass" /> 
				<label for="radio_starpass"><img src="images/starpass.png" alt="Logo Starpass" class="payment_logo"/></label>

				<div id="form_paypal">
					<select name="amount" id="payment_option">
						<option value="none">Sélectionnez le contenu de votre offre ...</option>';
	foreach ($itemList as $key => $value)
		$ret .= '<option value="'.$value['price'].'">'.$value['display'].'</option>';
	$ret .= '</select><p class="formfield"><input type="submit" value="Continuer" class="push_button_normal"></p>
	<input name="currency_code" type="hidden" value="EUR">
				<input name="shipping" type="hidden" value="0.00">
				<input name="tax" type="hidden" value="0.00">
				<input name="return" type="hidden" value="http://'.getServerURL ().'/boutique.php?paypal=true">
				<input name="cancel_return" type="hidden" value="http://'.getServerURL ().'/boutique.php">
	 			<input name="notify_url" type="hidden" value="http://'.getServerURL ().'/boutique.php?paypalCheck">
				<input name="cmd" type="hidden" value="_xclick">
				<input name="business" type="hidden" value="'.getPaypalEmail ().'">
				<input name="item_name" type="hidden" value="Commande de Tokens sur Elenos">
				<input name="no_note" type="hidden" value="1">
				<input name="lc" type="hidden" value="FR">
				<input name="bn" type="hidden" value="PP-BuyNowBF">
				<input name="custom" type="hidden" value="u='.getCurrentUserName ().'">
			</div><div id="form_starpass">
<div id="starpass_252558"></div><script type="text/javascript" src="http://script.starpass.fr/script.php?idd=252558&amp;verif_en_php=1&amp;datas="></script><noscript>Veuillez activer le Javascript de votre navigateur s\'il vous pla&icirc;t.<br /><a href="http://www.starpass.fr/">Micro Paiement StarPass</a></noscript>
			</div>
			</form>';
	return $ret;
}

?>