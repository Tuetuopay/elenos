<?php

function buildVoteButton ($url, $img, $credits)	{
	$ret = '<div class="vote_button"><a href="'.$url.'"';
	if (isUserLoggedIn ())
		$ret .= ' target="_blank"';
	$ret .= '></a><div class="vote_button_container"><div><img src="'.$img.'" alt="server_logo" class="server_logo" /></div><div><p>'.$credits.
		' points</p></div></div></div>';
	return $ret;
}

function buildVoteEntry ($username, $avatarURL, $info, $rank)	{
	$ret = '<td><div class="vote_rank"><img src="'.$avatarURL.'" alt="avatar_'.$username.'" class="avatar_small" /><span class="vote_rank">'.$rank.
		'</span></div><div><h2><a href="membre.php?u='.$username.'">'.$username.'</a></h2>';
	if ($info)	$ret .= '<span class="thin_info">'.$info.'</span>';
	$ret .= '</div></td>';
	return $ret;
}

function buildProgressBar ($max, $value)	{
	return '<progress value="'.$value.'" max="'.$max.'"></progress>';
}

function buildVoteShop ($items, $points)	{
	$content = '<br /><table class="noborder filler">';
	$i = 0;
	foreach ($items as $name => $item)	{
		if ($i != 0)	$content .= '<tr><td colspan="4"><div class="horizontal_separator"></div></td></tr>';
		$i++;
		$content .= '<tr><td>'.$name.'<br /><span class="thin_info">'.$item['server'].'</span></td><td>'.
			buildProgressBar ($item['price'], $points).'</td><td>'.$points.' / '.$item['price'].' jetons</td><td>';
		if ((int)$points < (int)$item['price'])
			$content .= '<span class="thin_info">Vous n\'avez pas<br />assez de jetons !</span>';
		else
			$content .= '<a href="vote.php?buy='.$item['id'].'"class="push_button_normal">Recevoir</a>';
		$content .= '</td></tr>';
	}
	$content .= '</table><br /><form action="vote.php?buy=tokens" method="post">Je veux <input name="tokens" id="tokens" type="number" 
		value="1" min="0" max="'.(int)($points / 10).'" onchange="document.getElementById(\'tokenPrice\').innerHTML = this.value * '
		.getTokenPrice ().'" /> <label for="tokens">tokens</label> pour <span id="tokenPrice">10</span> jetons vote ! <input 
		type="submit" value="Recevoir" class="push_button_normal" /></form><br />
		<a href="#empty" class="push_button_normal">Annuler</a>';
	$ret = popupWindowStyle ('vote_shop').buildPopupWindow ('vote_shop', 'Récompenses', $content, true);
	return $ret;
}

function beginPrizeList ()	{
	echo '<div class="styled table"><div class="thead"><div class="tr"><span class="th">Nom</span><span class="th">Prix</span>
		<span class="th">Serveur</span><span class="th">Commande</span><span class="th">Actions</span></div>
	</div><div class="tbody">';
}
function endPrizeList ()	{
	echo '</div></div><span class="thin_info">Rappel : {player} = nom du joueur, {rec} = nom de la récompense, [{NEW}] = nouvelle commande.</span>';
}
function prizeEntry ($id, $name, $price, $server, $command, $servers)	{
	$ret = '<form action="vote.php?editItem='.$id.'" method="post" class="tr">
		<span class="td"><input type="text" name="name" value="'.$name.'" class="textfield" /></span>
		<span class="td"><input type="number" name="price" value="'.$price.'" class="textfield" /></span>
		<span class="td"><select name="server" id="server">';
	foreach ($servers as $key => $value)	{
		$ret .= '<option value="'.$key.'"';
		if ((int)$key == (int)$server)	$ret .= ' selected';
		$ret .= '>'.$value.'</option>';
	}
	$ret .= '</select></span>
		<span class="td"><input type="text" name="command" value="'.$command.'" class="textfield" /></span>
		<div class="td">'.popupWindowStyle ('item_'.$id).'
		<button type="submit" name="edit_'.$id.'" title="Modifier" class="push_button_normal"><img src="images/edit.png" 
		alt="edit_icon" class="icon" /></button><a href="#popup_item_'.$id.'" class="push_button_normal" title="Supprimer ce serveur">
		<img src="images/delete.png" alt="delete_icon" class="icon" /></a>';
	$ret .= buildPopupWindow ('item_'.$id, 'Confirmer la suppression : '.$name, '<br />
		<p>Voulez vous vraiment supprimer le serveur '.$name.' ?<br /><img src="images/warning.png" class="icon" alt="warning_icon" />
		<span class="thin_info">Cette suppression est définitive.</span><br /><br /><a href="#empty" class="push_button_normal">Annuler</a>
		<a href="vote.php?deleteItem='.$id.'" class="push_button_normal">Valider</a>');
	$ret .= '</div></form>';
	return $ret;
}
function addItemEntry ($servers)	{
	$ret = '<form action="vote.php?addItem" method="post" class="tr">
		<span class="td"><input type="text" name="name" class="textfield" placeholder="Nom" /></span>
		<span class="td"><input type="number" name="price" class="textfield" placeholder="Prix en points" /></span>
		<span class="td"><select name="server" id="server">';
	foreach ($servers as $key => $value)
		$ret .= '<option value="'.$key.'">'.$value.'</option>';
	$ret .= '</select></span>
		<span class="td"><input type="text" name="command" class="textfield" placeholder="Commande à exécuter" /></span>
		<span class="td"><button type="submit" name="add" title="Ajouter" class="push_button_normal"><img src="images/add.png" alt="add_icon" 
		class="icon" /></button></span>
		</form>';
	return $ret;
}

?>