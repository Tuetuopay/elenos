<?php

function beginTicketList ()	{
	echo '<table id="supportTicketList"><thead><tr><th>Statut</th><th>Sujet</th><th>Créé par</th><th>N°</th></tr></thead><tbody>';
}
function beginTicketDetail ($id, $title, $content, $status, $author, $avatar, $date)	{
	$ret = buildGoBackButton().'<table id="supportTicketDetail"><thead><tr><th><a href="membre.php?u='.$author.'">'.$author.
		'<div class="horizontal_separator"></div><img src="'.$avatar.'" alt="'.$author.'_avatar" class="avatar" /></a></th><th><h2>'.$title;
	if ($status != 'pending')
		$ret .= ' <span style="color: green">[Résolu]</span>';
	$ret .= '</h2>'.$content.'<p class="brcorner italic">Le '.$date.'</p></th></tr></thead><tbody>';
	echo $ret;
}
function endTicketList ()	{
	echo '</tbody></table>';
}

function buildSingleTicket ($id, $title, $content, $status, $author, $date, $answers)	{
	// <tr><td></td><td><div class="horizontal_separator"></div></td><td></td></tr>
	$answers = (int)$answers;
	if ($answers == 0)	$answers = 'Aucune';
	$ret2 = '<tr><td class="statusBox"><img src="images/';
	if ($status == 'pending')
		$ret2 .= 'false';
	else
		$ret2 .= 'true';
	$ret2 .= '.png" alt="Mark" /></td><td><a href="support.php?view='.$id.'">'.$title.'</a><span class="post_date">'.$answers.' réponse';
	if ($answers >= 2)
		$ret2 .= 's';
	$ret2 .= '</span></td><td class="infoBox"><p><a href="membre.php?u='.$author.'">'.$author.'</a><br /><span class="post_date">'.$date.'</span></p>
		</td><td class="post_id">#'.$id.'</td></tr>';
	return $ret2;
}
function buildTicketReply ($id, $content, $status, $author, $avatar, $date)	{
	$ret='<tr><td><a href="membre.php?u='.$author.'">'.$author.'<br /><img src="'.$avatar.'" alt="'.$author.'_avatar" class="avatar" /></a></td><td>'
		.$content.'<p class="brcorner italic">Le '.$date.'</p></td></tr>';
	return $ret;
}

function buildNewTicketForm ()	{
	$ret = '<p><a href="#new_ticket" class="push_button_normal"><img src="images/add.png" alt="add" class="icon icon_left" /> Nouveau ticket</a></p>';
	$ret .= '<div id="new_ticket" class="nodisplay"></div>';
	if (isUserLoggedIn ())
		$ret .= '<form method="post" action="support.php?newTicket" autocomplete="off" id="new_ticket_form">
				<p class="formfield">'.
	//					<label for="subject">Sujet</label><br />
					'<input type="text" name="subject" id="subject" required="required" placeholder="Sujet de votre demande..." class="textfield"><br />
				</p>
				<p class="formfield">
					<textarea name="content" id="content" placeholder="Décrivez ici pourquoi vous avez besoin d\'aide." required="required"
					 class="textfield" rows="8"></textarea>
				</p>
				<p class="formfield last">
					<input type="submit" class="submit push_button_normal" name="send_ticket" value="Envoyer" />
				</p>
			</form>';
	else
		$ret .= '<p id="new_ticket_form">Veuillez vous connecter pour poster un nouveau ticket.</p>';
	return $ret;
}
function buildNewReplyForm ($ticketID)	{
	$ret = '<p><a href="#new_answer" class="push_button_normal">Nouvelle réponse</a></p>';
	$ret .= '<div id="new_answer" class="nodisplay"></div>';
	if (isUserLoggedIn ())
		$ret .= '<form method="post" action="support.php?view='.$ticketID.'&amp;newAnswer" autocomplete="off" id="new_answer_form">
				<p class="formfield">
					<textarea name="content" id="content" placeholder="Votre réponse..." required="required"
					 class="textfield" rows="8"></textarea>
				</p>
				<p class="formfield last">
					<input type="submit" class="submit push_button_normal" name="send_answer" value="Envoyer" />
				</p>
			</form>';
	else
		$ret .= '<p id="new_answer_form">Veuillez vous connecter pour répondre à ce ticket.</p>';
	return $ret;
}
function buildValidateButtons ($ticketID)	{
	$ret = '<p id="validateButtons"><a href="support.php?view='.$ticketID.'&amp;validate" class="push_button_normal"><img src="images/true.png"
	 alt="validateImage" class="icon" /></a><a href="support.php?view='.$ticketID.'&amp;pend" class="push_button_normal"><img src="images/false.png"
	 alt="pendIcon" class="icon" /></a><a href="#popup_delete" class="push_button_normal"><img src="images/delete.png"
	 alt="deleteIcon" class="icon" /></a>';
	$ret .= popupWindowStyle ('delete').buildPopupWindow ('delete', 'Confirmer la suppression : ticket #'.$ticketID,
		'<p>Voulez vous vraiment supprimer le ticket #'.$ticketID.' ?<br /><img src="images/warning.png" class="icon" 
		alt="warning_icon" /> <span class="thin_info">Cette suppression est définitive.</span><br /><br /></p><a href="#empty" 
		class="push_button_normal">Annuler</a><a href="support.php?view='.$ticketID.'&amp;delete" class="push_button_normal">Valider</a>').
	'</p>';
	return $ret;
}
function buildViewPendingButton ()	{
	$ret = '<p id="validateButtons"><a href="support.php?pending" class="push_button_normal"><img src="images/false.png"
	 alt="pending_icon" class="icon icon_left" /> Tickets en attente</a></p>';
	return $ret;
}
function buildGoBackButton ()	{
	return '<p><a href="support.php" class="push_button_normal">&#8592; Retour</a></p>';
}

?>