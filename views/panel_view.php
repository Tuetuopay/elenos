<?php

function beginHistory ($title)	{
	echo '<h3>'.$title.'</h3><table class="historyTable"><thead><tr><th>Date</th><th>Description</th></tr></thead><tbody>';
}
function endHistory ()	{
	echo '</tbody></table>';
}

function buildMainSettings ()	{
	$ret = '<form action="panel.php?editProfile" method="post" enctype="multipart/form-data" autocomplete="off"><table id="main_settings">';
   $ret.='<tr><td>Pseudo</td><td>'.getCurrentUserName().'<br/><span class="thin_info">Votre pseudo est lié à votre compte Minecraft.</span></td></tr>';
	$ret .= '<tr><td colspan="2"><div class="horizontal_separator"></div></td></tr>';
	$ret .= '<tr><td>Adresse email</td><td>'.getCurrentUserEmail ().'<br /><span class="thin_info">À venir !</span></td></tr>';
	$ret .= '<tr><td colspan="2"><div class="horizontal_separator"></div></td></tr>';
	$ret .= '<tr><td>Avatar actuel :<br /><img src="'.getCurrentUserAvatarURL ().'" class="avatar drop-shadow" alt="avatar" /></td><td>
		<input type="radio" name="avatar" value="url" id="radio_url" checked /> <label for="radio_url">Par URL</label> 
		<input type="radio" name="avatar" value="upload" id="radio_upload" /> <label for="radio_upload">Par fichier</label>
		<div class="horizontal_separator"></div>
		<div id="form_url"><input type="url" placeholder="URL de votre avatar" name="avatar_url" id="avatar_url" class="textfield"  /></div>
		<div id="form_upload"><input type="file" name="avatar_file" id="avatar_file" class="push_button_normal" /></div>
		<span class="thin_info">jpg, png, gif, svg acceptés. 128x128px / 100ko max.</span>
		<div class="horizontal_separator"></div>
		<p><a href="panel.php?refreshAvatar" class="push_button_normal" title="Recalcule votre avatar à partir de votre skin Minecraft.">
		<img src="images/refresh.png" alt="refresh_icon" class="icon icon_left" /> Actualiser l\'avatar</a></p>
		</td></tr>';
	$ret .= '<tr><td colspan="2"><div class="horizontal_separator"></div></td></tr>';
	$ret .= '<tr><td><label for="current_pass">Mot de passe actuel :</label><br /><input type="password" name="current_pass" id="current_pass" 
		class="textfield"  /></td><td><label for="new_pass">Nouveau mot de passe :</label><br /><input type="password" name="new_pass"
		 id="new_pass" class="textfield"  /><label for="new_pass2">Confirmez le mot de passe :</label><br /><input type="password" 
		 name="new_pass2" id="new_pass2" class="textfield"  /></td></tr>';
	$ret .= '<tr><td colspan="2"><div class="horizontal_separator"></div></td></tr>';
	$ret .='<tr><td colspan="2"><button type="submit" class="push_button_normal"><img src="images/edit-profile.png" class="icon icon_left" 
		alt="edit_profile_icon" />Modifier mon compte</button></td></tr>';
	$ret .= '</table></form>';
	return $ret;
}
function buildImageUpload ()	{
	$ret = '<form action="panel.php?imgUpload" method="post" enctype="multipart/form-data">
		<input type="file" name="up_file" id="up_file" class="push_button_normal" />
		<span class="thin_info">Tous types de fichiers acceptés. Éviter les ficheirs trop gros ...</span>
		<button type="submit" class="push_button_normal"><img src="images/server.png" class="icon icon_left" 
		alt="send_icon" /> Envoyer</button></form>';
	return $ret;
}
function historyItem ($date, $desc, $desc2)	{
	$ret = '<tr><td>'.$date.'</td><td>'.$desc;
	if (!is_empty($desc2))
		$ret .= '<br /><span class="thin_info">'.$desc2.'</span>';
	$ret .= '</td></tr>';
	return $ret;
}

function buildFullHistoryButton ()	{
	return '<p><a href="panel.php?fullHistory" class="push_button_normal">Historiques complets</a></p>';
}
function buildReturnButton ()	{
	return '<p class="floating"><a href="panel.php" class="push_button_normal">&#8592; Retour</a></p>';
}

function beginServerList ()	{
	echo '<div class="serverTable table"><div class="thead"><div class="tr"><span class="th">Nom</span><span class="th">Nom Interne</span>
		<span class="th">Port</span><span class="th">Menu</span><span class="th">Boutique</span><span class="th">Actions</span></div>
	</div><div class="tbody">';
}
function endServerList ()	{
	echo '</div></div>';
}
function serverEntry ($id, $name, $internalName, $port, $shopDisplay, $menuDisplay)	{
	$ret = '<form action="panel.php?editServer='.$id.'" method="post" class="tr">
		<span class="td"><input type="text" name="name" value="'.$name.'" class="textfield" /></span>
		<span class="td"><input type="text" name="iname" value="'.$internalName.'" class="textfield" /></span>
		<span class="td"><input type="number" name="port" value="'.$port.'" class="textfield" /></span>
		<span class="td"><input type="checkbox" name="menuDisplay"';
	if ($menuDisplay)	$ret .= ' checked';
	$ret .= ' class="textfield" /></span>
		<span class="td"><input type="checkbox" name="shopDdisplay"';
	if ($shopDisplay)	$ret .= ' checked';
	$ret .= ' class="textfield" /></span>
		<div class="td">'.popupWindowStyle ('srv_'.$id).'
		<button type="submit" name="edit_'.$id.'" title="Modifier" class="push_button_normal"><img src="images/edit.png" 
		alt="edit_icon" class="icon" /></button><a href="#popup_srv_'.$id.'" class="push_button_normal" title="Supprimer ce serveur">
		<img src="images/delete.png" alt="delete_icon" class="icon" /></a>';
	$ret .= buildPopupWindow ('srv_'.$id, 'Confirmer la suppression : '.$name, '<br />
		<p>Voulez vous vraiment supprimer le serveur '.$name.' ?<br /><img src="images/warning.png" class="icon" alt="warning_icon" />
		<span class="thin_info">Cette suppression est définitive.</span><br /><br /><a href="#empty" class="push_button_normal">Annuler</a>
		<a href="panel.php?deleteServer='.$id.'" class="push_button_normal">Valider</a>');
	$ret .= '</div>
		</form>';
	return $ret;
}
function addServerEntry ()	{
	$ret = '<form action="panel.php?addServer" method="post" class="tr">
		<span class="td"><input type="text" name="name" class="textfield" placeholder="Nom affiché" /></span>
		<span class="td"><input type="text" name="iname" class="textfield" placeholder="Nom interne du serveur" /></span>
		<span class="td"><input type="number" name="port" class="textfield" placeholder="Port de JSONAPI" /></span>
		<span class="td"><input type="checkbox" name="menuDisplay" checked class="textfield" /></span>
		<span class="td"><input type="checkbox" name="shopDisplay" checked class="textfield" /></span>
		<span class="td"><button type="submit" name="add" title="Ajouter" class="push_button_normal"><img src="images/add.png" alt="add_icon" 
		class="icon" /></button></span>
		</form>';
	return $ret;
}
function setLobbyForm ($servers, $lobbyID)	{
	$ret = '<form action="panel.php?setLobby" method="post"><table><tr><td><label for="lobby_srv">Serveur de Lobby : <br /><span class="thin_info">
		Serveur de hub, où les joueurs arrivent en se connectant.</span></label>&nbsp;</td><td><select id="lobby_srv" name="lobby_srv">';
	foreach ($servers as $key => $value)	{
		$ret .= '<option';
		if ($key == $lobbyID)
			$ret .= ' selected';
		$ret .= ' value="'.$key.'">'.$value.'</option>';
	}
	$ret .= '</select></td><td><button type="submit" name="edit_'.$id.'" title="Modifier" class="push_button_normal"><img src="images/edit.png" 
		alt="edit_icon" class="icon" /></button></td></tr></table></form>';
	return $ret;
}

?>