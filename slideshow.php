<?php

include_once 'common.php';

function buildSlideshow ($bdd)	{
	$ret = '<div class="drop-shadow" id="slideshow"><div id=""><p>';
	$slides = getSlides ($bdd);
	foreach ($slides as $key => $value)
		$ret .= '<img src="'.$value['image'].'" alt="slideshow" class="slide" />';
	$ret .= '</p></div></div>';
	return $ret;
}
function makeSlideshowCSS ($bdd)	{
	$count = getSlideCount ($bdd);
	$slidePercent1S = 10 / $count;

	$ret = '#slideshow p	{ width: '.$count.'01%; '."\n";
	$prefixes = array ('-webkit-', '-moz-', '-ms-', '-o-', '');
	foreach ($prefixes as $prefix)	{
		$ret .= $prefix.'animation: slideAnim '.(10*$count).'s ease 0s infinite;'."\n";
	}
	$ret .= "}\n";
	foreach ($prefixes as $prefix) {
		$ret .= '@'.$prefix.'keyframes slideAnim	{';
			// 0%	{ left: 0; }'."\n";
			for ($i = 0; $i < $count; $i++)	{
				$ret .= (($i*10)*$slidePercent1S).'%	{ left: -'.(100*$i).'%; }'."\n";
				$ret .= ((9 + $i*10)*$slidePercent1S).'%	{ left: -'.(100*$i).'%; }'."\n";
			}
			$ret .= '100%	{ left: 0; }'."\n";
		$ret .= '}'."\n";
	}

	return $ret;
}

function getSlides ($bdd)	{
	$req = $bdd->query ('SELECT * FROM `elenos_slideshow` ORDER BY RAND()');
	$slides = array ();
	while ($s = $req->fetch ())
		$slides[$s['id']] = $s;
	return $slides;
}
function getSlideCount ($bdd)	{
	$req = $bdd->query ('SELECT COUNT(*) AS `cnt` FROM `elenos_slideshow`');
	$count = $req->fetch ();
	return $count['cnt'];
}

?>