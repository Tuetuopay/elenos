<?php

include_once 'session.php';

$noURLSave = true;

include_once ('bdd_connect.php');
include_once ('user_data.php');

$display = isset($_GET['forgotPassword']);

function protect ($string)	{
	return hash('sha512', $string);
}
function hashPassword ($username, $password)	{
	$username = strtolower($username);

	for ($i = 0; $i < 1000; $i ++)	{
		$password = protect ($username) . protect ($password . $username);
	}

	return $password;
}
function canUserLogin ($bdd, $username, $password)	{
	$req = $bdd->prepare ('SELECT `hashedPassword` FROM `elenos_users` WHERE UPPER(`username`) = UPPER(:username) AND `valid` = 0');
	$req->execute (array ('username' => $username));
	$data = $req->fetch ();
	$req->closeCursor ();

	if ($data == null)
		return false;

	if (hashPassword ($username, $password) == $data['hashedPassword'])
		return true;

	return false;
}

if (isset($_POST['username']) && isset($_POST['password']) && isset ($_POST['login']))	{
	if (canUserLogin ($bdd, $_POST['username'], $_POST['password']))	{
		$_SESSION['loginError'] = '';
		$_SESSION['loggedIn'] = true;
		$_SESSION['username'] = $_POST['username'];
		loadUserData ($bdd, $_POST['username']);
	} else	{
		$_SESSION['loginError'] = 'Identifiant ou mot de passe incorrect; ou compte non activé.';
		$_SESSION['loggedIn'] = false;
	}

	header ('Location: '.$_SESSION['currentURL']);
	exit;
}
if (isset($_GET['logout']))	{
	session_destroy ();
	header ('Location: '.$_SESSION['currentURL']);
	exit;
}
if (isset ($_GET['sendNewPassword']) && isset ($_POST['username']))	{
	$username = $_POST['username'];
	if (empty ($username))	{
		$_SESSION['sendNewPasswordResult'] = "Veuillez rentrer un nom d'utilisateur.";
		header('Location: ?forgotPassword');
		exit;
	}
	/* Getting the correctly-cased username + checking existence */
	$username = PDOQuery ($bdd, 'SELECT `username` AS `var` FROM `elenos_users` WHERE `username` = :var', 'var', $username);
	if (empty ($username))	{
		$_SESSION['sendNewPasswordResult'] = "Cet utilisateur n'existe pas.";
		header('Location: ?forgotPassword');
		exit;
	}
	$email = PDOQuery ($bdd, 'SELECT `email` AS `var` FROM `elenos_users` WHERE `username` = :var', 'var', $username);
	
	$token = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 40);
	$time = time ();

	mail($email, 'Récupération de votre mot de passe Elenos.fr', '<html><head><title>Récupération de votre mot de passe Elenos.fr</title><meta charset="utf-8">
</head><body><p>Bonjour '.$username.' !</p><p>Nous venons de recevoir une demande de changement de mot de passe sur 
<a href="http://'.getServerURL ().'">Elenos.fr</a>. Pour changer votre mot de passe, veuillez cliquer sur le lien ci-dessous :</p>
<p><a href="http://'.getServerURL ().'/login.php?newPassword='.$token.'&amp;u='.$username.'">
http://'.getServerURL ().'/login.php?newPassword='.$token.'&amp;u='.$username.'</a></p><p>Si vous n\'avez pas formulé cette demande, ignorez cet email : ce 
lien n\'est valide que 3 heures.</p><p>À bientôt sur <a href="http://'.getServerURL ().'">Elenos.fr</a>,<br />
L\'équipe Elenos</p>', 'MIME-Version: 1.0'."\r\n".'Content-type: text/html; charset=utf-8'."\r\n".'From: Elenos <no-reply@elenos.fr>');

	$req = $bdd->prepare ('UPDATE `elenos_users` SET `newPasswordToken` = :token, `newPasswordDate` = :time WHERE `username` = :user');
	$req->execute (array ('token' => $token, 'time' => $time, 'user' => $username));

	$_SESSION['sendNewPasswordResult'] = 'ok';
	header('Location: index.php');
	exit;
}
if (isset ($_GET['newPassword']) && isset ($_GET['u']) && !empty($_GET['newPassword']) && !empty($_GET['u']))	{
	$username = $_GET['u'];
	$req = $bdd->prepare ('SELECT `newPasswordDate` FROM `elenos_users` WHERE `username` = :u AND `newPasswordToken` = :token');
	$req->execute (array ('u' => $username, 'token' => $_GET['newPassword']));
	$time = $req->fetch ();
	$time = $time['newPasswordDate'];
	if (empty($time))	{
		$_SESSION['newPasswordResult'] = "Le jeton et l'utilisateur ne correspndent pas.";
		header ('Location: ?forgotPassword');
		exit;
	}
	if (time () - $time >= 10800)	{
		$_SESSION['newPasswordResult'] = "Votre jeton de réinitialisation a expiré (3h maximum)";
		PDOQuery ($bdd, 'UPDATE `elenos_users` SET `newPasswordDate` = 0, `newPasswordToken` = "" WHERE `username` = :u', 'u', $username);
		header ('Location: ?forgotPassword');
		exit;
	}
	$displayNewPasswordForm = true;
	$display = true;
}
if (isset ($_GET['setNewPassword']) && isset($_POST['passwd1']) && isset($_POST['passwd2']) && isset($_POST['username']) 
	&& isset($_POST['token']))	{
	if (empty($_POST['passwd1']) || empty($_POST['passwd2']))	{
		$_SESSION['setNewPasswordResult'] = "Veuillez remplir tous les champs.";
		header('Location: ?newPassword='.$_POST['token'].'&u='.$_POST['username']);
		exit;
	}
	if (empty($_POST['username']) || empty($_POST['token']))	{
		$_SESSION['setNewPasswordResult'] = "Erreur interne. Veuillez réessayer plus tard.";
		header('Location: ?forgotPassword');
		exit;
	}
	if (!doUserExists ($bdd, $_POST['username']))	{
		$_SESSION['setNewPasswordResult'] = "Cet utilisateur n'existe pas.";
		header('Location: ?newPassword='.$_POST['token'].'&u='.$_POST['username']);
		exit;
	}
	if ($_POST['passwd1'] != $_POST['passwd2'])	{
		$_SESSION['setNewPasswordResult'] = "Les deux mots de passe sont différents. Veuillez réessayer.";
		header('Location: ?newPassword='.$_POST['token'].'&u='.$_POST['username']);
		exit;
	}
	$req = $bdd->prepare ('UPDATE `elenos_users` 
					 	   SET `hashedPassword`=:hash, `newPasswordDate`=0, `newPasswordToken` = "" 
					 	   WHERE `username` = :username AND `newPasswordToken` = :token');
	$req->execute (array ('hash' => hashPassword ($_POST['username'], $_POST['passwd1']), 'username' => $_POST['username'], 
						  'token' => $_POST['token']));
	$_SESSION['setNewPasswordResult'] = 'ok';
	header('Location: index.php');
	exit;

}

// $displayNewPasswordForm = true;

if ($display === true)	{

	$page_name = 'Mot de passe oublié';
	include 'header.php';

	checkSessionExecuteMessage ('sendNewPasswordResult', 'ok', "Un email vous a été envoyé, vous devriez le recevoir sous peu.");
	checkSessionExecuteMessage ('newPasswordResult', 'ok', "Votre mot de passe a été réinitialisé. Veuillez vous connecter.");
	checkSessionExecuteMessage ('setNewPasswordResult', 'ok', "Votre mot de passe a été réinitialisé. Veuillez vous connecter.");

	beginPage ();
	beginStandaloneSection ("Mot de passe oublié", false);

	if (isset ($_GET['forgotPassword']))	{
		echo "<p>Vous avez oublié votre mot de passe ?<br>Ce n'est pas grave, vous pouvez en demander un nouveau.</p>";
		echo '<p><form action="login.php?sendNewPassword" method="post"><label for="username"><img src="images/user.png" 
			class="icon icon_left" alt="user_icon" />Votre pseudo : <br>&nbsp; <input type="text" name="username" id="username" 
			placeholder="Votre nom d\'utilisateur" class="textfield" required style="width: 200px;"> &nbsp; <button type="submit" 
			class="push_button_normal"><img src="images/email_go.png" class="icon icon_left" alt="email_icon" /> Envoyer la demande
			</button></form></p>';
	} else if ($displayNewPasswordForm)	{
		?>
		<p><form action="?setNewPassword" method="post">
			<p class="formfield">
				<label for="passwd1" class="pass">Nouveau mot de passe souhaité</label><br />
				<input type="password" name="passwd1" id="passwd1" required="required" placeholder="Entrez le nouveau mot de passe souhaité" class="textfield"><br />
				<span class="field_info">Préférez un mot de passe fort, qui ne se trouve dans aucun dictionnaire.</span>
			</p>
			<p class="formfield">
				<label for="passwd2" class="pass">Nouveau mot de passe souhaité (2è fois)</label><br />
				<input type="password" name="passwd2" id="passwd2" required="required" placeholder="Entrez à nouveau le mot de passe" class="textfield" /><br />
			</p>
			<input type="hidden" name="token" value="<?php echo $_GET['newPassword']; ?>">
			<input type="hidden" name="username" value="<?php echo $_GET['u']; ?>">
			<button type="submit" class="push_button_normal"><img src="images/lock.png" class="icon icon_left" alt="lock_icon" 
				/> Valider mon mot de passe</button>
		</form></p>
		<?php
	}

	endStandaloneSection ();
	endPage ();

	include ('right_menu.php');
	include ('footer.php');

}


?>