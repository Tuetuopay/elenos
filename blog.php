<?php

include_once 'session.php';

include_once ('common.php');
include_once ('models/blog_model.php');

function redirectAndExit ()	{
	if (isset ($_GET['view']))
		header('Location: blog.php?view='.$_GET['view']);
	else
		header('Location: blog.php');
	exit;
}

if (isset ($_POST['pseudo']) && isset ($_POST['email']) && isset ($_POST['commentID']) && isset ($_GET['view']) && isset ($_POST['comment']))	{
	$_SESSION['resultComment'] = blogSaveComment ($bdd, $_GET['view'], $_POST['commentID'], $_POST['pseudo'], $_POST['email'], $_POST['comment']);
	redirectAndExit ();
}
if (isset ($_POST['article_title']) && isset($_POST['article_content']) && isset($_GET['poster']))	{
	$_SESSION['resultArticle'] = blogSaveArticle ($bdd, $_POST['article_title'], $_POST['article_content']);
	redirectAndExit ();
}
if (isset ($_GET['deleteArticle']) && doCurrentUserHavePermission ('blog.deleteArticle'))	{
	$_SESSION['resultDeleteArticle'] = blogDeleteArticle ($bdd, $_GET['deleteArticle']);
	redirectAndExit ();
}
if (isset ($_GET['deleteComment']) && doCurrentUserHavePermission ('blog.deleteComment'))	{
	$_SESSION['resultDeleteComment'] = blogDeleteComment ($bdd, $_GET['deleteComment']);
	redirectAndExit ();
}

$startingArticle = '-1';
if (isset($_GET['first']))
	$startingArticle = $_GET['first'];

$page_name = 'Accueil';
$slideshow = true;

if ($_SESSION['resultArticle'] > 0)	{
	header('Location: blog.php?view='.$_SESSION['resultArticle']);
	$_SESSION['resultArticle'] = 'ok';
	exit;
}


include_once ('header.php');

if ($_SESSION['loginError'] != '')	{
	printErrorMessage ($_SESSION['loginError']);
	$_SESSION['loginError'] = '';
}

checkSessionExecuteMessage ('resultComment', 'ok', "Le commentaire a été posté.");
checkSessionExecuteMessage ('resultArticle', 'ok', "L'article a été posté.");
checkSessionExecuteMessage ('resultDeleteComment', 'ok', "Le commentaire a été supprimé.");
checkSessionExecuteMessage ('resultDeleteArticle', 'ok', "L'article a été supprimé.");
/* Password recovery */
checkSessionExecuteMessage ('sendNewPasswordResult', 'ok', "Un email vous a été envoyé, vous devriez le recevoir sous peu.");
checkSessionExecuteMessage ('setNewPasswordResult', 'ok', "Votre mot de passe a été réinitialisé. Veuillez vous connecter.");

if (isset($_GET['view']))	{
	if (!isset($_GET['answerTo']))	{
		$_GET['answerTo'] = 0;
	}
	blogPostDetail ($bdd, $_GET['view'], $_GET['answerTo']);
} 
else if (isset ($_GET['poster']))	{
	if (doCurrentUserHavePermission ('blog.postArticle'))
		blogNewArticleEditor ($bdd);
	else	{
		printErrorMessage ('Désolé, vous n\'avez pas le droit de poster de nouveaux articles.');
		blogHomePage ($bdd, $startingArticle);
	}
}
else {
	blogHomePage ($bdd, $startingArticle);
}

include ('right_menu.php');
include ('footer.php');

?>















