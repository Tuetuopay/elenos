<?php
include_once 'session.php';

include_once ('common.php');
include_once ('models/support_model.php');

$page_name = 'Support';

function redirectAndExit ()	{
	if (isset($_GET['view']))
		$a = '?view='.$_GET['view'];
	header('Location: support.php'.$a);
	exit;
}

if (isset ($_GET['newTicket']) && isset($_POST['subject']) && isset($_POST['content']))	{
	$_SESSION['saveTicketResult'] = supportSaveTicket ($bdd, $_POST['subject'], $_POST['content']);
	header('Location: support.php?view='.$_SESSION['ticketID']);
	unset($_SESSION['ticketID']);
	exit;
}
if (isset($_GET['newAnswer']) && isset($_GET['view']) && isset($_POST['content']))	{
	$_SESSION['saveAnswerResult'] = supportSaveAnswer($bdd, $_GET['view'], $_POST['content']);
	redirectAndExit ();
}
if (isset($_GET['view']) && isset($_GET['validate']))	{
	$_SESSION['validateTicketResult'] = supportValidateTicket ($bdd, $_GET['view']);
	redirectAndExit ();
}
if (isset($_GET['view']) && isset($_GET['pend']))	{
	$_SESSION['pendTicketResult'] = supportPendTicket ($bdd, $_GET['view']);
	redirectAndExit ();
}
if (isset($_GET['view']) && isset($_GET['delete']))	{
	$_SESSION['deleteTicketResult'] = supportDeleteTicket ($bdd, $_GET['view']);
	redirectAndExit ();
}

include_once ('header.php');

if ($_SESSION['loginError'] != '')	{
	printErrorMessage ($_SESSION['loginError']);
	$_SESSION['loginError'] = '';
}

if ($_SESSION['deleteTicketResult'] == 'ok')
	unset($_GET['view']);

checkSessionExecuteMessage ('saveTicketResult', 'ok', "Votre ticket a été ajouté.");
checkSessionExecuteMessage ('saveAnswerResult', 'ok', "Réponse ajoutée.");
checkSessionExecuteMessage ('validateTicketResult', 'ok', "Ticket validé.");
checkSessionExecuteMessage ('pendTicketResult', 'ok', "Ticket invalidé.");
checkSessionExecuteMessage ('deleteTicketResult', 'ok', "Ticket supprimé.");

if (isset($_GET['view']))
	supportViewTicket ($bdd, $_GET['view']);
else
	supportMainPage ($bdd, isset($_GET['pending']));

include ('right_menu.php');
include ('footer.php');


?>