<?php

include_once ('user_data.php');
include_once ('bbcode_parser.php');

?>

			<div id="side_infos" onclick="this.style['right']=(this.style['right']=='-1px')?'':'-1px';">
				<p id="side_infos_title"><?php
					if (isUserLoggedIn ())
						echo "Espace Membre";
					else
						echo 'Connexion / Inscription';
					?></p>
				<aside id="side_infos_head">
					<?php
					if (isUserLoggedIn ())	{
						?>
						<div id="main_user_avatar">
						<p class="image"><img src="<?php echo getCurrentUserAvatarURL (); ?>" class="image" alt="Avatar" /></p>
						<div class="horizontal_separator"></div>
						<?php
						echo bbcodeParse ('[b][color='.getGroupDisplayColor ().']['.getGroupName ().'][/color][/b] '.getCurrentUserName ());
						echo '<p><a href="login.php?logout">Déconnexion</a></p><div class="horizontal_separator"></div>';
						echo '</div>';
					} else
						echo buildLoginForm ();
					?>
				</aside>
				<?php if (isUserLoggedIn ())	{ ?>
				<aside>
					<!--<h3>Actions</h3>-->
					<ul>
						<?php echo '<li><a href="membre.php?u='.getCurrentUserName ().'">Profil <img src="images/user.png" class="icon" alt="user_icon" /></a></li>' ?>
						<li><a href="panel.php">Réglages <img src="images/newtake.png" class="icon" alt="settings_icon" /></a></li>
						<li><a href="support.php">Tickets ouverts <img src="images/ticket.png" class="icon" alt="ticket_icon" /></a></li>
						<?php
						if (doCurrentUserHavePermission ('blog.postArticle'))
							echo '<li><a href="blog.php?poster">Nouveau billet <img src="images/new.png" class="icon" alt="new_bill_icon" /></a></li>';
						?>
					</ul>
				</aside>
				<?php } ?>
			</div>