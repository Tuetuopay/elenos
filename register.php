<?php

include_once 'session.php';

/* Gives us a PDO object $bdd */
include_once ('bdd_connect.php');
include_once ('user_data.php');
include_once ('login.php');

$page_name = 'S\'inscrire';

function buildRegisterForm ()	{
	?>
	<div id="site_corpse">
		<section class="standalone_section">
			<header><h2>S'inscrire</h2></header>
			<article>
				<form method="post" action="register.php" autocomplete="off">
					<p class="formfield">
						<label for="display_name" class="name">Pseudo InGame</label><br />
						<input type="text" name="name" id="display_name" value="<?php echo $_POST['name']; ?>" required="required" placeholder="Entrez ici votre pseudo souhaité..." class="textfield" maxlength="30"><br />
						<span class="field_info">Votre pseudo Minecraft. Vous devez être connecté sur le serveur !</span>
					</p>
					<p class="formfield">
						<label for="email" class="mail">Adresse e-mail</label><br />
						<input type="email" name="email" id="email" value="<?php echo $_POST['email']; ?>" required="required" placeholder="Entrez ici votre adresse e-mail..." class="textfield"><br />
						<span class="field_info">Votre adresse e-mail doit être valide pour vous permettre d'activer votre nouveau compte.</span>
					</p>
					<p class="formfield">
						<label for="passwd1" class="pass">Mot de passe souhaité</label><br />
						<input type="password" name="passwd1" id="passwd1" required="required" placeholder="Entrez le mot de passe souhaité" class="textfield"><br />
						<span class="field_info">Préférez un mot de passe fort, qui ne se trouve dans aucun dictionnaire.</span>
					</p>
					<p class="formfield">
						<label for="passwd2" class="pass">Mot de passe souhaité (2è fois)</label><br />
						<input type="password" name="passwd2" id="passwd2" required="required" placeholder="Entrez à nouveau le mot de passe" class="textfield" /><br />
					</p>
					<p class="formfield last">
						<input type="submit" class="submit push_button_normal" name="send_new_account" value="Créer mon compte" />
					</p>
					<p class="form_bottom_info">Un e-mail va vous être envoyé, il contiendra un lien à suivre pour activer votre compte nouvellement créé. Assurez-vous donc de la validité de l'adresse e-mail saisie.</p>
				</form>
			</article>
		</section>
	</div>
	<?php
}
function registerAccount ($bdd, $username, $email, $password1, $password2)	{
	if (doUserExists ($bdd, $username))	{
		printErrorMessage ('Ce nom d\'utilisateur est déjà pris.');
		return false;
	}
	if (getUserServer ($bdd, $username) == 0)	{
		printErrorMessage ('Veuillez vous connecter sur play.elenos.fr pour vous inscrire.');
		return false;
	}
	if (!preg_match('#^[a-z0-9._-]+@[a-z0-9._-]{2,}\\.[a-z]{2,4}$#', $email))	{
		printErrorMessage ('Vous devez spécifier une adresse email valide.');
		return false;
	}
	if ($password1 != $password2)	{
		printErrorMessage ('Les mots de passe saisis sont différents.');
		return false;
	}

	$activationToken = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 40);

	makeUserAvatar ($username);

	$req = $bdd->prepare ('INSERT INTO `elenos_users`(`username`, `email`, `hashedPassword`, `avatarURL`, `valid`, `registerDate`, `voteCooldown`)
						   VALUES(:username, :email, :hash, :avatar, :token, NOW(), "[0,0,0,0]")');
	$req->execute (array ('username' => $username, 'email' => $email, 'hash' => hashPassword ($username, $password1), 'token' => $activationToken,
						'avatar' => 'avatar/'.$username.'.png'));

	mail($email, 'Activation de votre compte Elenos.fr', '<html><head><title>Activation de votre compte Elenos.fr</title><meta charset="utf-8">
</head><body><p>Bonjour '.$username.' !</p><p>Vous venez de vous inscrire sur <a href="http://'.getServerURL ().'">Elenos.fr</a>. Pour terminer votre 
inscription, veuillez cliquer sur le lien ci-dessous :</p><p><a href="http://'.getServerURL ().'/register.php?activate='.$activationToken.'">
http://'.getServerURL ().'/register.php?activate='.$activationToken.'</a></p><p>À bientôt sur <a href="http://'.getServerURL ().'">Elenos.fr</a>,<br />
L\'équipe Elenos</p>', 'MIME-Version: 1.0'."\r\n".'Content-type: text/html; charset=utf-8'."\r\n".'From: Elenos <no-reply@elenos.fr>');

	printInfoMessage ('Votre compte a bien été enregistré, un email vous a été envoyé à '.$email.' afin de valider votre compte.');

	return true;
}
function validateAccount ($bdd, $token)	{
	$username = PDOQuery ($bdd, 'SELECT `username` AS `var` FROM `elenos_users` WHERE `valid` = :var', 'var', $token);
	$req = $bdd->prepare ('UPDATE `elenos_users` SET `valid` = 0 WHERE `valid` = :token');
	$req->execute (array ('token' => $token));

	if ($req->rowCount () >= 1)	{
		$lobbyPort = PDOQuery ($bdd, 'SELECT `port` AS `var` FROM `elenos_servers` WHERE `id` = :var', 'var', getLobbyServer ($bdd));
		$api = getJSONAPI ($lobbyPort);
		$api->call("runConsoleCommand", array ("pex user ".$username." group set Membre"));
	}
	else
		printErrorMessage ('Erreur : aucun compte n\'est associé à ce jeton, ou le compte a déjà été activé.');
}

include ('header.php');

if (isset ($_GET['activate']))
	validateAccount ($bdd, $_GET['activate']);
if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['passwd1']) && isset($_POST['passwd2']))	{
	$_POST['name'] = htmlspecialchars(stripslashes($_POST['name']));
	registerAccount ($bdd, $_POST['name'], $_POST['email'], $_POST['passwd1'], $_POST['passwd2']);
	buildRegisterForm ();
} else	{
	buildRegisterForm ();
}

include ('right_menu.php');
include ('footer.php');

?>















