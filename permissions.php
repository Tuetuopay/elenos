<?php

/* Main permission file,  */
function reloadPermissions ($bdd, $username)	{
	$req = $bdd->prepare ('SELECT `elenos_user_group`.* FROM `elenos_user_group`, `elenos_users` WHERE `elenos_user_group`.`id` = `elenos_users`.`groupID`
														 AND UPPER(`elenos_users`.`username`) = UPPER(:pseudo) AND `elenos_users`.`valid` = 0');
	$req->execute (array ('pseudo' => $username));
	$data = $req->fetch ();

	foreach ($data as $key => $value) {
		if ($key == 'id')
			continue;
		setCurrentUserPermission ($key, $value);
	}
	$req->closeCursor ();
}
function doCurrentUserHavePermission ($permissionNode)	{
	// $nodes = explode('.', $permissionNode);
	return $_SESSION[$permissionNode]; // getNodeValue ($_SESSION, $nodes);
}
function setCurrentUserPermission ($permissionNode, $value)	{
	// $nodes = explode('.', $permissionNode);
	// setNodeValue ($_SESSION, $nodes, $value);
	$_SESSION[$permissionNode] = $value;
}

function getGroupProperty ($property)	{
	// $nodes = explode('.', $property);
	// return getNodeValue ($_SESSION, $nodes);
	return $_SESSION[$property];
}
function getGroupDisplayColor ()	{
	return getGroupProperty ('general.groupColor');
}
function getGroupName()	{
	return getGroupProperty ('groupName');
}

function getNodeValue ($container, $nodes)	{
	if (count ($nodes) <= 1)
		return $container[$nodes[0]];
	else	{
		$nextNodes = array();
		for ($i = 1; $i < count ($nodes); $i++) {
			$nextNodes[$i - 1] = $nodes[$i];
		}
		return getNodeValue ($container[$nodes[0]], $nextNodes);
	}
}
function setNodeValue ($container, $nodes, $value)	{
	if (!isset ($container[$nodes[0]]))
		$container[$nodes[0]] = array ('' => '');
	if (count ($nodes) <= 1)
		$container[$nodes[0]] = $value;
	else	{
		$nextNodes = array();
		for ($i = 1; $i < count ($nodes); $i++) {
			$nextNodes[$i - 1] = $nodes[$i];
		}
		setNodeValue ($container[$nodes[0]], $nextNodes, $value);
	}
}

function buildLoginForm ()	{
	$ret = '<h3>Connexion</h3><form action="login.php" method="post" class="login_form">
	<p class="formfield"><label for="username" class="name">Identifiant</label><br /><input type="text" id="username" name="username" required="required" class="textfield" /></p>
	<p class="formfield"><label for="password" class="pass">Mot de passe</label><br /><input type="password" id="password" name="password" required="required" class="textfield" /></p>
	<input type="hidden" name="login" />
	<p><input type="submit" class="submit push_button_normal" name="send_login" value="Connexion"></p>';
	$ret .= '</form><p><a href="register.php" class="login_info">S\'inscrire</a><br /><a href="login.php?forgotPassword" 
		class="forgot_passwd">Mot de passe oublié ?</a></p>';
	return $ret;
}

?>