<?php

include_once 'session.php';

include_once ('common.php');
include_once ('models/membre_model.php');

$page_name = 'Membres';

function redirectAndExit ($user = "")	{
	if (is_empty ($user))
		header('Location: membre.php');
	else
		header('Location: membre.php?u='.$user);
	exit;
}

if (isset($_GET['editRank']))	{
	if (isset($_POST['user_rank']) && !is_empty ($_GET['editRank']))
		$_SESSION['editRankResult'] = membreSetRank ($bdd, $_GET['editRank'], $_POST['user_rank']);
	redirectAndExit ($_GET['editRank']);
}
if (isset($_GET['editTokens']))	{
	if (isset($_POST['user_tokens']) && !is_empty ($_GET['editTokens']))
		$_SESSION['editTokensResult'] = membreSetTokens ($bdd, $_GET['editTokens'], $_POST['user_tokens']);
	redirectAndExit ($_GET['editTokens']);
}
if (isset($_GET['sendTokens']) && !empty($_GET['sendTokens']))	{
	$_SESSION['sendTokensResult'] = membreSendToken ($bdd, $_GET['sendTokens'], $_POST['send_tokens']);
	redirectAndExit ($_GET['sendTokens']);
}
if (isset($_GET['sendTokens']) && isset($_GET['fromShop']))	{
	if (isset($_POST['send_tokens']) && isset($_POST['send_user']) && !empty($_POST['send_user']))	{
		$_SESSION['sendTokensResult'] = membreSendToken ($bdd, $_POST['send_user'], $_POST['send_tokens']);
	}
	header('Location: boutique.php');
	exit;
}

include_once ('header.php');

if ($_SESSION['loginError'] != '')	{
	printErrorMessage ($_SESSION['loginError']);
	$_SESSION['loginError'] = '';
}

checkSessionExecuteMessage ('editRankResult', 'ok', "Rang modifié.");
checkSessionExecuteMessage ('editTokensResult', 'ok', "Tokens modifiés.");
checkSessionExecuteMessage ('sendTokensResult', 'ok', "Tokens envoyés.");

if (isset($_GET['u']))
	membreVewProfile ($bdd, $_GET['u']);
else if (isset ($_GET['l']) && !empty ($_GET['l']))
	membreList ($bdd, $_GET['l']);
else
	membreList ($bdd, "meh");

include ('right_menu.php');
include ('footer.php');

?>