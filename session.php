<?php

include_once 'bdd_connect.php';

class MySessionHandler implements SessionHandlerInterface	{
	public function open ($savePath, $sessionName)	{
		global $sess_bdd;
		$sess_bdd = createPDO ();
		$this->gc (68);
		return $sess_bdd;
	}
	public function close ()	{
		global $sess_bdd;
		$sess_bdd = null;
		return true;
	}
	public function read ($id)	{
		global $sess_bdd;

		$req = $sess_bdd->prepare ('SELECT `data` FROM `elenos_sessions` WHERE `id` = :id');
		$req->execute (array ('id' => $id));
		$data = $req->fetch ();
		$req->closeCursor ();

		if (empty ($data))
			return false;
		else
			return $data['data'];
	}
	public function write ($id, $data)	{
		global $sess_bdd, $bdd;
		$expire = intval (time () + (3 * 31 * 24 * 3600));	// 3 mois
		$reqCnt = $sess_bdd->prepare ('SELECT COUNT(`id`) AS `cnt` FROM `elenos_sessions` WHERE `id` = :id');
		$reqCnt->execute (array ('id' => $id));
		if ($reqCnt->fetch ()['cnt'] == 0)
			$sql = 'INSERT INTO `elenos_sessions`(`id`, `data`, `expire`) VALUES(:id, :data, :expire)';
		else
			$sql = 'UPDATE `elenos_sessions` SET `data` = :data, `expire` = :expire WHERE `id` = :id';
		$req = $sess_bdd->prepare ($sql);
		$req->execute (array ('id' => $id, 'data' => $data, 'expire' => $expire));

		$ok = $req->rowCount () != 0;
		$reqCnt->closeCursor ();
		$req->closeCursor ();

		return $ok;
	}
	public function destroy ($id)	{
		global $sess_bdd;

		$req = $sess_bdd->prepare ('DELETE FROM `elenos_sessions` WHERE `id` = :id');
		$req->execute (array ('id' => $id));

		$ok = $req->rowCount () != 0;
		$req->closeCursor ();

		return $ok;
	}
	public function gc ($maxlifetime)	{
		global $sess_bdd;

		$req = $sess_bdd->prepare ('DELETE FROM `elenos_sessions` WHERE `expire` < :expire');
		$req->execute (array ('expire' => time ()));

		$ok = $req->rowCount () != 0;
		$req->closeCursor ();

		return $ok;
	}
}

$handler = new MySessionHandler ();
ini_set ('session.save_handler', 'user');
// session_set_save_handler ('sess_open', 'sess_close', 'sess_read', 'sess_write', 'sess_destroy', 'sess_gc');
session_set_save_handler ($handler);
session_start ();

?>