<?php 
include_once ('common.php');
include_once 'bdd_connect.php';
include_once 'slideshow.php';
?>

<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $page_name; ?> &middot; Elenos</title>
		<meta charset="utf-8">
		<link rel="shortcut icon" href="favicon.png" />
		<link rel="stylesheet" href="style/elenos.css" type="text/css">
		<script src="js/move_popups.js"></script>

		<?php
		if ($slideshow == true)	{
			echo '<style type="text/css">'.makeSlideshowCSS($bdd).'</style>';
			?>
			<script type="text/javascript">
				function makeSlideWidth ()	{
					var slideWidth = document.getElementById("slideshow").offsetWidth;
					var slideHeight = slideWidth * (300/930);
					var slides = document.getElementsByClassName("slide");
					for(var i=0; i<slides.length; i++){
						slides[i].style['width'] = slideWidth+"px";
						slides[i].style['height'] = slideHeight+"px";
					}
					document.getElementById("slideshow").style['height'] = slideHeight+"px";
					document.getElementById("topspacer").style['display'] = "none";
				}
				window.onload = makeSlideWidth;
				window.onresize = makeSlideWidth;
			</script>
			<?php
		}
		echo $extraHeaderInfo;
		?>
	</head>
	<body>
		<header id="main_header">
			<div id="header_contents">
				<a href="index.php"><img src="images/Logo-Elenos.png" class="header_logo" alt="Site Icon" /></a>
				<div id="header_info"><h1><a href="index.php">ELENOS</a></h1></div>
				<nav class="navbar">
					<?php echo buildMainMenu(); ?>
				</nav>
			</div>
		</header>
		<div class="nodisplay" id="empty"></div>
		<div id="topspacer"></div>
		<?php 
			if ($slideshow == true)	{
			echo buildSlideshow ($bdd);
			?>
			<div id="server_ip"><h2>IP : <input type="text" value="play.elenos.fr" readonly /></h2>
				<h2>TeamSpeak : <input type="text" value="À venir" readonly></h2></div>
			<?php
		}
		?>
		<div id="site_content">
			<?php
			//if (!(strpos(getServerURL (), 'localhost') !== false) || isset($_GET['servers']))
				include_once ('server_menu.php');
			?>