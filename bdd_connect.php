<?php

include_once ('JSONAPI.php');

function createPDO ()	{
	try {
		$bdd = new PDO('mysql:host=localhost;dbname=elenos', 'root', '');
	}	catch(Exception $e) {
		die('Erreur : '.$e->getMessage());
	}
	return $bdd;
}
$bdd = createPDO ();

function getJSONAPI ($port)	{
	/* JSONAPI infos */
	$jsonAPI_IP = 'play.elenos.fr';
	$jsonAPI_User = 'JSONAPI_USER';
	$jsonAPI_Password = 'JSONAPI_PASSWPRD';
	$jsonAPI_Salt = '';

	return new JSONAPI($jsonAPI_IP, $port, $jsonAPI_User, $jsonAPI_Password, $jsonAPI_Salt);
}

?>
