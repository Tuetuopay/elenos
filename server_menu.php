<?php

include_once ('bdd_connect.php');
include_once ('user_data.php');
include_once ('bbcode_parser.php');

?>

			<div id="left_infos" onclick="this.className=(this.className=='clicked')?'':'clicked';">
			<?php
			if (isUserLoggedIn ())
				$connectedServer = getCurrentUserServer ($bdd);
			else
				$connectedServer = 0;

			$tot = '';
			$count = 0;
			$servers = getServersInfo($bdd, true);
			foreach ($servers as $id => $srv)	{
				$tot .= '<tr><td colspan="2"><div class="horizontal_separator"></div></td></tr><tr><td>';
				if ((int)$id == (int)$connectedServer)
					$tot .= '<span class="bold italic">';
				$tot .= $srv['name'].'</td><td>';
				if ($srv['status'] == 'offline')
					$tot .= '<span style="color: red">Fermé</span>';
				else	{
					$tot .= $srv['onlineCount'];
					$count += (int)$srv['onlineCount'];
				}
				if ((int)$id == (int)$connectedServer)
					$tot .= '</span>';
				$tot .= '</td></tr>';
			}
			?>
			<h2 id="left_infos_title">Serveurs</h2>
			<?php echo '<p class="thin_info">'.$count.' joueurs</p>'; ?>
				<aside id="left_infos_head">
					<table>
						<?php echo $tot; ?>
					</table>
				</aside>
			</div>