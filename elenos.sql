-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 24, 2014 at 02:51 PM
-- Server version: 5.5.38-1~dotdeb.0-log
-- PHP Version: 5.4.33-1~dotdeb.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `tuetuopay`
--

-- --------------------------------------------------------

--
-- Table structure for table `elenos_admin_upload`
--

CREATE TABLE IF NOT EXISTS `elenos_admin_upload` (
`id` int(11) NOT NULL,
  `username` text NOT NULL,
  `file` text NOT NULL,
  `size` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `elenos_blog_comments`
--

CREATE TABLE IF NOT EXISTS `elenos_blog_comments` (
`id` int(11) NOT NULL,
  `postID` int(11) NOT NULL COMMENT 'The target (blog article OR other comment)',
  `commentID` int(11) NOT NULL DEFAULT '0' COMMENT 'The ID of the comment this comment replies to',
  `author` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `email` text NOT NULL,
  `commentContents` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `elenos_contact_messages`
--

CREATE TABLE IF NOT EXISTS `elenos_contact_messages` (
`id` int(11) NOT NULL,
  `author` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `elenos_menu`
--

CREATE TABLE IF NOT EXISTS `elenos_menu` (
`id` int(11) NOT NULL,
  `name` text NOT NULL,
  `link` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `elenos_menu`
--

INSERT INTO `elenos_menu` (`id`, `name`, `link`) VALUES
(1, 'Accueil', 'blog.php'),
(2, 'Boutique', 'boutique.php'),
(3, 'Membres', 'membre.php'),
(4, 'Support', 'support.php'),
(5, 'Forum', 'forum/'),
(6, 'Vote', 'vote.php'),
(7, 'Contact', 'contact.php');

-- --------------------------------------------------------

--
-- Table structure for table `elenos_news`
--

CREATE TABLE IF NOT EXISTS `elenos_news` (
`id` int(11) NOT NULL,
  `title` text NOT NULL,
  `contents` text NOT NULL,
  `authorName` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `elenos_servers`
--

CREATE TABLE IF NOT EXISTS `elenos_servers` (
`id` int(11) NOT NULL,
  `name` text NOT NULL COMMENT 'The display name for the server',
  `internalName` text NOT NULL COMMENT 'The internal ',
  `port` int(11) NOT NULL COMMENT 'the port JSONAPI listens on',
  `shopDisplay` int(11) NOT NULL DEFAULT '1' COMMENT 'Can you buy items on this server ?',
  `isLobby` int(11) NOT NULL DEFAULT '0' COMMENT 'the main server (the one you lands on when connecting). '
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `elenos_servers`
--

INSERT INTO `elenos_servers` (`id`, `name`, `internalName`, `port`, `shopDisplay`, `isLobby`) VALUES
(5, 'Lobby', 'lobby', 27055, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `elenos_shop_history`
--

CREATE TABLE IF NOT EXISTS `elenos_shop_history` (
`id` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `player` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `elenos_shop_items`
--

CREATE TABLE IF NOT EXISTS `elenos_shop_items` (
`id` int(11) NOT NULL,
  `section` int(11) NOT NULL,
  `label` text NOT NULL,
  `icon` text NOT NULL,
  `description` text NOT NULL,
  `price` int(11) NOT NULL,
  `command` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `elenos_shop_sections`
--

CREATE TABLE IF NOT EXISTS `elenos_shop_sections` (
`id` int(11) NOT NULL,
  `name` text NOT NULL,
  `server` int(11) NOT NULL,
  `order` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `elenos_shop_tokens`
--

CREATE TABLE IF NOT EXISTS `elenos_shop_tokens` (
`id` int(11) NOT NULL,
  `name` text NOT NULL,
  `display` text NOT NULL,
  `tokens` int(11) NOT NULL,
  `price_paypal` float NOT NULL,
  `price_allopass` int(11) NOT NULL,
  `price_starpass` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `elenos_slideshow`
--

CREATE TABLE IF NOT EXISTS `elenos_slideshow` (
`id` int(11) NOT NULL,
  `image` text NOT NULL,
  `url` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `elenos_support_tickets`
--

CREATE TABLE IF NOT EXISTS `elenos_support_tickets` (
`id` int(11) NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `status` text NOT NULL,
  `author` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `elenos_support_tickets_replies`
--

CREATE TABLE IF NOT EXISTS `elenos_support_tickets_replies` (
`id` int(11) NOT NULL,
  `ticketID` int(11) NOT NULL,
  `content` text NOT NULL,
  `author` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `elenos_token_transact`
--

CREATE TABLE IF NOT EXISTS `elenos_token_transact` (
`id` int(11) NOT NULL,
  `user` text NOT NULL,
  `token` text NOT NULL,
  `status` text NOT NULL,
  `itemID` int(11) NOT NULL,
  `completionDate` datetime DEFAULT NULL,
  `method` text NOT NULL,
  `fromUser` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `elenos_users`
--

CREATE TABLE IF NOT EXISTS `elenos_users` (
`id` int(11) NOT NULL,
  `username` text NOT NULL,
  `email` text NOT NULL,
  `groupID` int(11) NOT NULL DEFAULT '1',
  `hashedPassword` text NOT NULL,
  `avatarURL` text NOT NULL,
  `tokens` int(11) NOT NULL DEFAULT '0',
  `valid` varchar(50) NOT NULL DEFAULT '1' COMMENT 'Bit special : 0 means ''Yes, account valid'', everything else is the activation token.',
  `registerDate` text NOT NULL,
  `votePoints` int(11) NOT NULL DEFAULT '0',
  `newPasswordToken` text NOT NULL,
  `newPasswordDate` int(11) NOT NULL DEFAULT '0',
  `voteCooldown` text NOT NULL COMMENT 'JSON data',
  `voteCount` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `elenos_user_group`
--

CREATE TABLE IF NOT EXISTS `elenos_user_group` (
`id` int(11) NOT NULL,
  `groupName` text NOT NULL,
  `blog.postComment` int(11) NOT NULL DEFAULT '0',
  `blog.postArticle` int(11) NOT NULL DEFAULT '0',
  `blog.deleteArticle` int(11) NOT NULL DEFAULT '0',
  `blog.deleteComment` int(11) NOT NULL DEFAULT '0',
  `general.groupColor` text NOT NULL,
  `shop.editItems` int(11) NOT NULL DEFAULT '0',
  `support.validateTicket` int(11) NOT NULL DEFAULT '0',
  `general.editServers` int(11) NOT NULL DEFAULT '0',
  `general.editUser` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `elenos_user_group`
--

INSERT INTO `elenos_user_group` (`id`, `groupName`, `blog.postComment`, `blog.postArticle`, `blog.deleteArticle`, `blog.deleteComment`, `general.groupColor`, `shop.editItems`, `support.validateTicket`, `general.editServers`, `general.editUser`) VALUES
(1, 'Clampin', 1, 0, 0, 0, 'white', 0, 0, 0, 0),
(2, 'ModÃ©rateur', 1, 0, 0, 1, 'cyan', 0, 1, 0, 0),
(3, 'RÃ©dacteur', 1, 1, 0, 0, 'green', 0, 0, 0, 0),
(4, 'Administrateur', 1, 1, 1, 1, 'red', 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `elenos_vote`
--

CREATE TABLE IF NOT EXISTS `elenos_vote` (
`id` int(11) NOT NULL,
  `name` text NOT NULL,
  `url` text NOT NULL,
  `image` text NOT NULL,
  `cooldown` int(11) NOT NULL DEFAULT '10800',
  `points` int(11) NOT NULL DEFAULT '10'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `elenos_vote`
--

INSERT INTO `elenos_vote` (`id`, `name`, `url`, `image`, `cooldown`, `points`) VALUES
(1, 'RPG-Paradize', 'http://www.rpg-paradize.com/?page=vote&vote=41049', 'images/rpg-paradize.png', 10800, 8),
(2, 'Serv-Minecraft.com', 'http://www.minecraft-serveur.com/vote.php?id=956', 'images/minecraft-serveur.png', 86400, 20),
(3, 'McParadize', 'http://www.mc-paradize.fr/vote,56', 'images/mc-paradize.png', 10800, 8),
(4, 'GTOP100', 'http://www.gtop100.com/in.php?site=84498', 'images/gtop100.png', 10800, 8);

-- --------------------------------------------------------

--
-- Table structure for table `elenos_vote_shop`
--

CREATE TABLE IF NOT EXISTS `elenos_vote_shop` (
`id` int(11) NOT NULL,
  `price` int(11) NOT NULL DEFAULT '100',
  `name` text NOT NULL,
  `command` text NOT NULL,
  `server` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `elenos_admin_upload`
--
ALTER TABLE `elenos_admin_upload`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `elenos_blog_comments`
--
ALTER TABLE `elenos_blog_comments`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`), ADD KEY `id_2` (`id`);

--
-- Indexes for table `elenos_contact_messages`
--
ALTER TABLE `elenos_contact_messages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `elenos_menu`
--
ALTER TABLE `elenos_menu`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `elenos_news`
--
ALTER TABLE `elenos_news`
 ADD PRIMARY KEY (`id`), ADD KEY `id` (`id`);

--
-- Indexes for table `elenos_servers`
--
ALTER TABLE `elenos_servers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `elenos_shop_history`
--
ALTER TABLE `elenos_shop_history`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `elenos_shop_items`
--
ALTER TABLE `elenos_shop_items`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `elenos_shop_sections`
--
ALTER TABLE `elenos_shop_sections`
 ADD PRIMARY KEY (`id`), ADD KEY `id` (`id`);

--
-- Indexes for table `elenos_shop_tokens`
--
ALTER TABLE `elenos_shop_tokens`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `elenos_slideshow`
--
ALTER TABLE `elenos_slideshow`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `elenos_support_tickets`
--
ALTER TABLE `elenos_support_tickets`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `elenos_support_tickets_replies`
--
ALTER TABLE `elenos_support_tickets_replies`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `elenos_token_transact`
--
ALTER TABLE `elenos_token_transact`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `elenos_users`
--
ALTER TABLE `elenos_users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `elenos_user_group`
--
ALTER TABLE `elenos_user_group`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `elenos_vote`
--
ALTER TABLE `elenos_vote`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `elenos_vote_shop`
--
ALTER TABLE `elenos_vote_shop`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `elenos_admin_upload`
--
ALTER TABLE `elenos_admin_upload`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `elenos_blog_comments`
--
ALTER TABLE `elenos_blog_comments`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `elenos_contact_messages`
--
ALTER TABLE `elenos_contact_messages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `elenos_menu`
--
ALTER TABLE `elenos_menu`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `elenos_news`
--
ALTER TABLE `elenos_news`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `elenos_servers`
--
ALTER TABLE `elenos_servers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `elenos_shop_history`
--
ALTER TABLE `elenos_shop_history`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `elenos_shop_items`
--
ALTER TABLE `elenos_shop_items`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `elenos_shop_sections`
--
ALTER TABLE `elenos_shop_sections`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `elenos_shop_tokens`
--
ALTER TABLE `elenos_shop_tokens`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `elenos_slideshow`
--
ALTER TABLE `elenos_slideshow`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `elenos_support_tickets`
--
ALTER TABLE `elenos_support_tickets`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `elenos_support_tickets_replies`
--
ALTER TABLE `elenos_support_tickets_replies`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `elenos_token_transact`
--
ALTER TABLE `elenos_token_transact`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `elenos_users`
--
ALTER TABLE `elenos_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `elenos_user_group`
--
ALTER TABLE `elenos_user_group`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `elenos_vote`
--
ALTER TABLE `elenos_vote`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `elenos_vote_shop`
--
ALTER TABLE `elenos_vote_shop`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
