<?php
function bbcodeTextBetweenTags ($string, $tags)	{
	return preg_replace('#(.*?)\['.$tags.'\](.*?)\[/'.$tags.'\](.*)#is', '$2', $string);
}
function bbcodeParseSimple ($string)	{
	/* Defining generic structures when no extra regex is needed (like bold, etc...) */

	/* Bold [b] ... [/b] */
	/* Italic [i] ... [/i] */
	/* Underlined [u] ... [/u] */
	/* Quotes [quote] Quoted text [/quote] */
	/* Code [code] #include <...> [/code] */

	$generic = array('b' => '<span class="bold">$1</span>',
					 'i' => '<span class="italic">$1</span>',
					 'u' => '<span class="underline">$1</span>',
					 's' => '<span class="striked">$1</span>',
					 'left' => '</p><p class="text_left">$1</p><p>',
					 'right' => '</p><p class="text_right">$1</p><p>',
					 'center' => '</p><p class="text_center">$1</p><p>',
					 'justify' => '</p><p class="text_justify">$1</p><p>',
					 'thin' => '<span class="thin_info">$1</span>',
					 /*'gauche' => '</p><p class="text_left">$1</p><p>',
					 'droite' => '</p><p class="text_right">$1</p><p>',
					 'centré' => '</p><p class="text_center">$1</p><p>',
					 'justifié' => '</p><p class="text_justify">$1</p><p>',
					 'centre' => '</p><p class="text_center">$1</p><p>',
					 'justifie' => '</p><p class="text_justify">$1</p><p>',*/
					 'quote' => '</p><blockquote><p>$1</p></blockquote><p>',
					 /*'citation' => '</p><blockquote><p>$1</p></blockquote><p>',*/
					 'code' => '</p><pre><span class="code_label">Code</span><code class="prettyprint linenums">$1</code></pre><p>',
					 /*'titre1' => '</p><h1>$1</h1><p>',
					 'titre2' => '</p><h2>$1</h2><p>',
					 'titre3' => '</p><h3>$1</h3><p>',
					 'titre4' => '</p><h4>$1</h4><p>',
					 'titre5' => '</p><h5>$1</h5><p>',
					 'titre6' => '</p><h6>$1</h6><p>',*/
					 'title1' => '</p><h1>$1</h1><p>',
					 'title2' => '</p><h2>$1</h2><p>',
					 'title3' => '</p><h3>$1</h3><p>',
					 'title4' => '</p><h4>$1</h4><p>',
					 'title5' => '</p><h5>$1</h5><p>',
					 'title6' => '</p><h6>$1</h6><p>');

	foreach ($generic as $key => $value) {
		$string = preg_replace("#\[".$key."\](.*)\[/".$key."\]#isU", $value, $string);
	}
	return $string;
}
function bbcodeParseParam($string)	{
	/* Defining generic structures when no extra regex is needed but with alphanumeric parameter (like colors, sizes, align, ...) */

	/* $generic = array('align' => '</p><p style="text-align: $1">$1</p><p>'
		);

	foreach ($generic as $key => $value) {
		$string = preg_replace("#\[".$key."\](.+)\[/".$key."\]#isU", $value, $string);
	}*/

	/* Colors [color=...] ... [/color], accepts (red|green|blue|yellow|purple|olive) or hex code (3 or 6 digits) */
	$string = preg_replace("#\[color=(black|white|red|green|blue|yellow|purple|olive|cyan|\#[A-Fa-f0-9]{3}|\#[A-Fa-f0-9]{6})\](.+)\[/color\]#isU",
						  '<span style="color: $1">$2</span>', $string);
	/* Code (with language specified) [code=lang] ... [/code] */
	$string = preg_replace("#\[code=(.+)\](.+)\[/code\]#isU",
						  '</p><pre><span class="code_label">Code : $1</span><code class="prettyprint linenums">$2</code></pre><p>', $string);

	return $string;
}
function bbcodeParseURL ($string)	{
	/* Links w/o tags */
	$string = preg_replace("#([^=\]\"])(https?://[a-zA-Z0-9._%?&~/=-]+)#i", '$1<a href="$2">$2</a>', $string);
	/* Links [url=http://...] title [/url] */
	$string = preg_replace("#\[url=(https?://.+)\](.+)\[/url\]#i", '<a href="$1">$2</a>', $string);
	/* Links [url]http://...[/url] */
	$string = preg_replace("#\[url\](https?://.+)\[/url\]#i", '<a href="$1">$1</a>', $string);
	/* Images [img=http://...] Caption [/img] => little images on the side */
	$string = preg_replace("#\[img=(.*)\](.+)\[/img\]#i",
						'<span class="image"><img src="$2" alt="User Image" /><br>$1<span class="horizontal_separator"></span></span>', $string);
	/* Images [bimg=http://...] Caption [/bimg] => big images centered */
	$string = preg_replace("#\[bimg=(.*)\](.+)\[/bimg\]#i",
						'</p><p class="image"><img src="$2" alt="User Image" /><br>$1</p><p>', $string);

	return $string;
}
function bbcodeParseTable ($string)	{
	/* Removing all <br> inside a table */
	while (preg_match('#\[table\](.*)\[/table\]#isU', $string))	{
		$table_content = preg_replace('#(<br/>|<br />|<br>)#isU', '', bbcodeTextBetweenTags($string, 'table'));
		/* $table_content = preg_replace('#()#isU', '', bbcodeTextBetweenTags($string, 'table')); */

		$string = preg_replace('#\[table\](.*)\[/table\]#isU', '</p><table>'.$table_content.'</table><p>', $string, 1);
	}

	$generic = array(/* 'table' => '</p><table>$1</table><p>', */
					 'caption' => '<caption>$1</caption>',
					 'head' => '<thead>$1</thead>',
					 'body' => '<tbody>$1</tbody>',
					 'foot' => '<tfoot>$1</tfoot>',
					 'line' => '<tr>$1</tr>',
					 'cell' => '<td>$1</td>',
					 'title_cell' => '<th>$1</th>');

	foreach ($generic as $key => $value) {
		$string = preg_replace("#\[".$key."\](.+)\[/".$key."\]#isU", $value, $string);
	}

	
	return $string;
}
function bbcodeParseSmilies ($string)	{
	$generic = array(':)' => '<span class="bold">$1</span>',
					 'i' => '<span class="italic">$1</span>',);

	foreach ($generic as $key => $value) {
		$string = preg_replace("#\[".$key."\](.*)\[/".$key."\]#isU", $value, $string);
	}

	return $string;
}

function bbcodeParse ($string, $escapeSpecials = false, $addParagraphs = true)	{
	if ($escapeSpecials)	$string = stripslashes(nl2br(htmlspecialchars($string)));
	if ($addParagraphs)		$string = '<p>'.$string.'</p>';

	$string = bbcodeParseSimple($string);
	$string = bbcodeParseURL ($string);
	$string = bbcodeParseParam ($string);
	$string = bbcodeParseTable ($string);

	/* Horizontal linear gradient */
	$string = preg_replace("#\[hsep/\]#i", '<span class="horizontal_separator"></span>', $string);

	/* Cleaning orphan & empty <p></p> */
	$string = preg_replace('#<p></p>#isU', '', $string);
	$string = preg_replace('#<p>(<br />|<br/>|<br>|\s)*</p>#', '', $string);

	return $string;
}
?>