<?php

include_once 'session.php';

/* Gives us a PDO object $bdd */
include_once ('bdd_connect.php');
include_once ('user_data.php');
include_once ('login.php');
include_once ('bbcode_parser.php');

$page_name = 'Nous contacter';

function buildContactForm ()	{
	?>
	<div id="site_corpse">
		<section id="contact_form">
			<header><h2>Nous contacter</h2></header>
			<p>
				Un souci, un problème ? Vous pouvez nous contacter afin de nous poser quelques questions.<br />
				Pensez bien à faire un tour sur le <a href="forum.php">forum</a> et sur le <a href="support.php">support</a>.
			</p>
			<div class="horizontal_separator"></div>
			<article>
				<h3>Ingame</h3>
				<p>
					Vous pouvez obtenir de l'aide directement Ingame auprès des administrateurs / modérateurs.
				</p>
			</article>
			<div class="horizontal_separator"></div>
			<article>
				<h3>Par email</h3>
				<p>
					Notre adresse mail principale est la suivante, à utiliser avec modértion !<br />
					<?php echo '<a href="mailto:"'.getAdminEmail ().'">'.getAdminEmail ().'</a>'; ?>
				</p>
			</article>
			<div class="horizontal_separator"></div>
			<article>
				<h3>Via les réseaux sociaux</h3>
				<p>
					Vous pouvez également nous contacter via Twitter : <a href="http://twitter.com/ElenosServeurs">@ElenosServeurs</a>
				</p>
			</article>
			<div class="horizontal_separator"></div>
			<article>
				<h3>En nous envoyant un message</h3>
				<p>Note : vous devez être inscrit et connecté pour envoyer un message.</p>
				<form method="post" action="contact.php?sendMessage" autocomplete="off">
					<p class="formfield">
						<label for="msg_title" class="">Pourquoi nous contactez-vous ?</label><br />
						<input type="text" name="msg_title" id="msg_title" value="" required="required" 
						placeholder="Entrez ici le sujet de votre demande..." class="textfield">
					</p>
					<p class="formfield">
						<textarea name="msg_content" id="msg_content" placeholder="Racontez-nous tout !" required="required" class="textfield" rows="10"
						></textarea>
					</p>
					<p class="formfield last">
						<input type="submit" class="submit push_button_normal" name="send_message" value="Envoyer" />
					</p>
				</form>
			</article>
		</section>
	</div>
	<?php
}
function saveMessage ($bdd, $title, $content)	{
	if (!isUserLoggedIn ())
		return 'Vous devez être connecté pour envoyer un message.';
	mail (getAdminEmail (), htmlspecialchars($title), '<html><head><title>'.htmlspecialchars($title).'</title><meta charset="utf-8">
</head><body style="font-family: Helvetica, Arial, sans-serif, serif;"><p>Via le formulaire Elenos</p>'.bbcodeParse($content, true), 'MIME-Version: 1.0'."\r\n".'Content-type: text/html; charset=utf-8'."\r\n".'From: '.getCurrentUserName ().' <'.getCurrentUserEmail ().'>');

	$req = $bdd->prepare ('INSERT INTO `elenos_contact_messages`(`author`, `title`, `content`, `date`) VALUES (:author, :title, :content, NOW())');
	$req->execute (array('author' => getCurrentUserName (), 'title' => $title, 'content' => $content));
	$rc = $req->rowCount ();
	$req->closeCursor ();

	if ($rc != 0)
		return 'ok';
	else
		return 'Erreur interne, réessayez plus tard.';
}

if (isset($_GET['sendMessage']) && isset($_POST['msg_title']) && isset($_POST['msg_content']))	{
	$_SESSION['saveMessageResult'] = saveMessage ($bdd, $_POST['msg_title'], $_POST['msg_content']);
	header('Location: contact.php');
	exit;
}

include ('header.php');

checkSessionExecuteMessage ('saveMessageResult', 'ok', 'Message envoyé');

buildContactForm ();

include ('right_menu.php');
include ('footer.php');

?>















