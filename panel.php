<?php

include_once 'session.php';

include_once ('common.php');
include_once ('models/panel_model.php');

$page_name = 'Espace Membre';

function redirectAndExit ()	{
	header('Location: panel.php');
	exit;
}

/* Fuck you PHP ... An unchecked checkbox is NOT sent to PHP T_T */
if (!isset($_POST['shopDisplay']))	$_POST['shopDisplay'] = "off";
if (!isset($_POST['menuDisplay']))	$_POST['menuDisplay'] = "off";

if (isset($_GET['editProfile']))	{
	if (isset($_POST['avatar']))	{
		if ($_POST['avatar'] = 'url' && isset($_POST['avatar_url']) && !is_empty($_POST['avatar_url']))
			$_SESSION['setAvatarResult'] = panelSetAvatarURL ($bdd, $_POST['avatar_url']);
		else if ($_POST['avatar'] = 'upload' && isset($_FILES['avatar_file']) && $_FILES['avatar_file']['error'] != UPLOAD_ERR_NO_FILE)
			$_SESSION['setAvatarResult'] = panelSetAvatarFile ($bdd, $_FILES['avatar_file']);
	}
	if (isset($_POST['current_pass']) && isset($_POST['new_pass']) && isset($_POST['new_pass2']) &&
		!is_empty($_POST['current_pass']) && !is_empty($_POST['new_pass']) && !is_empty($_POST['new_pass2']))
		$_SESSION['setPasswordResult'] = panelSetPassword ($bdd, $_POST['current_pass'], $_POST['new_pass'], $_POST['new_pass2']);
	redirectAndExit ();
}
if (isset($_GET['refreshAvatar']))	{
	makeUserAvatar (getCurrentUserName ());
	$_SESSION['refreshAvatarResult'] = panelSetAvatarURL ($bdd, 'avatar/'.getCurrentUserName ().'.png');
	redirectAndExit ();
}
if (isset($_GET['editServer']))	{
	if (isset ($_POST['name']) && isset ($_POST['iname']) && isset($_POST['port']) && isset($_POST['shopDisplay']) && isset($_POST['menuDisplay']))
		$_SESSION['editServerResult'] = panelEditServer ($bdd, $_GET['editServer'], $_POST['name'], $_POST['iname'], $_POST['port'], $_POST['shopDisplay'], $_POST['menuDisplay']);
	redirectAndExit ();
}
if (isset($_GET['deleteServer']))	{
	$_SESSION['deleteServerResult'] = panelDeleteServer ($bdd, $_GET['deleteServer']);
	redirectAndExit ();
}
if (isset($_GET['addServer']))	{
	$_SESSION['addServerResult'] = panelAddServer ($bdd, $_POST['name'], $_POST['iname'], $_POST['port'], $_POST['shopDisplay'], $_POST['menuDisplay']);
	redirectAndExit ();
}
if (isset($_GET['setLobby']))	{
	if (isset($_POST['lobby_srv']))
		$_SESSION['setLobbyResult'] = panelSetLobby ($bdd, $_POST['lobby_srv']);
	redirectAndExit ();
}
if (isset($_GET['imgUpload']))	{
	if (isset($_FILES['up_file']) && $_FILES['up_file']['error'] != UPLOAD_ERR_NO_FILE)
		$_SESSION['imgUploadResult'] = panelSendFile ($bdd, $_FILES['up_file']);
	else	{
		var_dump($_FILES);
		if (!isset($_FILES['up_file']))
			$_SESSION['imgUploadResult'] = "Not set";
		else if ($_FILES['up_file']['error'] == UPLOAD_ERR_NO_FILE)
			$_SESSION['imgUploadResult'] = "UPLOAD_ERR_NO_FILE";
	}
	redirectAndExit ();
}
if (isset($_GET['shopCSV']))	{
	if (!doCurrentUserHavePermission ('general.editServers'))	{
		$_SESSION['CSVresult'] = "Vous n'avez pas la permission de visualiser les historiques CSV.";
		redirectAndExit ();
	}
	header('Content-type: text/csv');
	header('Content-disposition: attachment;filename=ShopHistory.csv');
	echo "Article,Prix (tokens),Serveur,Pseudo,Date\n";
	$req = $bdd->query ('SELECT `hist`.*, `itm`.*, `srv`.`name` AS `server`
FROM `elenos_shop_history` AS `hist`, `elenos_shop_items` AS `itm`, `elenos_servers` AS `srv`, `elenos_shop_sections` AS `sec` 
WHERE `itm`.`id` = `hist`.`item` AND `srv`.`id` = `sec`.`server` AND `sec`.`id` = `itm`.`section` ORDER BY `date` DESC');
	while ($data = $req->fetch ())
		echo $data['label'].','.$data['price'].','.$data['server'].','.$data['player'].','.$data['date']."\n";
	exit;
}
if (isset($_GET['tokensCSV']))	{
	if (!doCurrentUserHavePermission ('general.editServers'))	{
		$_SESSION['CSVresult'] = "Vous n'avez pas la permission de visualiser les historiques CSV.";
		redirectAndExit ();
	}
	header('Content-type: text/csv');
	header('Accept-Charset: utf-8');
	header('Content-disposition: attachment;filename=TokensHistory.csv');
	echo "De,À,Montant (Tokens),Montant (€),Date\n";
	$req = $bdd->query ('SELECT `hist`.*, `itm`.* FROM `elenos_token_transact` AS `hist`, `elenos_shop_tokens` AS `itm` 
						 WHERE `itm`.`id` = `hist`.`itemID` ORDER BY `completionDate` DESC');
	while ($data = $req->fetch ())	{
		if ($data['method'] == "paypal")
			echo 'PayPal,'.$data['user'].','.$data['tokens'].','.$data['price_paypal'].'€,'.$data['completionDate'];
		else if ($data['method'] == "starpass")
			echo 'StarPass,'.$data['user'].',10,'.'1€,'.$data['completionDate'];
		else if ($data['method'] == "user")
			echo $data['fromUser'].','.$data['user'].','.$data['token'].','.(((int)$data['token'])/10).'€,'.$data['completionDate'];
		echo "\n";
	}
	exit;
}

include_once ('header.php');

if ($_SESSION['loginError'] != '')	{
	printErrorMessage ($_SESSION['loginError']);
	$_SESSION['loginError'] = '';
}

// checkSessionExecuteMessage ('session', 'ok', "ToDisplay");
checkSessionExecuteMessage ('setAvatarResult', 'ok', "Avatar modifié.");
checkSessionExecuteMessage ('setPasswordResult', 'ok', "Mot de passe édité.");
checkSessionExecuteMessage ('refreshAvatarResult', 'ok', "Avatar mis à jour.");
checkSessionExecuteMessage ('editServerResult', 'ok', "Serveur édité.");
checkSessionExecuteMessage ('deleteServerResult', 'ok', "Serveur supprimé.");
checkSessionExecuteMessage ('addServerResult', 'ok', "Serveur ajouté");
checkSessionExecuteMessage ('setLobbyResult', 'ok', "Lobby défini.");
checkSessionExecuteMessage ('imgUploadResult', 'ok', "Fichier uploadé.");
checkSessionExecuteMessage ('CSVresult', 'ok', "Historiques exportés en CSV.");

if (isset($_GET['fullHistory']))
	panelFullHistory ($bdd);
else
	panelMainPage ($bdd);

include ('right_menu.php');
include ('footer.php');

?>