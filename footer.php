<?php
include_once ('common.php');
?>

		</div>
		<footer id="main_footer">
			<div class="horizontal_separator"></div>
			<div id="main_footer_content">
				<section>
					<h3><a href="#">Elenos.fr</a></h3>
					<?php echo buildMainMenu (); ?>
				</section>
				<h4><a href="http://tuetuopay.fr">© 2014 Tuetuopay</a></h4>
			</div>
		</footer>
	</body>
</html>