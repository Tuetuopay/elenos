<?php

/* Gives us a PDO object $bdd */
include_once ('bdd_connect.php');
include_once ('permissions.php');
include_once ('user_data.php');
include_once ('server.php');

if (!isset($_SESSION['postsPerPage']))
	$_SESSION['postsPerPage'] = 5;

$postsPerPage = $_SESSION['postsPerPage'];
$slideshow = false;
if (!(isset($noURLSave) && $noURLSave == true))
	$_SESSION['currentURL'] = basename($_SERVER['REQUEST_URI']);

date_default_timezone_set ('Europe/Paris');

/* Just in case ... OVH's outdated PHP is escaping ALL forms ' with \' ... */
foreach ($_POST as $key => $value)	{
	$_POST[$key] = str_replace("\\'", "'", $value);
}

if (!isset($mainMenuItems))	{
	$mainMenuItems = array ();
	loadMainMenu ($bdd);
}

function loadMainMenu ($bdd)	{
	$req = $bdd->query ('SELECT * FROM `elenos_menu` ORDER BY `id`');
	global $mainMenuItems;

	while ($data = $req->fetch ())	{
		$mainMenuItems[$data['id']] = array('link' => $data['link'], 'name' => $data['name']);
	}

	$req->closeCursor ();
}

function buildMainMenu ()	{
	global $mainMenuItems;

	$ret = '<ul>';
	for ($i = 1; $i <= count($mainMenuItems); $i++)	{
		$ret .= '<li><a href="'.$mainMenuItems[$i]['link'].'">'.$mainMenuItems[$i]['name'].'</a></li>';
	}
	return $ret.'</ul>';
}

function beginPage ()	{
	echo '<div id="site_corpse">';
}
function endPage ()	{
	echo '</div>';
}
function beginStandaloneSection ($title, $floating, $id = "", $maxWidth = -1)	{
	echo '<div';
	if ($floating)
		echo ' class="floating"';
	if ($id != "")
		echo ' id="'.$id.'"';
	if ($maxWidth > 0)
		echo ' style="max-width:'.$maxWidth.'px;"';
	echo '><section class="standalone_section"><header><h2>'.$title.'</h2></header>';
}
function endStandaloneSection ()	{
	echo '</section></div>';
}

function beginTable ($colTitles, $styleTable)	{
	$ret = '<table';
	if ($styleTable)	$ret .= ' class="styled"';
	$ret .= '><thead><tr>';
	foreach ($colTitles as $title)
		$ret .= '<th>'.$title.'</th>';
	$ret .= '</tr></thead><tbody>';
	echo $ret;
}
function tableEntry ($cells)	{
	$ret = '<tr>';
	foreach ($cells as $cell)
		$ret .= '<td>'.$cell.'</td>';
	$ret .= '</tr>';
	echo $ret;
}
function endTable ()	{
	echo '</tbody></table>';
}

function printErrorMessage ($message)	{
	echo '<div class="error_message"><p>'.$message.'</p></div>';
}
function printInfoMessage ($message)	{
	echo '<div class="info_message"><p>'.$message.'</p></div>';
}

function popupWindowStyle ($name)	{
	return '<style scoped>#popup_'.$name.':target ~ .popup_window	{z-index: 40; opacity: 1;}</style>';
}
function buildPopupWindow ($name, $title, $content, $large = false)	{
	$ret =  '<div id="popup_'.$name.'" class="nodisplay"></div><div class="popup_window';
	if ($large)	$ret .= ' large';
	$ret .= '"><a href="#empty" class="popup_background"></a><h2>'.$title.'</h2>'.$content.'</div>';
	return $ret;
}

function PDOQuery ($bdd, $query, $field, $value)	{
	$req = $bdd->prepare ($query);
	$req->execute (array($field => $value));
	$ret = $req->fetch ();
	$ret = $ret[$field];
	$req->closeCursor ();
	return $ret;
}

function checkSessionExecuteMessage ($field, $validKey, $validMessage)	{
	if ($_SESSION[$field] == $validKey)	{
		printInfoMessage ($validMessage);
		$_SESSION[$field] = null;
	}
	else if (isset($_SESSION[$field]) && $_SESSION[$field] != null)	{
		printErrorMessage ($_SESSION[$field]);
		$_SESSION[$field] = null;
	}
}

function parseURL ($url)	{
	$params = explode("&", $url);

	foreach($params as $param)	{
		list($name, $value) = explode("=", $param);
		$ret[$name]=urldecode($value);
	}
	return $ret;
}

function is_empty ($var)	{
	return empty($var);
}

loadMainMenu ($bdd);

?>