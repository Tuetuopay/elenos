<?php
include_once 'session.php';

include_once ('common.php');
include_once ('paymentAPI.php');
include_once ('models/boutique_model.php');

$page_name = 'Boutique';

function redirectAndExit ()	{
	if (isset($_GET['srv']) && @$_GET['srv'] != -1)
		header('Location: boutique.php?srv='.$_GET['srv']);
	else
		header('Location: boutique.php');
	exit;
}

$noSRV = false;
if (!isset($_GET['srv']) && !isset($_GET['sendTokens']) && !isset($_GET['buyTokens']))	{
	$_GET['srv'] = -1;
	$noSRV = true;
}

if (isset($_GET['buy']))	{
	if (shopItemExists ($bdd, $_GET['buy']))
		$_SESSION['purchaseItemResult'] = shopBuyItem ($bdd, $_GET['buy']);
	else
		$_SESSION['purchaseItemResult'] = "Cette référence n'existe pas.";
	redirectAndExit ();
}
if (isset($_GET['addItem']))	{
	if (isset($_POST['item_title']) && isset($_POST['item_img']) && isset($_POST['item_desc']) && isset($_POST['item_price']) && 
		isset($_POST['sectionID']) && isset($_POST['item_command']))	{
		$_SESSION['addItemResult'] = shopAddItem($bdd, $_POST['item_title'], $_POST['item_img'], $_POST['item_desc'], $_POST['item_price'], 
			$_POST['sectionID'], $_POST['item_command']);
	}	else
		$_SESSION['addItemResult'] = "Veuillez spécifier tous les champs.";
	redirectAndExit ();
}
if (isset($_GET['editItem']))	{
	if (isset($_POST['edit_title']) && isset($_POST['edit_img']) && isset($_POST['edit_desc']) && isset($_POST['edit_price']) && 
		isset($_POST['edit_command']))	{
		$_SESSION['editItemResult'] = shopEditItem($bdd, $_GET['editItem'], $_POST['edit_title'], $_POST['edit_img'], $_POST['edit_desc'],
			$_POST['edit_price'], $_POST['edit_command']);
	}	else
		$_SESSION['editItemResult'] = "Veuillez spécifier tous les champs.";
	redirectAndExit ();
}
if (isset($_GET['deleteItem']))	{
	$_SESSION['deleteItemResult'] = shopDeleteItem ($bdd, $_GET['deleteItem']);
	redirectAndExit ();
}
if (isset($_GET['addSection']) && isset($_POST['section_title']))	{
	$_SESSION['addSectionResult'] = shopAddSection ($bdd, $_POST['section_title'], $_GET['srv']);
	redirectAndExit ();
}
if (isset($_GET['deleteSection']))	{
	$_SESSION['deleteSectionResult'] = shopDeleteSection ($bdd, $_GET['deleteSection']);
	redirectAndExit ();
}
if (isset($_GET['moveUp']))	{
	$_SESSION['moveUpResult'] = shopMoveSectionUp ($bdd, $_GET['srv'], $_GET['moveUp']);
	redirectAndExit ();
}
if (isset($_GET['moveDown']))	{
	$_SESSION['moveDownResult'] = shopMoveSectionDown ($bdd, $_GET['srv'], $_GET['moveDown']);
	redirectAndExit ();
}

if (isset ($_GET['cancelCheckout']))	{
	if ($_GET['cancelCheckout'] == 'paypal' && isset($_GET['token']))	{
		$req = $bdd->prepare ('UPDATE `elenos_token_transact` SET `status` = "cancelled", `completionDate` = NOW() WHERE `token` = :token');
		$req->execute (array ('token' => $_GET['token']));
		$req->closeCursor ();
	}
	header('Location: boutique.php');
	exit;
}
if (isset($_GET['buyTokens']) && $_GET['buyTokens'] == 'proceed')	{
	if (isset($_GET['item']))	{	/* Here we are dealing with StarPass */
		$extraHeaderInfo = '<noscript><meta http-equiv="refresh" content="0;url=http://script.starpass.fr/error_code2.php?idd=252559&idp=157304"></noscript><script type="text/javascript" src="http://script.starpass.fr/error_code.php?idd=252559&idp=157304"></script>';
	}
	if (isset($_POST['payment']) && isset($_POST['payment_option']) && $_POST['payment_option'] == 'none')	{
		$_SESSION['buyTokenResult'] = 'Veuillez faire votre choix !';
		header('Location: boutique.php?buyTokens=display');
		exit;
	}
	if ($_POST['payment'] == 'paypal')	{
		$request = buildPaypalURL ();

		$tokensInfo = shopGetTokenInfo ($bdd, $_POST['payment_option']);

		$request = $request."&METHOD=SetExpressCheckout".
					"&CANCELURL=".urlencode("http://".getServerURL ()."/boutique.php?cancelCheckout=paypal").
					"&RETURNURL=".urlencode("http://".getServerURL ()."/boutique.php?checkout=paypal&item=".$tokensInfo['id']).
					"&AMT=".$tokensInfo['price_paypal'].
					"&CURRENCYCODE=EUR".
					"&DESC=".urlencode($tokensInfo['display']).
					"&LOCALECODE=FR".
					"&HDRIMG=".urlencode("http://".getServerURL ()."/images/Logo-Elenos.png");

		$ch = curl_init($request);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$paypalResult = curl_exec($ch);

		if (!$paypalResult)	{
			$_SESSION['buyTokenResult'] = "Erreur de communication avec PayPal [code 1]. Cet incident a été signalé, merci de réessayer plus tard.";
			mail("WEBMASTER_EMAIL", "Erreur PayPal 1 Elenos", "Erreur CURL : ".curl_error($ch).".\n\nRequête : ".$request);
			header ('Location: boutique.php');
			exit;
		}
		else	{
			$paramList = parseURL ($paypalResult);
			if ($paramList['ACK'] == 'Success')	{
				$req = $bdd->prepare ('INSERT INTO `elenos_token_transact`(`user`, `token`, `status`, `itemID`, `method`, `completionDate`)
									   VALUES (:user, :token, "pending", :itemID, "paypal", NOW())');
				$req->execute (array ('user' => getCurrentUserName (), 'token' => $paramList['TOKEN'], 'itemID' => $tokensInfo['id']));
				$req->closeCursor ();

				$_SESSION['paypal_info']['TOKEN'] = $paramList['TOKEN'];
				$_SESSION['paypal_info']['ITEM'] = $_POST['payment_option'];
				header("Location: https://www.".getBasePaypalURL()."/webscr&cmd=_express-checkout&token=".$paramList['TOKEN']);
				exit();
			}	else	{
				$_SESSION['buyTokenResult'] = "Erreur de communication avec PayPal [code 2]. Cet incident a été signalé, merci de réessayer plus tard.";
				mail("WEBMASTER_EMAIL", "Erreur PayPal 2 Elenos", "Erreur courte : ".$paramList['L_SHORTMESSAGE0'].
					"\nErreur longue : ".$paramList['L_LONGMESSAGE0']);
				header ('Location: boutique.php');
				exit;
			}
		}

		curl_close($ch);
	}
}
if (isset($_GET['buyTokens']) && $_GET['buyTokens'] == 'starpass' &&
	isset($_POST['idd']) && isset($_POST['idp']) && isset($_POST['DATAS']) && isset($_POST['code1']))	{
	if (checkStarpassCode ($_POST['code1'], $_POST['idp'], $_POST['idd']))	{
		$req = $bdd->prepare ('INSERT INTO `elenos_token_transact`(`user`, `token`, `status`, `itemID`, `completionDate`, `method`)
							   VALUES (:user, :token, "completed", 1, NOW(), "starpass")');
		$req->execute (array ('user' => getCurrentUserName (), 'token' => $_POST['code1']));
		$req->closeCursor ();
		shopSetTokens ($bdd, getCurrentUserTokens () + 10);
		$_SESSION['buyTokenResult'] = 'ok';
		redirectAndExit ();
	}	else	{
		$_SESSION['buyTokenResult'] = "Code StarPass incorrect.";
		redirectAndExit ();
	}
}
if (isset($_GET['checkout']))	{
	if ($_GET['checkout'] == 'paypal')	{
		$request = buildPaypalURL ();

		$request = $request."&METHOD=DoExpressCheckoutPayment".
					"&TOKEN=".htmlentities($_GET['token'], ENT_QUOTES).
					"&AMT=10.0".
					"&CURRENCYCODE=EUR".
					"&PayerID=".htmlentities($_GET['PayerID'], ENT_QUOTES).
					"&PAYMENTACTION=sale";

		$ch = curl_init($request);

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$paypalResult = curl_exec($ch);

		if (!$paypalResult)	{
			$_SESSION['buyTokenResult'] = "Erreur de communication avec PayPal [code 3]. Cet incident a été signalé, merci de réessayer plus tard.";
			mail("WEBMASTER_EMAIL", "Erreur PayPal 3 Elenos", "Erreur CURL : ".curl_error($ch).".\n\nRequête : ".$request);
			header ('Location: boutique.php');
			exit;
		}	else	{
			$paypalData = parseURL($paypalResult);
			if ($paypalData['ACK'] == 'Success')	{
				$req = $bdd->prepare ('UPDATE `elenos_token_transact` SET `status` = "completed", `completionDate` = NOW(), `token` = :token + "-done"
									   WHERE `token` = :token ;');
				$req->execute (array ('token' => $_GET['token']));
				$req = $bdd->prepare ('SELECT `elenos_shop_tokens`.`tokens` AS val FROM `elenos_shop_tokens`, `elenos_token_transact` 
										 WHERE `elenos_shop_tokens`.`id` = `elenos_token_transact`.`itemID` AND `elenos_token_transact`.`token` = :token ;');
				$req->execute (array ('token' => $_GET['token']));
				$val = $req->fetch ();
				$val = (int)$val['val'];
				shopSetTokens ($bdd, getCurrentUserTokens () + $val);
				$_SESSION['buyTokenResult'] = 'ok';
				header ('Location: boutique.php');
				exit;
			}	else	{
				$_SESSION['buyTokenResult']= "Erreur de communication avec PayPal [code 4]. Cet incident a été signalé, merci de réessayer plus tard.";
				mail("WEBMASTER_EMAIL", "Erreur PayPal 4 Elenos", "Erreur courte : ".$paypalData['L_SHORTMESSAGE0'].
					"\nErreur longue : ".$paypalData['L_LONGMESSAGE0']);
				header ('Location: boutique.php');
				exit;
			}
		}
		curl_close($ch);
	}
}
if (isset ($_GET['paypalCheck']))	{
	$req = 'cmd=_notify-validate';
	foreach ($_POST as $key => $value) {
		$value = urlencode(stripslashes($value));
		$req .= "&$key=$value";
	}
	
	$header = "POST /cgi-bin/webscr HTTP/1.0\n";
	$header .= "Host: ".getBasePaypalURL ()."\n";
	$header .= "Content-Type: application/x-www-form-urlencoded\n";
	$header .= "Content-Length: " . strlen($req) . "\n\n";
	$fp = fsockopen ('ssl://'.getBasePaypalURL (), 443, $errno, $errstr, 30);
	$item_name = $_POST['item_name'];
	$item_number = $_POST['item_number'];
	$payment_status = $_POST['payment_status'];
	$payment_amount = $_POST['mc_gross'];
	$payment_currency = $_POST['mc_currency'];
	$txn_id = $_POST['txn_id'];
	$receiver_email = $_POST['receiver_email'];
	$payer_email = $_POST['payer_email'];
	parse_str($_POST['custom'],$custom);
	$username = $custom['u'];
	if ($fp) {
		fputs ($fp, $header . $req);
		while (!feof($fp)) {
			$res = fgets ($fp, 1024);
			if (strcmp ($res, "VERIFIED") == 0 && $payment_status == "Completed" && getPaypalEmail () == $receiver_email) {
				$req = $bdd->prepare ('SELECT * FROM `elenos_shop_tokens` WHERE `price_paypal` = :price');
				$req->execute (array ('price' => (float)$payment_amount));
				$item = $req->fetch ();

				if ($item)	{
					$req = $bdd->prepare ('INSERT INTO `elenos_token_transact`(`user`, `token`, `status`, `itemID`, `method`, `completionDate`)
										   VALUES (:user, :token, "completed", :itemID, "paypal", NOW())');
					$req->execute (array ('user' => $username, 'token' => $txn_id, 'itemID' => $item['id']));
					$req = $bdd->prepare ('UPDATE `elenos_users` SET `tokens`=`tokens`+:tokens WHERE UPPER(`username`)=UPPER(:username)');
					$req->execute (array ('tokens' => $item['tokens'], 'username' => $username));
					// shopSetTokens ($bdd, getCurrentUserTokens () + (int)$item['tokens']);
				}
				fclose($fp);
				exit;
			} else if (strcmp ($res, "INVALID") == 0) {}
		}
	}

	exit;
}
if (isset($_GET['paypal']) && @$_GET['paypal'] == "true")	{
	$_SESSION['buyTokenResult'] = 'ok';
	redirectAndExit ();
}

include_once ('header.php');

if ($_SESSION['loginError'] != '')	{
	printErrorMessage ($_SESSION['loginError']);
	$_SESSION['loginError'] = '';
}

checkSessionExecuteMessage ('purchaseItemResult', 'ok', "L'objet a bien été acheté.");
checkSessionExecuteMessage ('addItemResult', 'ok', "L'objet a bien été ajouté.");
checkSessionExecuteMessage ('editItemResult', 'ok', "L'objet a bien été modifié.");
checkSessionExecuteMessage ('deleteItemResult', 'ok', "L'objet a bien été supprimé.");
checkSessionExecuteMessage ('addSectionResult', 'ok', "La section a bien été ajoutée.");
checkSessionExecuteMessage ('deleteSectionResult', 'ok', "La section a bien été supprimée.");
checkSessionExecuteMessage ('moveUpResult', 'ok', "Déplacement effectué.");
checkSessionExecuteMessage ('moveDownResult', 'ok', "Déplacement effectué.");
checkSessionExecuteMessage ('buyTokenResult', 'ok', "Tokens achetés.");
checkSessionExecuteMessage ('sendTokensResult', 'ok', "Tokens envoyés");


if (isset($_GET['buyTokens']) && $_GET['buyTokens'] == 'display')
	$_GET['srv'] = -1;
if (isset($_GET['sendTokens']))
	$_GET['srv'] = -1;
shopServerList ($bdd, $_GET['srv']);

if ($noSRV)
	shopChooseServer ($bdd);
else	{
	if (isset($_GET['buyTokens']) && $_GET['buyTokens'] == 'display')
		shopBuyTokens ($bdd);
	else if (isset($_GET['sendTokens']))
		shopSendTokens ($bdd);
	else
		shopMainPage ($bdd, $_GET['srv']);
}

include ('right_menu.php');
include ('footer.php');


?>
