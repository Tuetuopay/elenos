function makePopupPos ()	{
	var popups = document.getElementsByClassName("popup_window");	
	var wh = document.documentElement.clientHeight;

	for(var i=0; i<popups.length; i++){
		popups[i].style['max-height'] = (wh - 100)+'px';
		var h = popups[i].offsetHeight;
		popups[i].style['top'] = ((wh - h) / 2.0)+'px';
	}
}
window.onload = makePopupPos;
window.onresize = makePopupPos;