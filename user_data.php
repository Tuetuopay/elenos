<?php

include_once ('permissions.php');
include_once ('common.php');
include_once ('JSONAPI.php');

if ($_SESSION['loggedIn'])	{
	loadUserData ($bdd, $_SESSION['username']);
	reloadPermissions ($bdd, $_SESSION['username']);
}

function loadUserData ($bdd, $username)	{
	$req = $bdd->prepare ('SELECT * FROM `elenos_users` WHERE UPPER(`username`) = UPPER(:pseudo) AND `elenos_users`.`valid` = 0');
	$req->execute (array ('pseudo' => $username));
	$data = $req->fetch ();
	$req->closeCursor ();

	foreach ($data as $key => $value) {
		//if ($key != 'id' && $key != 'hashedPassword')
		//	continue;
		$_SESSION[$key] = $value;
	}

	$_SESSION['cachedAvatars'] = array(strtolower ($_SESSION['username']) => $data['avatarURL']);

	// print_r($req->errorInfo ());
	// echo 'loadUserData : ';
	// print_r($data);
}
function loadCurrentUserData ($bdd)	{
	loadUserData ($bdd, $_SESSION['username']);
}

function getCurrentUserName ()	{
	return getSessionValue ('username', 'Visiteur');
}
function getCurrentUserAvatarURL ()	{
	return getSessionValue ('avatarURL', 'images/avatar_default.png');
}
function getCurrentUserEmail ()	{
	return getSessionValue ('email', '');
}
function getCurrentUserTokens ()	{
	return getSessionValue ('tokens', 0);	
}
function getUserAvatarURL ($bdd, $username)	{
	/* if (isset($_SESSION['cachedAvatars'][strtolower ($username)]))	{
		return $_SESSION['cachedAvatars'][strtolower ($username)];
	}

	$req = $bdd->prepare ('SELECT `avatarURL` FROM `elenos_users` WHERE UPPER(`username`) = UPPER(:username)');
	$req->execute (array ('username' => $username));
	$ret = $req->fetch ();
	$req->closeCursor ();

	if (!isset($ret['avatarURL']))	{
		if (!file_exists ('avatar/'.$username.'.png'))
			makeUserAvatar ($username);
		$ret['avatarURL'] = 'avatar/'.$username.'.png';
	}

	$_SESSION['cachedAvatars'][strtolower ($username)] = $ret['avatarURL'];

	return $ret['avatarURL']; */
	return 'avatar.php?u='.$username;
}

function getSessionValue ($key, $default)	{
	if (isset ($_SESSION[$key]))
		return $_SESSION[$key];
	else
		return $default;	
}
function doUserExists ($bdd, $username)	{
	$req = $bdd->prepare ('SELECT COUNT(*) AS cnt FROM `elenos_users` WHERE UPPER(`username`) = UPPER(:username)');
	$req->execute (array ('username' => $username));
	$count = $req->fetch ();
	$count = $count['cnt'];
	$req->closeCursor ();
	return $count != 0;
}

function isUserLoggedIn ()	{
	return getSessionValue ('loggedIn', false);
}

function isCurrentUserOnline ($bdd, $serverID)	{
	return isUserOnline ($bdd, getCurrentUserName (), $serverID);
}
function isUserOnline ($bdd, $username, $serverID)	{
	// if ($username == 'Tuetuopay' && $serverID == 2)
	//	return true;
	$req = $bdd->prepare ('SELECT `port` FROM `elenos_servers` WHERE `id` = :id');
	$req->execute (array('id' => $serverID));
	$port = $req->fetch();
	$port = $port['port'];
	$req->closeCursor ();

	$api = getJSONAPI ($port);
	$api->setTimeout(1);
	$connect = $api->call("getPlayer", array($username));
	$connect = $connect[0];
	if ($connect['result'] == 'error')
		return false;

	$connect = $connect["success"]["ip"];

	if ($api->getError () != '')
		return false;
	if($connect == "offline")
		return false;
	else
		return true;
}
function getCurrentUserServer ($bdd)	{
	return getUserServer ($bdd, getCurrentUserName ());
}
function getUserServer ($bdd, $username)	{
	/* Getting servers */
	$req = $bdd->query ('SELECT * FROM `elenos_servers`');
	while ($server = $req->fetch ())	{
		if (isUserOnline($bdd, $username, $server['id']))
			return $server['id'];
	}
	return 0;
}
function getServerName ($bdd, $serverID)	{
	$ret = PDOQuery ($bdd, 'SELECT `name` AS `var` FROM `elenos_servers` WHERE `id` = :var', 'var', $serverID);
	if (is_empty($ret))
		$ret = "Serveur inconnu";
	return $ret;
}
function getLobbyServer ($bdd)	{
	$req = $bdd->query ('SELECT `id` FROM `elenos_servers` WHERE `isLobby` = 1 ORDER BY `id`');
	$ret = $req->fetch ();
	$ret = $ret['id'];
	if (is_empty ($ret))
		$ret = 1;
	return $ret;
}
function getServersInfo ($bdd, $onlyDisplayed = false)	{
	/* Getting servers */
	$ret = array();
	if ($onlyDisplayed == false)
		$req = $bdd->query ('SELECT * FROM `elenos_servers`');
	else
		$req = $bdd->query ('SELECT * FROM `elenos_servers` WHERE `menuDisplay`=1');

	$api = getJSONAPI (0);
	$api->setTimeout(1);

	while ($server = $req->fetch ())	{
		$api->setPort ($server['port']);
		$ret[$server['id']] = $server;

		$online = $api->call("getPlayerNames", array());
		$online = $online[0];
		$error = $api->getError ();
		if ($error != '')	{
			$ret[$server['id']]['status'] = 'offline';
			// PDOQuery ($bdd, 'INSERT INTO `elenos_curl_errors`(`text`) VALUES (:texte)', 'texte', $error);
			// echo 'CURL error : '.$error."\n";
			continue;
		}
		
		if ($online['result'] == 'success')
			$ret[$server['id']]['onlineCount'] = count ($online['success']);
		$ret[$server['id']]['status'] = 'online';
	}
	return $ret;
}
function getOnlinePlayers ($bdd)	{
	$ret = array();
	$req = $bdd->query ('SELECT * FROM `elenos_servers`');

	$api = getJSONAPI (0);
	$api->setTimeout(1);

	while ($server = $req->fetch ())	{
		$api->setPort ($server['port']);

		$onlinePlayers = $api->call('getPlayerNames');
		$onlinePlayers = $onlinePlayers[0];
		$error = $api->getError ();
		if ($error != '')
			continue;
		
		if ($onlinePlayers['result'] == 'success')	{
			foreach ($onlinePlayers['success'] as $name)
				$ret[$name] = $server['name'];
		}
	}
	uksort($ret, 'strcasecmp');
	return $ret;
}
function getPlayersOnServers ($bdd)	{
	$ret = array();
	$req = $bdd->query ('SELECT * FROM `elenos_servers`');

	$api = getJSONAPI (0);
	$api->setTimeout(1);

	while ($server = $req->fetch ())	{
		$api->setPort ($server['port']);

		$onlinePlayers = $api->call('getPlayerNames');
		$onlinePlayers = $onlinePlayers[0];
		$error = $api->getError ();
		if ($error != '')
			continue;
		
		if ($onlinePlayers['result'] == 'success')	{
			$ret[$server['name']] = array ();
			foreach ($onlinePlayers['success'] as $name)
				$ret[$server['name']][$name] = $server['name'];
		}
	}
	return $ret;
}

function makeUserAvatar ($user)	{
	copy('http://'.getServerURL().'/skin.php?pseudo='.$user, 'avatar/'.$user.'.png');
	// @copy('http://blackcraft.fr/skin.php?pseudo='.$user, 'avatar/'.$user.'.png');
}

function getAdminEmail ()	{
	return 'contact@elenos.fr';
}
function getPaypalEmail ()	{
	return 'paypal@elenos.fr';
}

function sessionIdUsername ($bdd, $id)	{
	$username = PDOQuery ($bdd, 'SELECT `username` AS `var` FROM `elenos_users` WHERE `session_id` = `var`', `var`, $id);
	if ($username)
		return $username;
	else
		return "";
}
function generateUniqueSessionID ($bdd)	{
	$id = "";
	do
		$id = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 60);
	while (!isEmpty (sessionIdUsername ($bdd, $id)));
	return $id;
}

?>