<?php

function tableExists ($db, $table)	{
	$req = $db->prepare ('SELECT COUNT(*) AS ret FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = "blackcraft" AND TABLE_NAME = :table');
	$req->execute (array ('table' => $table));
	$ret = $req->fetch ()['ret'];
	$req->closeCursor ();
	if ($ret == 1)
		return true;
	else
		return false;
}

include ('bdd_connect.php');

$query = "CREATE TABLE IF NOT EXISTS `bc_blog_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `postID` int(11) NOT NULL COMMENT 'The target (blog article OR other comment)',
  `commentID` int(11) NOT NULL DEFAULT '0' COMMENT 'The ID of the comment this comment replies to',
  `author` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `email` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `commentContents` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `date` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_2` (`id`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;

CREATE TABLE IF NOT EXISTS `bc_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `contents` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `authorName` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `date` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;

CREATE TABLE IF NOT EXISTS `bc_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `email` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `groupID` int(11) NOT NULL DEFAULT '1',
  `hashedPassword` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `avatarURL` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `valid` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '1' COMMENT 'Bit special : 0 means ''Yes, account valid'', everything else is the activation token.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;

CREATE TABLE IF NOT EXISTS `bc_user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupName` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `blog.postComment` int(11) NOT NULL DEFAULT '0',
  `blog.postArticle` int(11) NOT NULL DEFAULT '0',
  `blog.deleteArticle` int(11) NOT NULL DEFAULT '0',
  `blog.deleteComment` int(11) NOT NULL DEFAULT '0',
  `general.groupColor` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;

INSERT INTO `bc_user_group` (`id`, `groupName`, `blog.postComment`, `blog.postArticle`, `blog.deleteArticle`, `blog.deleteComment`, `general.groupColor`) VALUES
(1, 'Clampin', 1, 0, 0, 0, 'black'),
(2, 'Modérateur', 1, 0, 0, 1, 'blue'),
(3, 'Rédacteur', 1, 1, 0, 0, 'green'),
(4, 'Administrateur', 1, 1, 1, 1, 'red');

CREATE TABLE IF NOT EXISTS `bc_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `link` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

INSERT INTO `bc_menu` (`id`, `name`, `link`) VALUES
(1, 'Accueil', 'blog.php'),
(2, 'Boutique', 'boutique.php'),
(3, 'Support', 'support.php'),
(4, 'Forum', 'forum.php'),
(5, 'Vote', 'vote.php'),
(6, 'Contact', 'contact.php');
";

$req = $bdd->prepare ($query);

?>

<html>
	<head>
		<title>BlackCraft table initiation</title>
		<meta charset="utf-8">
	</head>
	<body>
		<?php echo 'Error log : '; print_r($req->errorInfo ()); $req->closeCursor (); ?>
	</body>
</html>