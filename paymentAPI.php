<?php
function buildPaypalURL ()	{
	$api_paypal = 'https://api-3t.sandbox.paypal.com/nvp?'; // Site de l'API PayPal. On ajoute déjà le ? afin de concaténer directement les paramètres.
	$version = 56.0; // Version de l'API
	
	$user = 'PAYPAL_API_USER'; // Utilisateur API
	$pass = 'PAYPAL_API_PASS'; // Mot de passe API
	$signature = 'PAYPAL_API_SIGNATURE'; // Signature de l'API

	$api_paypal = $api_paypal.'VERSION='.$version.'&USER='.$user.'&PWD='.$pass.'&SIGNATURE='.$signature; // Ajoute tous les paramètres

	return 	$api_paypal; // Renvoie la chaîne contenant tous nos paramètres.
}
function getBasePaypalURL ()	{
	return 'www.paypal.com';
}

function checkStarpassCode ($code, $idp, $idd)	{
	if ($idp != STARPASS_IDP || $idd != STARPASS_IDD)
		return false;
	$idp = STARPASS_IDP;
	$idd = STARPASS_IDD;

	$ident=urlencode($idp.";;".$idd);
	$code=urlencode($code);

	$get_f = @file ('http://script.starpass.fr/check_php.php?ident='.$ident.'&codes='.$code.'&DATAS='); 
	if(!$get_f)
		return false;

	$tab = explode ('|', $get_f[0]);

	if (!$tab[1]) return false;

	if (substr($tab[0],0,3) != "OUI")	return false;
	else								return true;
}
?>
